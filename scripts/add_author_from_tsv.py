#!/usr/bin/env python

import os, sys
import csv, json

from helpers import AssetFromTSV

if len(sys.argv) != 4:
    print('Usage: {} TOKEN UPLOAD[TRUE|FALSE] TSV_FILE'.format(sys.argv[0]))
    sys.exit(1)

_token = sys.argv[1]
if sys.argv[2].lower() == 'true':
    _upload = True
else:
    _upload = False
_tsv_file = sys.argv[3]

_helper = AssetFromTSV(_token)

print(20*'-')
print(f'-- upload: {_upload}')
print(f'-- filename: {_tsv_file}')
print(20*'-')
print()
_matched = 0

idx = 1
with open(_tsv_file) as fp:
    db = csv.DictReader(fp, dialect='excel-tab')
    assets = []
    asset_id = 0
    _matched = 0
    for entry in db:
        print(f"[--] processing entry : {idx} / {entry['UT']}")
        _wos_id = entry['UT'].split(':')
        #_e, _, _ = _helper.check_exists_wos_id(_wos_id[0], _wos_id[1])
        _e = False
        if not _e:
            _asset, _m = _helper.generate_asset_from_tsv(entry)
            _matched += _m
            assets.append(_asset)
        asset_id += 1
        idx += 1

print(f'Number of assets: {asset_id}')
print(f'Number of matched authors: {_matched}')
print(f'New assets: {len(assets)}')

if len(assets) > 0:
    json.dump(assets, open(f'assets-{os.path.basename(_tsv_file.split(".")[0])}.json', 'w'), indent=2)

_inserted = 0
if _upload:
    for a in assets:
        #_e, _, _ = _helper.check_exists_wos_id(a['index_source']['source_name'], a['index_source']['source_id'])
        #if not _e:
        if a['is_uvt'] == 1:
            _helper.insert_asset(a)
            _inserted += 1

print('Inserted assets: {}'.format(_inserted))

