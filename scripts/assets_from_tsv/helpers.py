import requests
import json
import uuid
from itertools import combinations
import re
import os

import urllib3
urllib3.disable_warnings()


# DT -> PT -> URAP_types correlation
# Where URAP_types is ['journal','conference','workshop','chapter','book','poster']
publication_types = {
    # journal
    'J': {
        'Article; Proceedings Paper': 'conference',
        'Proceedings Paper': 'conference',
        'Meeting Abstract': 'conference',
        'Art Exhibit Review': 'journal',
        'Article': 'journal',
        'Review': 'journal',
        'Article; Early Access': 'journal',
        'Article; Retracted Publication': 'journal',
        'Review; Early Access': 'journal',
        'Review; Retracted Publication': 'journal',
        'Editorial Material': 'journal',
        'Book Review': 'journal',
        'Book Review; Early Access': 'journal',
        'Correction': 'journal',
        'Correction, Addition': 'journal',
        'Note': 'journal',
        'Letter': 'journal',
        'Biographical-Item': 'journal',
        'Film Review': 'journal',
        'Discussion': 'journal',
        'News Item': 'journal',
        'Bibliography': 'journal',
        'News Item': 'journal',
        'Article; Data Paper': 'journal',
        'Editorial Material; Early Access': 'journal'
    },
    # book
    'B': {
        'Book': 'book',
        'Article; Book Chapter': 'chapter',
        'Editorial Material; Book Chapter': 'chapter',
        'Proceedings Paper': 'conference',
        'Article; Proceedings Paper': 'conference',
        'Meeting Abstract': 'conference'
    },
    # patent
    'P': {

    },
    # series
    'S': {
        'Article; Proceedings Paper': 'book',
        'Book': 'book',
        'Review; Book Chapter': 'chapter',
        'Review': 'book',
        'Editorial Material; Book Chapter': 'chapter',
        'Article; Book Chapter': 'journal',
        'Proceedings Paper': 'conference',
        'Article': 'journal',
        'Article; Book': 'journal'
    }
}

uvt_names = [
                    'Fac Math, Timisoara 1900, Romania',
                    'WEST UNIVERSITY OF TIMISOARA',
                    'AGR UNIV TIMISOARA',
                    'IND CHEM ENVIRONM ENGN FAC',
                    'TIMISOARA UNIV',
                    'UNIT WEST',
                    'UNIV DIN TIMISOARA',
                    'UNIV OUEST TIMISOARA',
                    'UNIV TIMIS OARA',
                    'UNIV TIMISOARA',
                    'UNIV TIMISOARA W',
                    'UNIV TIMISOARA WEST',
                    'UNIV VEST',
                    'UNIV VEST DIN TIMISOARA',
                    'Univ Vest Timisoara',
                    'UNIV W',
                    'UNIV W TIMISOARA',
                    'UNIV W TOMISOARA',
                    'UNIV WEST',
                    'UVT',
                    'VEST UNIV TIMISOARA',
                    'W TIMISOARA UNIV',
                    'W UNIV',
                    'W UNIV RIMISOARA',
                    'W UNIV TIMIOARA',
                    'W UNIV TIMIS OARA',
                    'W UNIV TIMISAORA',
                    'W UNIV TIMISOARA',
                    'W UNIV TIMISOARA V PARVAN',
                    'W UNIV TIMISOARRA',
                    'W UNIV TIMISOUARA',
                    'W UNIV TIMISOURA',
                    'W UNIV TIMSOARA',
                    'W UNIV TINDSOARA',
                    'W UNIV TUNISOARA',
                    'W UNV TIMISOARA',
                    'W VIRGINIA UNIV TIMISOARA',
                    'WEAT UNIV TIMISOARA',
                    'WEST UNIV',
                    'WEST UNIV TIMISOARA',
                    'WESTERN UNIV',
                    'WESTERN UNIV TIMISOARA'
                ]

asset_types = {
    'J': 'publication',
    'B': 'publication',
    'S': 'publication',
    'P': 'patent'
}

class AssetFromTSV():

    def __init__(self, token):
        self.headers = {'Content-type': 'application/json; charset=utf-8', 'Authorization': token}

    def check_exists_wos_id(self, index_source, source_id, silent=True, endpoint='asset/by-index-source'):
        ret = requests.get('http://localhost:5001/api/v2/{}?where={{"index_source.source_name":"{}", "index_source.source_id":{{"$in": ["{}", "WOS:{}"]}}}}'.format(endpoint, index_source, source_id, source_id), headers=self.headers, verify=False)
        if ret.status_code == 200:
            pl = json.loads(ret.text)
            if pl['_meta']['total'] == 1:
                if not silent:
                    print("-- {}:{} exists.".format(index_source, source_id))
                return True, pl['_items'][0]['_id'], pl['_items'][0]['_etag']
            else:
                if not silent:
                    print("-- {}:{} missing.".format(index_source, source_id))
                return False, None, None
        else:
            print(10*'-')
            print(ret.status_code, ret.text)
            print(10*'-')

    def get_asset(self, id, silent=True, endpoint='assets'):
        ret = requests.get('http://localhost:5001/api/v2/{}/{}'.format(endpoint, id), headers=self.headers, verify=False)
        if ret.status_code == 200:
            pl = json.loads(ret.text)
            return pl
        else:
            return None
            print(10*'-')
            print(ret.status_code, ret.text)
            print(10*'-')

    def insert_asset(self, asset, endpoint='assets'):
        ret = requests.post(f'http://localhost:5001/api/v2/{endpoint}', data=json.dumps(asset), headers=self.headers)
        if ret.status_code != 201:
            print('\t {}'.format(ret.text))
            print(10*'-')
            print(json.dumps(asset, indent=4))
            print(10*'+')
            return False
        else:
            return True
    
    def update_asset(self, asset, id, etag, endpoint='assets'):
        _headers = self.headers
        _headers['If-Match'] = etag
        ret = requests.patch(f'http://localhost:5001/api/v2/{endpoint}/{id}', data=json.dumps(asset), headers=_headers)
        if ret.status_code != 200:
            print('\t {}'.format(ret.text))
            print(10*'-')
            print(asset)
            print(10*'+')
            return False
        else:
            return True

    def _cleanup_author_name(self, fn):
        aa = fn.strip()
        # fix author format: Lastname, Firstname1, Firstname2 etc.
        _at = aa.split(',')
        if len(_at) > 2:
            _an = '{}, {}'.format(_at[0], ' '.join(_at[1:]).strip())
            aa = _an
        # fix author format; before marriage  lastname by removing '(before_mariage_lastname)'
        aa = re.sub(r' \([^)]*\)', '', aa)
        return aa

    def generate_asset_from_tsv(self, entry, is_uvt=False, use_authors=None, match_authors=True):

        
        '''
        title: TI  Document Title (string)
        abstract: AB	Abstract (string)
        keywords: DE  Author Keywords (delimiter ';')
        year: PY Year Published (format: YYYY)
                if empty -> EA Early Access (format: Mmm YYYY)
        authors:
            name: AF	Author Full Name (delimiter ';')
            affiliation: C1	Author Address (format [AF1;AF2;AF3] Institution;[AF1,AF2,AF3] Institution ...)
            index:
        index_source:
            index_type: wos
            index_id: UT	Accession Number
        publication_metadata:
            name: J9    29-Character Source Abbreviation
            issue: IS Issue
            volume: VL Volume
            cites: Z9 Total Times Cited Count
            pages: BP-Beginning Page -- EP-Ending Page
            type: PT  Publication Type (J=Journal; B=Book; S=Series; P=Patent)
            doi: if B -> D2	Book Digital Object Identifier (DOI)
            doi_b:    DI	Digital Object Identifier (DOI)
            issn: SN    International Standard Serial Number (ISSN)
            eissn: EI	Electronic International Standard Serial Number (eISSN)
            isbn: BN	International Standard Book Number (ISBN)
            other:
                wos_document_type: DT Document Type (https://images.webofknowledge.com/images/help/WOS/hs_document_type.html)
                wos_categories: WC	Web of Science Categories
                wos_research_areas: SC	Research Areas
                wos_conference_title: CT Conference title
                wos_book_series_title: SE Book Series Title
        '''

        asset = {}
        
        _source = entry['UT'].split(':')
        asset['index_source'] = {}
        asset['index_source']['source_name'] = _source[0]
        asset['index_source']['source_id'] = _source[1]
        
        asset['asset_id'] = str(uuid.uuid4())
        asset['title'] = entry.get('TI', '')
        asset['abstract'] = entry.get('AB', '')
        asset['asset_type'] = asset_types[entry['\ufeffPT']]
        _year = entry.get('PY', 0)
        if _year == '':
            if 'early access' in entry['DT'].lower():
                try:
                    _year = re.match(r'.*([1|2][0|9][0-9][0-9]).*', str(entry['EA'])).group(1)
                except:
                    _year = ''
        asset['year'] = _year
        asset['year'] = 0 if asset['year'] == '' else int(asset['year'])
        asset['keywords'] = []
        for keyword in entry['DE'].split(';'):
            asset['keywords'].append(keyword.strip())

        asset['people'] = []
        _afound = False
        _matched = 0
        if entry['AF'] != '':
            author_lst = entry['AF'].split(';')
            adb = {}
            _idx = 1
            dba = None
            if use_authors:
                _fp = os.path.join(use_authors, f'wos-authors-{_source[1]}')
                if os.path.exists(_fp):
                    try:
                        dba = json.load(open(_fp))
                    except:
                        print(f'!!! author file for {_source[1]} exception')
                else:
                    print(f'!!! author file for {_source[1]} not found')
            for a in author_lst:
                pers = {}
                aa = self._cleanup_author_name(a)
                pers['name'] = aa
                pers['affiliation'] = []
                pers['index'] = _idx
                if dba and len(dba) > 0:
                    pers['source_author_id'] = dba[_idx-1]['source_author_id']
                adb[pers['name']] = pers
                _idx += 1
            if re.match(r'\[(.*?)\]', entry['C1']):
                affiliations = entry['C1'].split('[')[1:]
                for af in affiliations:
                    _authors, _afls = af.split(']')
                    for aut in _authors.split(';'):
                        try:
                            adb[self._cleanup_author_name(aut)]['affiliation'].append(_afls.strip().replace(';', '').upper())
                        except:
                            pass
            
            if match_authors:
                # verify if is UVT user
                if len(adb.keys()) < 200:
                    for a in adb.keys():
                        h = self.headers
                        h['fullname'] = adb[a]['name']
                        ret = requests.get('http://localhost:5001/api/v2/services/getUrapIdByName', headers=h)
                        if ret.status_code == 200:
                            data = json.loads(ret.text)
                            adb[a]['person_id'] = data['urap_id'][0]
                            _matched = 1
                        else:
                            _ua = [x.split(',')[0] for x in adb[a]["affiliation"]]
                            for e in uvt_names:
                                if e.upper() in _ua:
                                    print(f'++++++:{adb[a]["name"]}:{_ua}')
                            
                else:
                    for a in adb.keys():
                        if 'gravila' in adb[a]['name'].lower():
                            adb[a]['person_id'] = 'ba226244-5081-5189-be9f-a3ef2d2f99e3'
                            _matched = 1
                        if 'frincu' in adb[a]['name'].lower():
                            adb[a]['person_id'] = '5c49e0ac-8080-5baa-b3d3-1837101903f6'
                            _matched = 1
                    if not _matched:
                        print(f'ignored:{len(adb.keys())}:{entry["UT"]}')
                # end verify is UVT user
            for a in adb.keys():
                asset['people'].append(adb[a])
                        

        '''
        if not _afound or len(asset['people']) < len(entry['BF'].split(';')) or len(asset['people']) < len(entry['BA'].split(';')): 
            if entry['AF'] == '':
                _af = entry['BF']
            else:
                if len(entry['BF'].split(';')) > len(entry['AF'].split(';')):
                    _af = entry['BF']
                elif len(entry['BA'].split(';')) > len(entry['AF'].split(';')):
                    _af = entry['BA']
                else:
                    _af = entry['AF']
            _authors = _af.split(';')
            if entry['C1'] == '':
                _affiliations = len(_authors)*['']
            else:
                _affiliations = entry['C1'].split(';')
                if len(_authors) != len(_affiliations):
                    _affiliations = len(_authors)*['']
            _index = 1
            asset['people'] = []
            for _author in _authors:
                pers = {}
                pers['name'] = _author.strip()
                pers['affiliation'] = [_affiliations[_index-1].strip()]
                pers['index'] = _index
                asset['people'].append(pers)
                _index += 1        
        '''

        #asset['is_uvt'] = _matched
        asset['is_uvt'] = 1 if is_uvt else 0
        
        pub_metadata = {}
        pub_metadata['doi'] = entry['DI']
        pissn = entry['SN'].strip().replace(' ','')
        pub_metadata['issn'] = pissn
        pisbn = entry['BN'].strip().replace(' ','')
        pub_metadata['isbn'] = [] if len(pisbn) == 0 else pisbn.split(';')
        peissn = entry['EI'].strip().replace(' ','')
        pub_metadata['eissn'] = peissn
        pub_metadata['eisbn'] = []
        pub_metadata['issue'] = entry['IS']
        pub_metadata['volume'] = entry['VL']
        pub_metadata['pages'] = entry['BP'] + '-' + entry['EP']
        try:
            pub_metadata['type'] = publication_types[entry['\ufeffPT']][entry['DT']]
        except KeyError:
            print('error: ', entry['\ufeffPT'],'-', entry['DT'], f' -- {entry}')

        pub_metadata['cites'] = []
        pub_metadata['others'] = {}

        pub_metadata['others']['journal_abbreviation'] = entry['J9']
        pub_metadata['others']['journal_name'] = entry['JI']
        pub_metadata['others']['wos_category'] = []
        _wc = entry['WC'].strip().replace(',', 'C0MM4').replace('&', '4ND').replace('; ', ';').replace(' ', '_').split(';')
        pub_metadata['others']['wos_category'] = _wc
        pub_metadata['others']['publication_date'] = '{} {}'.format(entry['PD'],entry['PY']).strip()
        pub_metadata['others']['book_series_title'] = entry['SE']
        pub_metadata['others']['book_series_subtitle'] = entry['BS']

        asset['publication_metadata'] = pub_metadata
        if _matched != 1 and match_authors:
            print('error-match-{}-{}'.format(_matched, entry['UT']))
        return asset, _matched


def get_name_combinations(name_string, with_name_split=True, with_initial=False):
    names = []
    if name_string == '':
        return names
    if with_name_split:
        name_string = name_string.replace('-', ' ')
    if len(name_string.split(' ')) > 1:
        _fs = name_string.split(' ')
        names.append(name_string.lower())
        _initial = ''
        for e in _fs:
            names.append(e.lower())
            if with_initial:
                names.append(e[0].lower() + '.')
    else:
        names.append(name_string.lower())
        if with_initial:
            names.append(name_string[0].lower() + '.')
    return names