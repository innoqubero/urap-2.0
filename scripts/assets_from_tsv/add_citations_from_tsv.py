#!/usr/bin/env python

import os, sys
import csv, json, re

from helpers import AssetFromTSV

if len(sys.argv) != 4:
    print('Usage: {} TOKEN UPLOAD[TRUE|FALSE] CITATIONS_PATH'.format(sys.argv[0]))
    sys.exit(1)

_token = sys.argv[1]
if sys.argv[2].lower() == 'true':
    _upload = True
else:
    _upload = False
_cpath = sys.argv[3]


_helper_asset = AssetFromTSV(_token)

if not os.path.exists(_cpath):
    print('[!!] Specified path {} doesn\'t exists!'.format(_cpath))
    sys.exit(1)

_files = os.listdir(_cpath)
cits = 1
paps = 1
for f in _files:
    #_wos_id = f.split('-')
    try:
        _wid = re.match(r'.*citations-([a-zA-Z0-9]+).tsv.*', f).group(1)
    except:
        print(f)
        raise
    _wos_id = ['WOS', _wid]
    print(f'[--] {":".join(_wos_id)} ({paps})/({cits})')
    _e, _id, _etag = _helper_asset.check_exists_wos_id(_wos_id[0], _wos_id[1], silent=True)
    if not _e:
        print('{} not found.'.format(_wos_id))
    else:
        _tsv_file = os.path.join(_cpath, f)
        with open(_tsv_file) as fp:
            db = csv.DictReader(fp, dialect='excel-tab')
            citations = []
            citations_ids = []
            for entry in db:
                try:
                    _wos_id1 = entry['UT'].split(':')
                    _cfound = ''
                    _ec, _, _ = _helper_asset.check_exists_wos_id(_wos_id1[0], _wos_id1[1], silent=True)
                    if not _ec:
                        cits += 1
                    else:
                        citations_ids.append(':'.join(_wos_id1))
                except:
                    print(f'[!!!] error: {_tsv_file}:{entry["TI"]}')
                    continue
                try:
                    if not _ec:
                        _citation, _ = _helper_asset.generate_asset_from_tsv(entry, match_authors=False)
                        if _upload:
                            ret = _helper_asset.insert_asset(_citation)
                            if ret:
                                citations_ids.append(':'.join(_wos_id1))
                        citations.append(_citation)
                except:
                     print(f'[!!!] error: {_tsv_file}:{entry["TI"]}:{entry["UT"]}')
        if _upload:
            _patch = {'publication_metadata': {'cites': []}}
            _asset = _helper_asset.get_asset(_id)
            if type(_asset['publication_metadata']['cites']) == int:
                _patch['publication_metadata']['cites'] = citations_ids
            elif type(_asset['publication_metadata']['cites']) == list:
                _asset['publication_metadata']['cites'] = []
                _patch['publication_metadata']['cites'] = list(set(citations_ids)|set(_asset['publication_metadata']['cites']))
            else:
                raise Exception('[!!] Current publication_metadata->cites format not recognized!')
            print(_patch)
            _ret = _helper_asset.update_asset(_patch, _asset['_id'], _asset['_etag'])
            if not _ret:
                print('[!!] {} patching failed!'.format(f))
        paps += 1

    print(f'Citations: {len(citations_ids)}')
    print(f'New citations: {len(citations)}')

    print('---------------------------------------')