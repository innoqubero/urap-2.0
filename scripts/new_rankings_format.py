import json
import sys, os


def modify_format(rankings_fh):
    data = json.loads(rankings_fh.read())
    
    for entry in data:
        uefis_rankings = entry['rankings']['uefiscdi']
        for k,v in uefis_rankings.items():
            if k == 'zone':
                for idx, wos_cat in enumerate(v):
                    for category, ranking in wos_cat.items():
                        _new_data = []
                        for year, rank in ranking.items():
                            _new_data.append({'year': int(year), 'if': rank})
                        uefis_rankings['zone'][idx] = {category : _new_data}
                continue
            _new_data = []
            for elem in v:
                for key,val in elem.items():
                    _new_data.append({'year': int(key), 'if': val})
                    
            uefis_rankings[k] = _new_data

    json.dump(data, open('new_rankings.json', 'w'), indent=4)


modify_format(open(sys.argv[1]))
