#!/usr/bin/env python
'''
    This script is used to:
        1. combine in one JSON file all the crawled individual papers using woscli
        2. generate a TSV file in form of: name, dais_id, affiliation
'''

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'urap/src'))

import glob
import json
from urap.woslib import Helpers

# directory with papers individual crawled files (json)  using woscli
_db_dir = 'wos_db/db/wos/raw/papers'

_out_db_file = 'out.json'

persons = {}
uvt_wos_names = Helpers().get_uvt_names()

def _help():
    print('[!!] Usage: {} generate|load filename'.format(sys.argv[0]))
    sys.exit(1)

if len(sys.argv) <= 1:
    _help()

print ('Database directory:', _db_dir)


if sys.argv[1] == 'generate':
    idx = 0
    for f in glob.glob(os.path.join(_db_dir, '*.json')):
        with open(f, 'r') as fp:
            _jdb = json.load(fp)
        for a in _jdb['author']:
            is_uvt = False
            for af in a['affiliation']:
                if af.split(',')[0].upper() in uvt_wos_names:
                    is_uvt = True
            if len(a['dais_id']) > 3 and is_uvt:
                if a['dais_id'] in persons.keys():
                    if a['name'] not in persons[a['dais_id']]['names']:
                        persons[a['dais_id']]['names'].append(a['name'])
                    for af in a['affiliation']:
                        if af.lower() == af.split(',')[0].lower():
                            if af not in persons[a['dais_id']]['institutions']:
                                persons[a['dais_id']]['institutions'].append(af)
                else:
                    persons[a['dais_id']] = {}
                    persons[a['dais_id']]['names'] = [a['name']]
                    persons[a['dais_id']]['institutions'] = []
                    persons[a['dais_id']]['institutions'].extend(a['affiliation'])
        idx += 1
    json.dump(persons, open(_out_db_file, 'w'), indent=4)
    print('[--] output json file: {}'.format(_out_db_file))
    sys.exit(0)
if sys.argv[1] == "load":
    db = json.load(open(sys.argv[2]))
    plst = []
    for k,v in db.items():
        p = {}
        p['dais_id'] = k
        if len(v['names']) > 0:
            lst = [-1, -1]
            for n in v['names']:
                if len(n.split(" ")) > 2:
                    lst = [0, n]
                elif len(n.split(" ")) == 2:
                    if len(n.split(" ")[1]) > 4:
                        if lst[0] != 0:
                            lst = [1, n]
                    else:
                        if lst[0] not in [0, 1]:
                            lst = [2, n]
                else:
                    print('!!:EROARE:{}'.format(k))
            p['name'] = lst[1]
        else:
            p['name'] = v['names'][0]
        p['affiliation'] = ';'.join(v['institutions'])
        plst.append(p)
    import csv
    with open('out.tsv', 'w') as fp:
        tsv_w = csv.writer(fp, delimiter='\t')
        tsv_w.writerow(['nume', 'dais_id', 'afiliere'])
        for p in plst:
            tsv_w.writerow([p['name'], p['dais_id'], p['affiliation']])
    print('[--] generated TSV: out.tsv')
    sys.exit(0)

_help()