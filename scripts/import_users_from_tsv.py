#!/usr/bin/env python

import os, sys
import csv, json

import requests
import uuid
import unidecode

with open(sys.argv[2]) as fp:
    db = csv.DictReader(fp, dialect='excel-tab')
    people = []
    departments = {}
    didx = 1
    pidx = 1
    for e in db:
        person = {}
        _dep = unidecode.unidecode(e['Departament']).lower()
        if _dep not in departments.keys():
            departments[_dep] = {}
            departments[_dep]['id'] = didx
            didx += 1
        person['urap_id'] = str(uuid.uuid5(uuid.NAMESPACE_URL, 'mailto:{}'.format(e['Email'])))
        person['lastname'] = e['Nume'].lower()
        person['firstname'] = ''
        if len(e['Prenume1']) > 0:
            person['firstname'] += e['Prenume1'].lower()
        if len(e['Prenume2']) > 0:
            person['firstname'] += ' ' + e['Prenume2'].lower()
        if len(e['Prenume3']) > 0:
            person['firstname'] += ' ' + e['Prenume3'].lower()
        person['email'] = e['Email'].strip('>').strip('<')
        person['password'] = '!!'
        person['roles'] = ['user']
        person['position'] = e['Grad'].lower()
        person['department'] = departments[_dep]['id']
        person['faculty'] = 0
        person['identifiers'] = {}
        person['identifiers']['wos'] = {}
        if len(e['OLD_WOS_ID']) > 4:
            person['identifiers']['wos']['ids'] = e['OLD_WOS_ID'].strip().split(';')
        else:
            person['identifiers']['wos']['ids'] = []
        person['identifiers']['wos']['names'] = []
        person['identifiers']['wos']['affiliations'] = []
        person['identifiers']['scopus'] = {}
        if len(e['SCOPUS_ID']) > 4:
            person['identifiers']['scopus']['ids'] = e['SCOPUS_ID'].strip().split(';')
        else:
            person['identifiers']['scopus']['ids'] = []
        person['identifiers']['scopus']['names'] = []
        person['identifiers']['scopus']['affiliations'] = []
        person['identifiers']['researcher_ids'] = []
        person['identifiers']['orcid_ids'] = []
        person['identifiers']['other'] = {}
        person['identifiers']['cnatdcu_ds'] = ''
        people.append(person)

json.dump(departments, open('departments.json', 'w'), indent=2)
json.dump(people, open('people.json', 'w'), indent=2)

headers = {'Content-type': 'application/json; charset=utf-8', 'Authorization': sys.argv[1]}

print('Total persons:', len(people))
for k, v in departments.items():
    ret = requests.post('http://localhost:5001/api/v2/departments', data=json.dumps({'id': v['id'], 'name': k}), headers=headers)
#    if ret.status_code != 201:
#        print(v, k)
#        print('\t {}'.format(ret.text))

for p in people:
    ret = requests.post('http://localhost:5001/api/v2/people', data=json.dumps(p), headers=headers)
    if ret.status_code != 201:
        #print(p)
        print('\t {}'.format(ret.text))
        
