#!/bin/env python

import glob, os
import json, csv

univ_names = [
   'AGR UNIV TIMISOARA',
   'FAC MATH',
   'IND CHEM ENVIRONM ENGN FAC',
   'TIBISCUS UNIV TIMISOARA',
   'TIMISOARA UNIV',
   'UNIT WEST',
   'UNIV DIN TIMISOARA',
   'UNIV OUEST TIMISOARA',
   'UNIV TIMIS OARA',
   'UNIV TIMISOARA',
   'UNIV TIMISOARA W',
   'UNIV TIMISOARA WEST',
   'UNIV VEST',
   'UNIV VEST DIN TIMISOARA',
   'UNIV VEST TIMISOARA',
   'UNIV W',
   'UNIV W TIMISOARA',
   'UNIV W TOMISOARA',
   'UNIV WEST',
   'UVT',
   'VEST UNIV TIMISOARA',
   'W TIMISOARA UNIV',
   'W UNIV',
   'W UNIV RIMISOARA',
   'W UNIV TIMIOARA',
   'W UNIV TIMIS OARA',
   'W UNIV TIMISAORA',
   'W UNIV TIMISOARA',
   'W UNIV TIMISOARA V PARVAN',
   'W UNIV TIMISOARRA',
   'W UNIV TIMISOUARA',
   'W UNIV TIMISOURA',
   'W UNIV TIMSOARA',
   'W UNIV TINDSOARA',
   'W UNIV TUNISOARA',
   'W UNV TIMISOARA',
   'W VIRGINIA UNIV TIMISOARA',
   'WEAT UNIV TIMISOARA',
   'WEST UNIV',
   'WEST UNIV TIMISOARA',
   'WESTERN UNIV',
   'WESTERN UNIV TIMISOARA'
]


_db_dir = 'wos_db_tsv'

persons = {}

# Maybe we should filter by affiliation containing "West" as a substring?
def get_data():
    with open('wos_db_tsv/uvt-1_501.tsv', 'U') as fp:
        db = csv.DictReader(fp, dialect='excel-tab')
        for entry in db:
            authors = entry['AF'].split(';')
            affiliations = entry['C1'].split('[')[1:]
            emails = entry['EM']
            orcid_ids = entry['OI'].split(';')
            researcher_ids = entry['RI'].split(';')

            for affil in affiliations:
                try:
                    _authors, _affil = affil.split(']')
                    _authors = _authors.split(';')
                    for author in _authors:
                        author = author.strip()
                        affil = _affil.strip().strip(';')
                        persons[author] = {'author': author,
                                           'affil': affil}
                except ValueError:
                    pass

                for orcid_id in orcid_ids:
                    try:
                        _author, _id = orcid_id.split('/')
                        _author = author.strip()
                        persons[_author]['orcid_id'] = _id
                    except ValueError:
                        pass

                for researcher_id in researcher_ids:
                    try:
                        _author, _id = researcher_id.split('/')
                        _author = author.strip()
                        persons[_author]['researcher_id'] = _id
                    except ValueError:
                        pass


def export_to_csv():
    with open('output.csv', 'w') as fp:
        header = ['author','affil', 'orcid_id', 'researcher_id']
        db = csv.DictWriter(fp, fieldnames=header)
        db.writeheader()

        for key, value in persons.items():
            if value['affil'].split(',')[0].upper() in univ_names:
                db.writerow(value)

get_data()
export_to_csv()

