import re
import ast
import math
import sys, os
import argparse
import requests

from lxml.html import fromstring, tostring

parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]),
                                 description='BID - Eigenfactor JournalRank PDF Ranking Importer.',
                                 usage='%(prog)s [options]')
parser.add_argument('--ISSN', type=str, required=True, help='')
parser.add_argument('--output-file', type=str, required=True, help='JSON output file to write data to')
parser.add_argument('--output-type', type=str, required=True, help='JSON/CSV')
args = parser.parse_args()


def extract_to_json(score):
    fname = args.output_file
    # older format, does not match the one used for csv files!
    import json

    if os.path.isfile(fname):
        _scores = json.load(open(fname))

        for key, value in score.items():
            key = str(key)
            
            if key in _scores[args.ISSN].keys() and not math.isnan(_scores[args.ISSN][key]):
                print(f"{key} matches {_scores[args.ISSN][str(key)]} - {value}")
                pass
            else:
                _scores[args.ISSN][str(key)] = float("{0:.3f}".format(value))
    else:
        _scores = {args.ISSN: float("{0:.3f}".format(score))}

    json.dump(_scores, open(fname, 'w'), indent=4)


def get_scores_from_issn_list(issn_list):
    for issn in issn_list:
        scores[issn] = get_scores(issn)
        

def get_scores(issn):
    _base_url = f"http://eigenfactor.org/projects/journalRank/plot1.php?issn={issn}"
    response = requests.get(_base_url)
    tree = fromstring(response.content)
    script = tostring(tree.find(".//body/script")).decode('utf-8')
    _score = {}

    try:
        _metric = ast.literal_eval(re.search(r'l2 = (.*?);', script)[0][5:-1])

        for year, value in _metric:
            _score[year] = value
            
    except SyntaxError:
        exit()
        
    return _score
    
if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)
else:
    extract_to_json(get_scores(args.ISSN))

