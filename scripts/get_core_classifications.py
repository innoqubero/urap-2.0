#!/usr/bin/env python

import json
import time
import requests
from bs4 import BeautifulSoup, Tag

"""
Get core conferences classifications using the core.edu.au portal
and store them into JSON
"""

MAIN_CORE_URL ='http://portal.core.edu.au/jnl-ranks/?search=&by=all&source=ERA2010%0D%0A&sort=atitle&page='
CORE_CONFERENCE_PAGE_URL = 'http://portal.core.edu.au/jnl-ranks/'

core_conference_pages = []
conference_rankings = {}


def get_ranking(page_url):
    res = requests.get(page_url)
    doc = res.text if res.status_code == 200 else False
    soup = BeautifulSoup(doc, 'html.parser')
    entry = {}

    for row in soup.find('div', {'class': 'detail'}):
        r = str(row.encode('utf-8')).split('>')
        if len(r) > 1:
            try:
                key, value = r[1].split('<')[0].split(':')
                key = key.strip()
                value = value.strip()
                entry[key] = value

                if key == 'ISSN':
                    conference_rankings[value] = entry
            except ValueError:
                pass


def get_conference_ids():
    for i in range(1,14):
        res = requests.get(f'{MAIN_CORE_URL}{i}')
        doc = res.text if res.status_code == 200 else False
        soup = BeautifulSoup(doc, 'html.parser')

        for row in soup.findAll('tr', onclick=True):
            if isinstance(row, Tag):
                try:
                    core_conference_pages.append(f"{CORE_CONFERENCE_PAGE_URL}{row['onclick'].split('/')[2]}")
                except KeyError:
                    pass

get_conference_ids()

start = time.time()
for conference_url in core_conference_pages:
    get_ranking(conference_url)
end = time.time()

with open('core-classifications.json', 'w') as f:
    f.write(json.dumps(conference_rankings))

print(f'Getting rakings took: {end-start}')
# print(conference_rankings)