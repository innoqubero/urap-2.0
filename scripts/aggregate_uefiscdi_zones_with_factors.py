import json
import sys, os
import argparse

parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]), description='BID - UEFISCDI PDF Ranking Importer.',
                                 usage='%(prog)s [options]')

parser.add_argument('--zones', type=str, required=True, help='Path to zone export (exported by extract_uefiscdi_zones.py)')
parser.add_argument('--collection-data', type=str, required=True, help='Path to metrics export (exported by get_score_from_uefis_pdf.py)')
parser.add_argument('--output-file', type=str, required=True, help='JSON output file to write aggregated data to')
args = parser.parse_args()


if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)


def agregate():
    _zone_data = json.load(open(args.zones, 'r'))
    _ranking_data = json.load(open(args.collection_data, 'r'))
    _new_data = []

    for key in _zone_data.keys():
        for entry in _ranking_data:
            if key in entry.keys():
                _new_entry = {}

                _new_entry['issn'] = key
                _new_entry['revista'] = _zone_data[key]['revista'].replace('\r', ' ')

                del _zone_data[key]['revista']

                _new_entry['rankings'] = entry[key]['rankings']
                _new_entry['rankings']['uefiscdi']['zone'] = []

                for k, v in _zone_data[key]['rankings'].items():
                    _new_entry['rankings']['uefiscdi']['zone'].append({k: v})

                _new_data.append(_new_entry)

    json.dump(_new_data, open(args.output_file, 'w'), indent=4)

if __name__ == '__main__':
    agregate()
