import re
import json
import sys, os
import argparse

from tabula import read_pdf

parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]), description='BID - UEFISCDI PDF Ranking Importer.',
                                 usage='%(prog)s [options]')
parser.add_argument('--pdf-file', type=str, required=True, help='PDF file path to import')
parser.add_argument('--output-file', type=str, required=True, help='JSON output file to write data to')
args = parser.parse_args()


if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)

    
fname = args.output_file
input_f = args.pdf_file.split('.pdf')[0]
year = os.path.basename(input_f).split('_')[1]
df = read_pdf(args.pdf_file, pages='all', lattice=True)


def clean_wos_category(wos_cat):
    return "".join(wos_cat.strip().replace('\r', ' ').replace('&', '4ND').replace(',', 'C0MM4').replace(' ', '_'))


def _extract_to_json():
    print(f"Extracting {args.pdf_file} to json file")

    if os.path.isfile(fname):
        _classifications = json.load(open(fname))
    else:
        _classifications = {
        }

    for page in df:
        for index, row in page.iterrows():
            _wos_category, _index, _revista,  _issn, _zona, _top = row[0:6]
            _wos_category = clean_wos_category(_wos_category)

            if _index == 'SCIENCE' or _index == 'science':
                _index = 'SCIE'

            if _issn in _classifications.keys():
                if _wos_category not in _classifications[_issn]['rankings'].keys():
                    _classifications[_issn]['rankings'][_wos_category] = {}

                _classifications[_issn]['rankings'][_wos_category][year] = {
                    'index': _index,
                    'zona': _zona,
                    'top': _top
                }
            else:
                _classifications[_issn] = {
                    'rankings': {},
                    'revista': _revista,
                }

                if _wos_category not in _classifications[_issn]['rankings'].keys():
                    _classifications[_issn]['rankings'][_wos_category] = {}

                _classifications[_issn]['rankings'][_wos_category][year] = {
                    'index': _index,
                    'zona': _zona,
                    'top': _top
                }

    json.dump(_classifications, open(fname, 'w'), indent=4)
    print(f'[--] DONE. Extracted {len(_classifications)} entries, dumped to {fname}')


if __name__ == '__main__':
    _extract_to_json()

