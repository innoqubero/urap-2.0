#!/usr/bin/env python3

import json
import csv

ais_file = open('/home/alex/urap/data/uefiscdi_scores/AIS/uefiscdi_ais.json')
rif_file = open('/home/alex/urap/data/uefiscdi_scores/RIF/uefiscdi_rif.json')
ris_file = open('/home/alex/urap/data/uefiscdi_scores/RIS/uefiscdi_ris.json')

ais_data = json.loads(ais_file.read())
rif_data = json.loads(rif_file.read())
ris_data = json.loads(ris_file.read())

data = {}

def extract_data_from_json(json_data, metric):
    for key,value in json_data.items(): 
        scores = {} 
        scores[metric] = [] 
 
        for v in range(2005, 2020): 
            try: 
                scores[metric].append({str(v): value[str(v)]})
            except KeyError: 
                pass

        if key in data.keys():
            data[key]['rankings']['uefiscdi'][metric] = scores[metric]
        else:
            data[key] = {'rankings': {'uefiscdi': scores}}


abbrevs = open('/home/alex/urap/urap-scripts/import.csv')

def get_abbrev():
    reader = csv.reader(abbrevs)
    # jf = json.loads(open(input_json, 'r').read())

    for row in reader:
        if row[0] in data.keys():
            data[row[0]]['abbreviation'] = row[1]

    
extract_data_from_json(ais_data, 'AIS')
extract_data_from_json(rif_data, 'RIF')
extract_data_from_json(ris_data, 'RIS')
get_abbrev()

out = []
for k,v in data.items():
    out.append({k:v})
        
json.dump(out, open('output.json', 'w'), indent=4)


