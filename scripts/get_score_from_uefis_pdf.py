from tabula import read_pdf
import sys, os
import argparse

# This script assumes we have a naming convention for input files like IF_year.pdf!!!

parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]), description='BID - UEFISCDI PDF Ranking Importer.',
                                 usage='%(prog)s [options]')
parser.add_argument('--pdf-file', type=str, required=True, help='PDF file path to import')
parser.add_argument('--output-file', type=str, required=True, help='JSON output file to write data to')
parser.add_argument('--output-type', type=str, required=True, help='JSON/CSV')
args = parser.parse_args()


if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)

    
fname = args.output_file
input_f = args.pdf_file.split('.pdf')[0]
ranking_year = os.path.basename(input_f).split('_')[1]
df = read_pdf(args.pdf_file, pages='all')


def _extract_to_csv():
    print("Extracting to csv file")
    import csv

    if os.path.isfile(fname):
        csv_out = open(args.output_file, 'a')
    else:
        csv_out = open(args.output_file, 'w')
        
    fields=['issn', 'journal_name', 'score', 'year']
    writer = csv.DictWriter(csv_out, fields)
    writer.writeheader()

    for page in df:
        for index, row in page.iterrows():
            _journal_name, _issn = row[0], row[1]
            
            try:
                _score = row[2]
            except KeyError:
                _score = 0.0

            # remove carriage returns from some journal names
            #     _journal_name = _journal_name.replace(r'\r',' ')
            
            if type(_score) is not str:
                _score = "{0:.3f}".format(_score)
                
            writer.writerow({'issn': _issn, 'journal_name': _journal_name, 'score': _score, 'year': ranking_year})

    csv_out.close()
    print(f'[--] DONE. Dumped output to {fname}')

    
def _extract_to_json():
    # older format, does not match the one used for csv files!
    print(f"Extracting {args.pdf_file} to json file")
    import json
    
    try:
        if os.path.isfile(fname):
            _scores = json.load(open(fname))
        else:
            _scores = {
            }

        for page in df:
            for index, row in page.iterrows():
                _journal_name, _issn, = row[0], row[1]

                try:
                    _score = row[2]
                    if type(_score) == str:
                        try:
                            _score = float(_score)
                        # The score cell might be "Not Available" instead of being empty, so we can't cast to float
                        except ValueError:
                            _score = 0.0
                    else:
                        _score = float("{0:.3f}".format(_score))
                except (KeyError, IndexError):
                    _score = 0.0

                if _issn in _scores.keys():
                    _scores[_issn][ranking_year] = _score
                else:
                    journal = {}
                    journal['journal'] = _journal_name
                    journal[ranking_year] = _score
                    _scores[_issn] = journal

        json.dump(_scores, open(fname, 'w'), indent=4)
        print(f'[--] DONE. Extracted {len(_scores)} entries, dumped to {fname}')
        
    except:
        raise


if args.output_type == 'json':
    _extract_to_json()
elif args.output_type == 'csv':
    _extract_to_csv()
