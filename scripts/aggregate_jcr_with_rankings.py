import json
import sys, os
import argparse

"""
This script will agregate JCR data (SCIE and SCSCI) with the custom rankings.json schema
to fill in the data we we're not able to obtain from UEFISCDI, namely impact factors from
year 1997 till 2015.
"""

parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]), description='BID - JCR with custom ranking schema importer.',
                                 usage='%(prog)s [options]')

parser.add_argument('--zones', type=str, required=True, help='Path to zone export (exported by extract_uefiscdi_zones.py)')
parser.add_argument('--collection-data', type=str, required=True, help='Path to directory containing JCR rankings')
parser.add_argument('--output-file', type=str, required=True, help='JSON output file to write aggregated data to')
args = parser.parse_args()


if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)


def agregate():
    rankings_data = json.load(open(args.zones, 'r'))

    for journal_year_file in os.listdir(args.collection_data):
        year = journal_year_file.split('-')[-1].split('.')[0]
        journal_data = json.load(open(os.path.join(args.collection_data, journal_year_file)))

        for entry in journal_data:
            try:
                issn = entry['issn']
            except KeyError:
                pass

            for ranking in rankings_data:
                if ranking['issn'] == issn:
                    try:
                        for ranking_year in ranking['uefiscdi']['AIS']:
                            if year in ranking_year.keys:
                                pass
                            else:
                                ranking['uefiscdi']['AIS'].append({
                                    'year': year,
                                    'if': entry['articleInfluenceScore']
                                })
                    except KeyError:
                        pass

    json.dump(rankings_data, open(args.output_file, 'w'), indent=4)

if __name__ == '__main__':
    agregate()