# Developement setup

## MongoDB Installation

Install a MongoDB database and import the `urap` database (with username `urap` and password `urap.application`).

```
mongo
use urap
db.createUser({user: "urap", pwd: passwordPrompt(), roles: [{role: "readWrite", db: "urap"}]})
quit()
```

Edit `/etc/mongod.conf` and enable security:

```
security:
  authorization: enabled
```

Restart the `mongod` daemon.

## Setup the backend

1. (optional) Install Anaconda from https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
1. conda create --name urap-2.0 python=3.8
1. conda activate urap-2.0
1. cd /urap/repo
1. pip install -r requirements.txt

### Start URAP backend
1. conda activate urap-2.0
1. cd /urap/repo
1. cd src
1. (optional: adjust MongoDB connection information by using env variables: URAP_MONGO_HOST, URAP_MONGO_PORT, URAP_MONGO_USERNAME, URAP_MONGO_PASSWORD, URAP_MONGO_DBNAME')
1. ./urap-backend.py
1. (by default the backend listens to port 5001)

## Setup the frontend

1. Install latest perl distribution
1. gem install bundler jekyll
1. gem install bundler jekyll-paginate
1. cd /urap/repo
1. cd src/frontend
1. bundle exec jekyll serve -b /ui
1. (by default the backend listens to port 4000)

## Bind backend and frontend using haproxy (optional)
 1. apt install haproxy
 1. edit `/etc/haproxy/haproxy.cfg` and add at the end of the file:
```
frontend http-in
        #bind 172.17.53.74:80
        bind *:80
        option                  httplog
        option                  dontlognull
        option                  http-server-close
        option                  forwardfor       except 127.0.0.0/8

        acl urap-ui path_beg -i /ui
        use_backend urap-www if urap-ui

        default_backend http-www
backend http-www
        option http-server-close
        option forwardfor
        cookie PHPSESSID prefix
        server instance-1 127.0.0.1:5001 cookie A check maxconn 64

backend urap-www
        option http-server-close
        option forwardfor
        cookie PHPSESSID prefix
        server instance-1 127.0.0.1:4000 cookie A check maxconn 64
```
 3. systemctl start haproxy (or `/etc/init.d/haproxy start` on WSL2)
 4. Open `http://localhost/ui/` 

# MongoDB/Express stack (deprecated/not updated)

## Startup docker stack 
docker-compose -f docker-stack.yaml up -d

### Mongo endpoints

Mongo Database: localhost:27017 (default)
Mongo Express: http://localhost:8081/

### Mongo Clients (Ubuntu18.04)

apt -y install mongo-clients

(docker stack uses 'root/urap.application' pair for autentication)

mongo -u root -p urap.application --authenticationDatabase admin

## Shutdown docker stack
docker-compose -f docker-stack.yaml down

## Check docker stack status

docker-compose -f docker-stack.yaml top

# Start URAP API backend

./src/urap-backend.py

API is available at: http://localhost:5001/api/v2/

