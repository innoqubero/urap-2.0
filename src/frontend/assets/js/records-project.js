var asset = undefined;

var project_template = {
    'records_datepicker': {
        'type': 'datepicker',
        'validate': '^[0-9][0-9]-[1-2][0-9][0-9][0-9]$',
        'setter': (v) => { asset.setyear = v; },
        'getter': () => { return asset.getyear; } 
    },
    'records_title': {
        'type': 'text',
        'validate': '^.{5,}$',
        'setter': (v) => { asset.title = v; },
        'getter': () => { return asset.title;}
    },
    'records_keywords': {
        'type': 'tags',
        'setter': (v) => { asset.setkeywords = v; },
        'getter': () => { return asset.getkeywords;}
    },
    'records_projacronym': {
        'type': 'text',
        'validate': '^.{2,}$',
        'setter': (v) => { asset.project_metadata.acronym = v; },
        'getter': () => { return asset.project_metadata.acronym;}
    },
    'records_authority': {
        'type': 'text',
        'tooltip': 'Valoare minim acceptata: 3 caractere',
        'validate': '^.{3,}$',
        'setter': (v) => { asset.project_metadata.authority = v; },
        'getter': () => { return asset.project_metadata.authority;}
    },
    'records_datepicker_end': {
        'type': 'datepicker',
        'validate': '^[0-9][0-9]-[1-2][0-9][0-9][0-9]$',
        'setter': (v) => { asset.project_metadata.date_end = v; },
        'getter': () => { return asset.project_metadata.date_end;}
    },
    'records_contractno': {
        'type': 'text',
        'setter': (v) => { asset.project_metadata.contractno = v; },
        'getter': () => { return asset.project_metadata.contractno;}
    },
    'records_projtype': {
        'type': 'select',
        'validate': '^(research|education)$',
        'setter': (v) => { asset.project_metadata.type = v; },
        'getter': () => { return asset.project_metadata.type;}
    },
    'records_projbudget': {
        'type': 'text',
        'validate': '^\\d+(\\.\\d\\d?)?$',
        'setter': (v) => { asset.project_metadata.budget = v; },
        'getter': () => { return asset.project_metadata.budget;}
    },
    'records_projvi': {
        'type': 'text',
        'setter': (v) => { asset.project_metadata.vi = v; },
        'getter': () => { return asset.project_metadata.vi ;}
    },
    'records_members_list': {
        'type': 'members',
        'setter': (v) => { asset.setmembers = v; },
        'getter': () => { return asset.getmembers; }
    }
};

function records_projinit() {
    _init_asset('project');
    $('label[for~="records_datepicker"]').text('Data inceput proiect');
    $('#records_datepicker_end').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mm-yyyy',
        modal: true,
        //maxDate: new Date(),
        weekStartDay: 1
    });
    $('#records_projtype').selectpicker({
        width: '100%',
        noneSelectedText: 'Nicio selectie'
    }).append($('<option>').text('Valoare selectata').attr('value', '').attr('hidden', '').attr('selected', ''));
    for (var i=0;i<project_types.length;i++) {
        $('#records_projtype').append(new Option(project_types[i].name, project_types[i].id));
    }
    $('#records_projtype').selectpicker('refresh').selectpicker('val', 'research');
    $('#records_div_projmeta_1').show();
    $('#records_projtype').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        if($('#records_projtype').val() == 'research') {
            $('#records_div_projmeta_1').show();
        } else {
            $('#records_div_projmeta_1').hide();
        }
    });
    member_list_init(project_membership);
    $('#records_overlay').hide();
    records_controls(project_template);
}

records_projinit();