var asset = undefined;

var publication_template = {
    'records_woscat': {
        'type': 'wostags',
        'setter': (v) => { asset.setwoscategory = v; },
        'getter': () => { return asset.getwoscategory; }
    },
    'records_pubauthors': {
        'type': 'authors',
        'setter': (v) => { asset.setauthors = v; },
        'getter': () => { return asset.getauthors; }
    },
    'records_datepicker': {
        'type': 'datepicker',
        'validate': '^[0-9][0-9]-[1-2][0-9][0-9][0-9]$',
        'setter': (v) => { asset.setyear = v; },
        'getter': () => {  return asset.getyear; }
    },
    'records_title': {
        'type': 'text',
        'validate': '^.{5,}$',
        'setter': (v) => { asset.title = v; },
        'getter': () => {  return asset.title; }
    },
    'records_keywords': {
        'type': 'tags',
        'setter': (v) => { asset.setkeywords = v; },
        'getter': () => {  return asset.getkeywords; }
    },
    'records_pubdoi':{
        'type': 'text',
        //'validate': '^.{5,}$',
        'setter': (v) => { asset.publication_metadata.doi = v; },
        'getter': () => {  return asset.publication_metadata.doi; }
    },
    'records_pubtype': {
        'type': 'select',
        'validate': '^(journal|conference|workshop|chapter|book|poster)$',
        'setter': (v) => { asset.publication_metadata.type = v; },
        'getter': () => {  return asset.publication_metadata.type; }
    },
    'records_pubabs':{
        'type': 'text',
        'setter': (v) => { asset.abstract = v; },
        'getter': () => {  return asset.abstract; }
    },
    'records_pubname':{
        'type': 'text',
        'setter': (v) => { asset.publication_metadata.name = v; },
        'getter': () => {  return asset.publication_metadata.name; }
    },
    'records_pubvolume':{
        'type': 'text',
        'setter': (v) => { asset.publication_metadata.volume = v; },
        'getter': () => {  return asset.publication_metadata.name; }
    },
    'records_pubissue':{
        'type': 'text',
        'setter': (v) => { asset.publication_metadata.issue = v; },
        'getter': () => {  return asset.publication_metadata.issue; }
    },
    'records_pubpages':{
        'type': 'text',
        'setter': (v) => { asset.publication_metadata.pages = v; },
        'getter': () => {  return asset.publication_metadata.pages; }
    },
    'records_pubissn':{
        'type': 'text',
        'setter': (v) => { asset.setissn = v; },
        'getter': () => {  return asset.getissn; }
    },
    'records_pubeissn':{
        'type': 'text',
        'setter': (v) => { asset.seteissn = v; },
        'getter': () => {  return asset.geteissn; }
    },
    'records_pubisbn':{
        'type': 'text',
        'setter': (v) => { asset.setisbn = v; },
        'getter': () => {  return asset.getisbn; }
    },
    'records_pubeisbn':{
        'type': 'text',
        'setter': (v) => { asset.seteisbn = v; },
        'getter': () => {  return asset.geteisbn; }
    }
};

function records_pubinit() {
    _init_asset('publication');
    $('#records_pubtype').selectpicker({
        width: '100%',
        noneSelectedText: 'Nicio selectie'
    }).append($('<option>').text('Valoare selectata').attr('value', '').attr('hidden', '').attr('selected', ''));
    for (var i=0;i<publication_types.length;i++) {
        $('#records_pubtype').append(new Option(publication_types[i].name, publication_types[i].id));
    }
    $('#records_pubtype').selectpicker('refresh');
    records_controls(publication_template, ['records_pubauthors']);
}

records_pubinit();


