var listtable = undefined;
var uvt_suggestions = undefined;
var woscat_suggestions = undefined;

const capitalize = (s) => {
    if (typeof s !== 'string') return '';
    if (s.split(' ').length > 1) {
      _arr = s.split(' ');
      for (i=0;i<_arr.length;i++) {
        _arr[i] = _arr[i].charAt(0).toUpperCase() + _arr[i].slice(1);
      }
      return _arr.join(' ');
    }
    if (s.split('-').length > 1) {
      _arr = s.split('-');
      for (i=0;i<_arr.length;i++) {
        _arr[i] = _arr[i].charAt(0).toUpperCase() + _arr[i].slice(1);
      }
      return _arr.join('-');
    }
    return s.charAt(0).toUpperCase() + s.slice(1)
}

function emailValidator (value) {
    return (value && !!value.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) || null
}

function uuidValidator(value) { 
  return (value && !!value.match(/^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[89ABab][0-9A-Fa-f]{3}-[0-9A-Fa-f]{12}$/i)) || null
}

function getPaginationData(progress, url, headers = {}, items = []) {
  return new Promise((resolve, reject) => fetch(url, {headers: headers})
    .then(response => {
        if (response.status !== 200)  {
          throw `${response.status}: ${response.statusText}`;
        }
        response.json().then(data => { 
           items = items.concat(data._items);

          if(data._links.next) {
            progress && progress(items);
            getPaginationData(progress, '/'+data._links.next.href, headers, items).then(resolve).catch(reject)
          } else {
            resolve(items);
          }
        }).catch(reject);
    }).catch(reject));
}

function progressCallback(items) {
  console.log(`getPagination data: ${items.length} loaded`);
}

function userLogout() {
    console.log('[user] logout requested');
    cookieEngine.removeItem('urap_login');
    $(location).attr('href','login.html');
}

function getJsonFromUrl(url) {
  var _parts = url.split('?')
  if (_parts.length < 2) {
    console.log('getJsonFromUrl: url has not query!');
    return {'url': _parts[0], 'query': undefined};
  }
  if (_parts[1] == '') {
    console.log('getJsonFromUrl: query parts is empty!');
    return {'url': _parts[0], 'query': undefined};
  }
  var query = _parts[1];
  var result = {'url': _parts[0], 'query':{}};
  query.split("&").forEach(function(part) {
    var item = part.split("=");
    result['query'][item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}

function userLogin(email=undefined) {
    // dummy login to be used for user/email
    if (cookieEngine.hasItem("urap_login")) {
        cookieEngine.removeItem('urap_login');
    }

    var silviu = JSON.stringify({
      loggedIn: true,
      urap_id: "04c0db0a-9658-5d53-803b-637d27e57411",
      name: "Silviu Panica",
      token: "0461c54f-6dc0-44ef-959a-b91149de57a2",
      identifiers: {
        "wos": {
          "ids": ["2592156"],
          "names": [],
          "affiliations": []
        },
        "scopus": {
          "ids": [""],
          "names": [],
          "affiliations": []
        },
        "researcher_ids": [""],
        "orcid_ids": [""],
        "other": {},
        "cnatdcu_ds": ""
      }
    });
    var jebelean = JSON.stringify({ loggedIn: true, 
      urap_id: "61845980-0d17-550e-9ee5-6e9381cc3b95",
      identifiers: {
        "wos" : { "ids": ["654159"]},
        "scopus": { "ids": ["66024484690"]}
      },
      name: "Petru Jebelean", 
      token: "d5d99cfc-9e49-4e54-9d56-4afde1bf44cc"
    });
    var bunget = JSON.stringify({ loggedIn: true, 
      urap_id: "9d4588fd-aac1-53b5-8a05-a491b9b8fa72",
      identifiers: {
        "wos" : { "ids": ["4488726", "41497346"]},
        "scopus": { "ids": ["55604283100"]}
      },
      name: "Bunget Ovidiu Constantin", 
      token: "150e6ca3-77e7-4b42-811e-57823a70bfbb"
    });
    
    var dima = JSON.stringify({
      loggedIn: true,
      urap_id: "967d17b5-39cc-593f-9fb3-39a392130c6d", 
      identifiers: {
        "wos": {
          "ids": ["1103132"],
          "names": [],
          "affiliations": []
        },
        "scopus": {
          "ids": ["35076990300"],
          "names": [],
          "affiliations": []
        },
        "researcher_ids": [],
        "orcid_ids": [],
        "other": {},
        "cnatdcu_ds": ""
      },
      name: "Bogdan Dima",
      token: "c4a47532-99ee-4e1e-9733-d30879e55362"
    });
    cookieEngine.setItem('urap_login', dima);
}

function reset_field(id, type, suggestions) {
  if (type == 'input') { 
    $('#'+id).val(''); 
  }
  if (type == 'select') { 
    $('#'+id).val('default').selectpicker('refresh');
  }
  if (type == 'tags') {
    $('#'+id).val('').amsifySuggestags({
      'suggestions': suggestions
    });
  }
}

function input_tag_fill(id, list) {
  for (var i in list) {
    _val = $('input[id="'+id+'"]').val();
    if (_val == '') {
        $('input[id="'+id+'"]').attr('value', list[i]);
    } else {
        $('input[id="'+id+'"]').attr('value', _val+','+list[i]);
    }
  }
  $('input[id="'+id+'"]').amsifySuggestags({
    'suggestions': list
  })
}

function get_element_value(elem, type) {
  console.log(elem, type)
  if (type === 'tags' || type === 'wostags') {
    return $('#'+elem).val();
  } else if (type === 'text') {
    return $('#'+elem).val();
  } else if (type === 'select') {
    return $('#'+elem).val();
  } else if (type === 'datepicker') {
    return $('#'+elem).val();
  } else if (type === 'members') {
    var _members = [];
    $('#'+elem+' input[id*="records_member_name_"]').each(function(i){
      var _spl = $(this).attr('id').split('_');
      var _id = _spl[_spl.length-1];
      _members.push({'id': $('#records_member_name_'+_id).val(), 'type': $('#records_membership_'+_id).val(), 'name': $('#records_member_name_'+_id+' ~ div > div > span').text().trim()})
    });
    return _members;
  } else if (type === 'authors') {
    var _authors = [];
    var _ids = $('#'+elem).val();
    if (_ids === '') {
      return [];
    }
    var _ilst = _ids.split(',');
    for (var i in _ilst) {
      _a = {'id': (uuidValidator(_ilst[i])) ? _ilst[i] : '', 'type': '', 'name': $('#'+elem+' ~ div > div > span[data-val="'+_ilst[i]+'"]').text().trim()};
      _authors.push(_a);
    }
    return _authors
  }
}

function set_element_value(elem, value, type) {
  if (type === 'text' || type === 'datepicker' || type === 'tags') {
    $('#'+elem).val(value);
  }
  if (type === 'tags') {
    $('#'+elem).amsifySuggestags({suggestions: []});
  }
  if (type === 'select') {
    $('#'+elem).val(value);
    $('#'+elem).selectpicker('refresh');
  }
  if (type == 'wostags') {
    woscat_suggestions = []
    for (var e in wos_categories) {
      woscat_suggestions.push({
        tag: wos_categories[e].replace(/4ND/g, '&').replace(/C0MM4/g, ',').replace(/_/g, ' '), 
        value: wos_categories[e]
      });
    }
    $('#'+elem).val(value);
    $('#'+elem).amsifySuggestags({suggestions: woscat_suggestions});
  }

  if (type === 'authors') {
    var _ids = [];
    for (var i in value) {
      if (value[i].id != '') {
        _ids.push(value[i].id);
      } else {
        if (value[i].name.includes(',')) {
          var _n = value[i].name.trim().split(',');
          _ids.push(_n[1]+' '+_n[0]);
        } else {
          _ids.push(value[i].name);
        }
      }
    }
    $('#'+elem).val(_ids.join(','));
    $('#'+elem).amsifySuggestags({suggestions: people_suggestions});
  }
  if (type === 'members') {
    for (var i in value) {
      console.log(member_index)
      if (i > 0) {
        $('#records_member_add').click();
      }
      var _c = Number(i)+1;
      $('#records_member_name_'+_c).val(value[i].id);
      $('#records_membership_'+_c).val(value[i].type);
      $('#records_membership_'+_c).selectpicker('refresh');
    }
  }
}

function clean_element_value(elem, type, _suggestions=[]) {
  console.log('clean element: ', elem, type);
  if (type === 'tags' || type === 'authors') {
    $('#'+elem).val('');
    $('#'+elem).amsifySuggestags({suggestions: _suggestions});
  } else if (type === 'wostags') {
    $('#'+elem).val('');
    $('#'+elem).amsifySuggestags({suggestions: woscat_suggestions});
  } else if (type === 'text') {
    $('#'+elem).val('');
  } else if (type === 'select') {
    $('#'+elem).val('').selectpicker('refresh');
  } else if (type === 'datepicker') {
    $('#'+elem).val('');
  } else if (type === 'members') {
    $('button[id~="records_membership_del"]').each(function(){
      var _id = $(this).attr('id');
      $(this).click();
    });
    $('#records_member_name_1').val(udata['urap_id']);
    $('#records_member_name_1').amsifySuggestags({suggestions: _suggestions});
    $('#records_membership_1').val('').selectpicker('refresh');
  } 
}

function validationModal(title, message, custom=false, type="danger", callfn=function(){}) {
  if (custom) {
    bootbox.alert({title: title, message: '<div class="alert alert-'+type+'" role="alert">'+message+'</div>', callback:function(){callfn()}});
  } else {
    bootbox.alert({title: title, message: '<div class="alert alert-'+type+'" role="alert">Campul <span class="badge badge-secondary">'+message+'</span> este invalid.</div>', callback:function(){callfn()}});
  }
}

function confirmationModal(title, message, callfn=undefined) {
  bootbox.confirm({
    title: title,
    message: message,
    callback: function(result){ 
      if (result) {
        callfn();
      }
    }
  });
}

function records_controls(_template, tags=[]) {
  if (tags.length > 0) {
    getPaginationData(progressCallback, '/api/v2/people/?max_results=50', {'Authorization': udata['token']}).then(people_list => {
        people_uvt = people_list;
        people_suggestions = people_list.map((x) => ({tag: '<img src="assets/img/uvt.svg"> '+capitalize(x.firstname)+' '+capitalize(x.lastname), value: x.urap_id}));
        for (var j in tags) {
          $('#'+tags[j]).amsifySuggestags({suggestions: people_suggestions}, 'refresh');
        }    
    }).catch(console.error);
  }
  $('#records_add').click(function(e){
    var is_citation = false;
    var is_edit = false;
    var parent_asset_id = $(this).attr('parent_asset_id');

    console.log('records_add -> clicked');
    if ($('#citations_list_div').is(':visible')) {
      console.log('[records_add] this a citation modal');
      is_citation = true;
    }
    if (!records_validate(_template, is_citation)) {
        e.preventDefault();
        return;
    }
    var _etag = undefined;
    var _id = undefined;
    is_edit = Boolean(parseInt($('#records_add').attr('is_edit')));
    console.log('[records_add] is_edit: ', is_edit, $('#records_add').attr('is_edit'));
    if (is_edit) {
        _etag = asset._etag;
        _id = asset._id
    }
    var _alert = true;
    if (is_citation) 
      _alert = false;
    asset.prepareForInsert(is_edit);
    if (_etag && _id) {
        asset.insert(_etag, _id, _alert);
    } else {
        asset.insert(undefined, undefined, _alert);
    }
    if(is_citation) {
      console.log('[records_add] parent asset id: ' + parent_asset_id);
      asset.add_citation(parent_asset_id);
    }
  });
  $('#records_delete').click(function(e){
      console.log('delete: ', asset.asset_id)
      confirmationModal('Stergere intrare', '<p class="text-danger">Sunteti sigur ca doriti stergerea documentului ('+asset.asset_id+') ?</p>', function(){
          asset.delete();
      });
  });
  $('#records_cleanup').click(function(e){
      var confirm = $(this).attr('confirm');
      records_cleanup(_template, confirm=parseInt(confirm));
      $('#records_overlay').hide();
      $('#records_add').attr('is_edit', '0');
      $('#records_overlay > div').html(`
      Pregatim interfata <div style="display: inline-flex; margin-left: 15px;" class="dot-carousel"></div>
      `);
  });
  $('#records_overlay').hide();
}

function records_validate(_template, is_citation=false) {
  var els = Object.keys(_template);
  for (var e in els) {
      var _val = get_element_value(els[e], _template[els[e]].type);
      if (Object.keys(_template[els[e]]).includes('validate')) {
          _valid = RegExp(_template[els[e]].validate).test(_val);
          console.log('validate ('+els[e]+'): ', _valid);
          if (!_valid) {
              console.log(els[e] + ' value: ', _val);
              validationModal('Validare esuata', $("label[for='"+els[e]+"']").text());
              return false;
          }
      }
      if (_template[els[e]].type === 'members') {
          var _found = false;
          var _existing_member = [];
          if (_val.length == 0) {
              validationModal("Validare esuata", 'Lista cu membrii trebuie sa contina cel putin o intrare.', true);
              return false;
          }
          for (var i in _val) {
              if (_existing_member.includes(_val[i].id)) {
                  validationModal("Validare esuata", 'Un membru a fost adaugat de doua ori in lista cu membrii.', true);
                  return false;
              }
              if (_val[i].id.length < 5) {
                  validationModal("Validare esuata", 'Campul <span class="badge badge-secondary">Nume membru</span> este invalid.', true);
                  return false;
              }
              if (_val[i].type === '') {
                  validationModal("Validare esuata", 'Campul <span class="badge badge-secondary">Calitate</span> este invalid.', true);
                  return false;
              }
              if (_val[i].id === udata['urap_id']) {
                  _found = true;
              }
              _existing_member.push(_val[i].id);
          }
          if (!_found) {
              validationModal("Validare esuata", 'Utilizatorul curent nu este membru in proiect. Poti adauga doar proiectele in care esti sau ai fost participant.', true);
              return false;
          }
      }
      if (_template[els[e]].type === 'authors') {
          var _found = false;
          var _existing_member = [];
          if (_val.length == 0) {
              validationModal("Validare esuata", 'Lista cu autori trebuie sa contina cel putin o intrare.', true);
              return false;
          }
          for (var i in _val) {
              if (_existing_member.includes(_val[i].name)) {
                  validationModal("Validare esuata", 'Un autor a fost adaugat de doua ori in lista cu membrii.', true);
                  return false;
              }
              if (_val[i].id === udata['urap_id']) {
                  _found = true;
              }
              _existing_member.push(_val[i].name);
          }
          if (!_found && !is_citation) {
              validationModal("Validare esuata", 'Utilizatorul curent nu este autor al publicatiei. Poti adauga doar publicatiile in care esti autor.', true);
              return false;
          }
      }
      _template[els[e]].setter(_val);
  }
  console.log('computed asset object: ', asset);

  // validate asset object after all the values are set;
  if (asset.asset_type === 'publication') {
      if (asset.publication_metadata.issn === '' && asset.publication_metadata.eissn === '' && asset.publication_metadata.isbn === '' && asset.publication_metadata.eisbn === '') {
          validationModal('Validare esuata', 'Publicatia trebuie sa aiba cel putin un identificator ISSN, eISSN, ISBN sau eISBN.', true);
          return false;
      }
  }
  return true;
}

function records_cleanup(_template, confirm=1) {
  var pubcleanup = function() {
      var els = Object.keys(_template);
      for (var e in els) {
          clean_element_value(els[e], _template[els[e]].type, suggestions=people_suggestions);
      }
  };
  if (confirm == 1) {
      confirmationModal('Confirmare', 'Sunteti sigur ca doriti stergerea informatiilor introduse?', callfn=pubcleanup);
  } else {
      pubcleanup();
  }
}

function _init_asset(asset_type) {
  console.log('[init_asset] generating a new asset object ('+asset_type+').');
  for (var i in asset_types) {
      if (asset_types[i].id === asset_type) {
        asset_types[i].create();
        asset.asset_type = asset_type;
      }
  }
  if (!asset || !asset.asset_type) {
      bootbox.alert({title: 'Unexpected exception', message: 'Tipul documentului ('+asset_type+') inexistent. Eroare de cod!'});
      throw new Error('FATAL ERROR! Check source code');
  }
 
}

function draw_tags_on_modal(modal_id, tag_id, tag_type) {
    //use this method to refresh tags fields on modals; without this the autocomplete function deosn't work properly;
    $('#'+modal_id).on('shown.bs.modal', function(e){
      
        if (typeof tag_id === 'string') {
          if (tag_type === 'wostags') {
            $('#'+tag_id).amsifySuggestags({suggestions: woscat_suggestions}, 'refresh');
          }
          if (tag_type === 'authors') {
            $('#'+tag_id).amsifySuggestags({suggestions: people_suggestions}, 'refresh');
          }
          if (tag_type === 'members') {
            $('#'+tag_id+' input[id*="records_member_name_"]').each(function(i){
              $(this).amsifySuggestags({
                suggestions: people_suggestions,
                tagLimit: 1,
                selectOnHover: false,
                whiteList: true
              }, 'refresh');
            });
          }
        } else {
          for (var i in tag_id) {
            if (tag_type === 'authors') {
              $('#'+tag_id[i]).amsifySuggestags({suggestions: people_suggestions}, 'refresh');
            }
            if (tag_type === 'wostags') {
              $('#'+tag_id[i]).amsifySuggestags({suggestions: woscat_suggestions}, 'refresh');
            }
            if (tag_type === 'members') {
              $('#'+tag_id[i]).amsifySuggestags({
                suggestions: people_suggestions,
                tagLimit: 1,
                selectOnHover: false,
                whiteList: true
              }, 'refresh');
            }
          }
        }  
        $('#records_keywords').amsifySuggestags({}, 'refresh');
    });
    $('#'+modal_id).on('hidden.bs.modal', function(e){
        $('#records_cleanup').attr('confirm', '0').click();
    });
}

function register_asset_edit_modal(_template, elem_id) {
  $('#'+elem_id).on('click', '#assetEdit', function(){
    var asset_id = $(this).attr('asset_id');
    $('button[data-dismiss~="editRecordModal"]').click(function(){
        $('#editRecordModal').modal('hide');
    });
    $('#records_div_title').parent().remove();
    $('#records_cleanup').parent().hide();
    $('#records_delete').parent().show();
    $('#records_add_name').text('Modifica');
    $('#records_add').attr('is_edit', '1');
    $('#editRecordModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $.ajax({
        async: false,
        url: '/api/v2/assets/'+asset_id,
        type: 'GET',
        cache: false,
        headers: {
            'Authorization': udata['token'],
            'Content-type': 'application/json'
        },
        error: function(xhr, ajaxOptions, thrownError){
            console.log('[asset] ERROR: '+thrownError);
            bootbox.alert('Eroare validare backend: '+ajaxOptions);
        },
        success: function(data) {
            console.log('[asset] QUERY OK');
            delete data._links;
            $('#editRecordModalLabel').text('Editeaza publicatie: '+asset_id);
            asset = new Asset(data);
            var ptk = Object.keys(_template);
            member_index = 2;
            for (var i in ptk) {
                if (Object.keys(_template[ptk[i]]).includes('getter')) {
                    set_element_value(ptk[i], _template[ptk[i]].getter(), _template[ptk[i]].type);
                }
            }
        }
    });
  });
}

function register_asset_add_modal(_template, elem_id, target_id) {
  var _b = '[register_asset_add_modal]';
  $('#'+elem_id).on('click', '#'+target_id, function(){
    var parent_asset_id = $(this).attr('parent_asset_id');
    $('#records_checkdocid').val('');
    $('#records_checkdoctitle').val('');
    $('button[data-dismiss~="editRecordModal"]').click(function(){
        $('#editRecordModal').modal('hide');
    });
    if ($('#citations_list_div').is(':visible')) {
      console.log('[register_asset_add_modal] adding document ID search fields ...');
      $('#records_checkdocid_row').show();
      //$('#records_checkdoctitle_row').show();
    }
      $('label[for="records_datepicker"]').parent().parent().parent().on('click', '#records_checkdocid_button', function(){
        var target_asset_id = $('#records_checkdocid').val();
        $('#records_add').attr('parent_asset_id', parent_asset_id);
        console.log('Check document id clicked: '+target_asset_id);
        if (uuidValidator(target_asset_id)) {
          $.ajax({
            async: false,
            url: '/api/v2/dt/user/assets?field_search='+target_asset_id,
            type: 'GET',
            cache: false,
            headers: {
                'Authorization': udata['token'],
                'Content-type': 'application/json'
            },
            error: function(xhr, ajaxOptions, thrownError){
                console.log('[asset] ERROR: '+thrownError);
                bootbox.alert('Eroare validare backend: '+ajaxOptions);
            },
            success: function(data) {
                console.log('[asset] QUERY OK');
                delete data._links;
                $('#records_add').attr('is_edit', '1');
                asset = new Asset(data);
                var ptk = Object.keys(_template);
                for (var i in ptk) {
                  //console.log(ptk[i], _template[ptk[i]].type, _template[ptk[i]].getter())
                    if (Object.keys(_template[ptk[i]]).includes('getter')) {
                        set_element_value(ptk[i], _template[ptk[i]].getter(), _template[ptk[i]].type);
                    }
                }
            }
          });
        }
      });
    $('#editRecordModalLabel').text('Adauga citare pentru: '+parent_asset_id);
    $('#records_div_title').parent().remove();
    $('#records_cleanup').parent().show();
    $('#records_delete').parent().hide();
    $('#records_add_name').text('Adauga');
    $('#records_add').attr('is_edit', '0');
    $('#editRecordModal').modal({
        backdrop: 'static',
        keyboard: false
    });
   
  });
}

function get_reports_view() {
  $.ajax({
    async: false,
    url: '/api/v2/reports',
    type: 'GET',
    cache: false,
    headers: {
        'Authorization': udata['token'],
        'Content-type': 'application/json'
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('[reports_view] ERROR: '+thrownError);
        $('#reports_area').html('<div class="alert alert-warning" role="alert" style="text-align: center;">Pentru profilul dumneavoastra nu exista raportari disponibile</div>');
    },
    success: function(data) {
        console.log('[reports_view] QUERY OK');
        _data = JSON.parse(data)
        if ((Object.keys(_data).length) == 0) {
          $('#reports_area').html('<div class="alert alert-warning" role="alert" style="text-align: center;">Pentru profilul dumneavoastra nu exista raportari disponibile</div>');
        } else {
          $('#reports_area').html('');
          for (var k in _data) {
            _oh = $('#reports_area').html()
            $('#reports_area').html(_oh + '<a id="reports_'+k+'" href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> '+_data[k]['title']+'</a>')
            $('#reports_area').on('click', '#reports_'+k, function(){
              call_reports_init(k);
            });
          }
          //<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Raport CNATDCU</a>
        }
    }
});
}

function call_reports_init(rtype) {
  $('#report_generate_loader').css('visibility', 'visible');
  $.ajax({
    url: '/api/v2/reports/'+rtype,
    type: 'GET',
    xhrFields: {
      responseType: 'blob'
    },
    cache: false,
    headers: {
        'Authorization': udata['token']
    },
    error: function(xhr, ajaxOptions, thrownError){
        console.log('[reports_view] ERROR: '+thrownError);
        bootbox.alert('Eroare validare backend: '+ajaxOptions);
    },
    success: function(data, status, xhr) {
      console.log('[reports_init] QUERY OK');
      var ds = xhr.getResponseHeader('Content-Disposition');
      var filename = 'report.xlsx';
      if (ds && ds.indexOf('attachment') !== -1) {
        var fregex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var m = fregex.exec(ds);
        if (m != null && m[1]) filename = m[1].replace(/['"]/g, '');
      }
      var a = document.createElement('a');
      var url = window.URL.createObjectURL(data);
      a.href = url;
      a.download = filename;
      document.body.append(a);
      a.click();
      a.remove();
      window.URL.revokeObjectURL(url);
      $('#report_generate_loader').css('visibility', 'hidden');
    }
  });
}

var wos_categories = ['PSYCHOLOGYC0MM4_MULTIDISCIPLINARY', 'IMAGING_SCIENCE_4ND_PHOTOGRAPHIC_TECHNOLOGY', 'ENGINEERINGC0MM4_PETROLEUM', 'RADIOLOGYC0MM4_NUCLEAR_MEDICINE_4ND_MEDICAL', 'EDUCATIONC0MM4_SPECIAL', 'FAMILY_STUDIES', 'IMMUNOLOGY', 'MATERIALS_SCIENCEC0MM4_CHARACTERIZATION_4ND_TESTING', 'QUANTUM_SCIENCE_4ND_TECHNOLOGY', 'TOXICOLOGY', 'ENGINEERINGC0MM4_CHEMICAL', 'INTEGRATIVE_4ND_COMPLEMENTARY_MEDICINE', 'COMPUTER_SCIENCEC0MM4_SOFTWARE_ENGINEERING', 'IMAGING_SCIENCE_4ND_PHOTOGRAPHIC', 'MEDICAL_INFORMATICS', 'METALLURGY_4ND_METALLURGICAL_ENGINEERING', 'INFECTIOUS_DISEASES', 'REPRODUCTIVE_BIOLOGY', 'MINERALOGY', 'DEMOGRAPHY', 'FOOD_SCIENCE_4ND_TECHNOLOGY', 'TRANSPORTATION', 'SOCIAL_WORK', 'MARINE_4ND_FRESHWATER_BIOLOGY', 'PSYCHOLOGYC0MM4_MATHEMATICAL', 'PUBLICC0MM4_ENVIRONMENTAL_4ND_OCCUPATIONAL_HEALTH', 'PRIMARY_HEALTH_CARE', 'OPERATIONS_RESEARCH_4ND_MANAGEMENT_SCIENCE', 'THERMODYNAMICS', 'MEDICAL_LABORATORY_TECHNOLOGY', 'COMMUNICATION', 'RESPIRATORY_SYSTEM', 'POLYMER_SCIENCE', 'MEDICINEC0MM4_LEGAL', 'OPTICS', 'TROPICAL_MEDICINE', 'CELL_BIOLOGY', 'ASTRONOMY_4ND_ASTROPHYSICS', 'ORNITHOLOGY', 'VIROLOGY', 'RHEUMATOLOGY', 'GEOSCIENCESC0MM4_MULTIDISCIPLINARY', 'ANESTHESIOLOGY', 'ENDOCRINOLOGY_4ND_METABOLISM', 'AUTOMATION_4ND_CONTROL_SYSTEMS', 'MULTIDISCIPLINARY_SCIENCES', 'ENGINEERINGC0MM4_ELECTRICAL_4ND_ELECTRONIC', 'PSYCHOLOGYC0MM4_APPLIED', 'POLITICAL_SCIENCE', 'CONSTRUCTION_4ND_BUILDING_TECHNOLOGY', 'SOCIOLOGY', 'CHEMISTRYC0MM4_APPLIED', 'ELECTROCHEMISTRY', 'URBAN_STUDIES', 'PHYSICSC0MM4_PARTICLES_4ND_FIELDS', 'HISTORY_OF_SOCIAL_SCIENCES', 'LAW', 'PARASITOLOGY', 'HISTORY', 'LOGIC', 'EDUCATIONC0MM4_SCIENTIFIC_DISCIPLINES', 'PHYSICSC0MM4_MATHEMATICAL', 'AREA_STUDIES', 'MICROBIOLOGY', 'AGRICULTURAL_ECONOMICS_4ND_POLICY', 'COMPUTER_SCIENCEC0MM4_THEORY_4ND_METHODS', 'ZOOLOGY', 'HEMATOLOGY', 'CHEMISTRYC0MM4_MEDICINAL', 'COMPUTER_SCIENCEC0MM4_INFORMATION_SYSTEMS', 'PSYCHOLOGYC0MM4_EXPERIMENTAL', 'CARDIAC_4ND_CARDIOVASCULAR_SYSTEMS', 'GREEN_4ND_SUSTAINABLE_SCIENCE_4ND_TECHNOLOGY', 'SOCIAL_SCIENCESC0MM4_MATHEMATICAL_METHODS', 'SOCIAL_SCIENCESSC0MM4_BIOMEDICAL', 'PHARMACOLOGY_4ND_PHARMACY', 'NEUROSCIENCES', 'COMPUTER_SCIENCEC0MM4_HARDWARE_4ND_ARCHITECTURE', 'GENETICS_4ND_HEREDITY', 'GEOGRAPHYC0MM4_PHYSICAL', 'BIOCHEMICAL_RESEARCH_METHODS', 'BUSINESSC0MM4_FINANCE', 'ERGONOMICS', 'ENVIRONMENTAL_STUDIES', 'AGRICULTUREC0MM4_MULTIDISCIPLINARY', 'INTERNATIONAL_RELATIONS', 'SOCIAL_SCIENCESC0MM4_BIOMEDICAL', 'MEDICINEC0MM4_GENERAL_4ND_INTERNAL', 'NUCLEAR_SCIENCE_4ND_TECHNOLOGY', 'PHYSICSC0MM4_CONDENSED_MATTER', 'COMPUTER_SCIENCEC0MM4_INTERDISCIPLINARY_APPLICATIONS', 'PUBLIC_ADMINISTRATION', 'PUBLICC0MM4_ENVIRONMENTAL_4ND_OCCUPATIONAL', 'REHABILITATION', 'PSYCHOLOGYC0MM4_BIOLOGICAL', 'SOCIAL_SCIENCESSC0MM4_INTERDISCIPLINARY', 'CRIMINOLOGY_4ND_PENOLOGY', 'MATERIALS_SCIENCEC0MM4_PAPER_4ND_WOOD', 'GEOCHEMISTRY_4ND_GEOPHYSICS', 'ROBOTICS', 'CRYSTALLOGRAPHY', 'PLANT_SCIENCES', 'CHEMISTRYC0MM4_ANALYTICAL', 'ECOLOGY', 'DENTISTRYC0MM4_ORAL_SURGERY_4ND_MEDICINE', 'ANATOMY_4ND_MORPHOLOGY', 'DERMATOLOGY', 'BUSINESS', 'RADIOLOGYC0MM4_NUCLEAR_MEDICINE_4ND_MEDICAL_IMAGING', 'EVOLUTIONARY_BIOLOGY', 'MINING_4ND_MINERAL_PROCESSING', 'NEUROIMAGING', 'CELL_4ND_TISSUE_ENGINEERING', 'Development_Studies', 'ECONOMICS', 'BEHAVIORAL_SCIENCES', 'CHEMISTRYC0MM4_MULTIDISCIPLINARY', 'BIOLOGY', 'MEDICINEC0MM4_RESEARCH_4ND_EXPERIMENTAL', 'SUBSTANCE_ABUSE', 'AGRONOMY', 'HEALTH_POLICY_4ND_SERVICES', 'ENTOMOLOGY', 'ANTHROPOLOGY', 'PEDIATRICS', 'CHEMISTRYC0MM4_PHYSICAL', 'ENGINEERINGC0MM4_MARINE', 'PSYCHOLOGYC0MM4_EDUCATIONAL', 'BIOPHYSICS', 'ENGINEERINGC0MM4_ENVIRONMENTAL', 'ENGINEERINGC0MM4_MECHANICAL', 'EMERGENCY_MEDICINE', 'PHYSIOLOGY', 'DEVELOPMENT_STUDIES', 'VETERINARY_SCIENCES', 'OBSTETRICS_4ND_GYNECOLOGY', 'FISHERIES', 'FORESTRY', 'PSYCHOLOGY', 'SPORT_SCIENCES', 'INDUSTRIAL_RELATIONS_4ND_LABOR', 'HOSPITALITYC0MM4_LEISUREC0MM4_SPORT_4ND_TOURISM', 'BIODIVERSITY_CONSERVATION', 'ENGINEERINGC0MM4_OCEAN', 'INSTRUMENTS_4ND_INSTRUMENTATION', 'METEOROLOGY_4ND_ATMOSPHERIC_SCIENCES', 'CLINICAL_NEUROLOGY', 'PHYSICSC0MM4_ATOMICC0MM4_MOLECULAR_4ND_CHEMICAL', 'GEOLOGY', 'MATERIALS_SCIENCEC0MM4_CERAMICS', 'MATERIALS_SCIENCEC0MM4_TEXTILES', 'GERONTOLOGY', 'PALEONTOLOGY', 'GEOGRAPHY', 'MECHANICS', 'REGIONAL_4ND_URBAN_PLANNING', 'MYCOLOGY', 'ENVIRONMENTAL_SCIENCES', 'PATHOLOGY', 'HISTORY_4ND_PHILOSOPHY_OF_SCIENCE', 'EDUCATION_4ND_EDUCATIONAL_RESEARCH', 'COMPUTER_SCIENCEC0MM4_CYBERNETICS', 'MATERIALS_SCIENCEC0MM4_COATINGS_4ND_FILMS', 'OPHTHALMOLOGY', 'PHYSICSC0MM4_FLUIDS_4ND_PLASMAS', 'PLANNING_4ND_DEVELOPMENT', 'ALLERGY', 'MEDICAL_ETHICS', 'MICROSCOPY', 'COMPUTER_SCIENCEC0MM4_ARTIFICIAL_INTELLIGENCE', 'SOCIAL_ISSUES', 'REMOTE_SENSING', 'TRANSPLANTATION', 'MATHEMATICSC0MM4_APPLIED', 'ETHICS', 'HISTORY_OF_SOCIAL_SCIENCESS', 'AGRICULTUREC0MM4_DAIRY_4ND_ANIMAL_SCIENCE', 'CHEMISTRYC0MM4_ORGANIC', 'ENGINEERINGC0MM4_MANUFACTURING', 'PSYCHOLOGYC0MM4_DEVELOPMENTAL', 'GASTROENTEROLOGY_4ND_HEPATOLOGY', 'MANAGEMENT', 'WATER_RESOURCES', 'ENGINEERINGC0MM4_BIOMEDICAL', 'LINGUISTICS', 'ANDROLOGY', 'ENGINEERINGC0MM4_INDUSTRIAL', 'ORTHOPEDICS', 'TRANSPORTATION_SCIENCE_4ND_TECHNOLOGY', 'UROLOGY_4ND_NEPHROLOGY', 'SOCIAL_SCIENCESC0MM4_INTERDISCIPLINARY', 'PSYCHOLOGYC0MM4_CLINICAL', 'ENGINEERINGC0MM4_GEOLOGICAL', 'MATERIALS_SCIENCEC0MM4_MULTIDISCIPLINARY', 'CRITICAL_CARE_MEDICINE', 'HEALTH_CARE_SCIENCES_4ND_SERVICES', 'STATISTICS_4ND_PROBABILITY', 'WOMENS_STUDIES', 'MATERIALS_SCIENCEC0MM4_BIOMATERIALS', 'CHEMISTRYC0MM4_INORGANIC_4ND_NUCLEAR', 'COMPUTER_SCIENCEC0MM4_INTERDISCIPLINARY', 'HORTICULTURE', 'PHYSICSC0MM4_NUCLEAR', 'DEVELOPMENTAL_BIOLOGY', 'ENGINEERINGC0MM4_CIVIL', 'NURSING', 'ENGINEERINGC0MM4_MULTIDISCIPLINARY', 'ENGINEERINGC0MM4_AEROSPACE', 'ENERGY_4ND_FUELS', 'PERIPHERAL_VASCULAR_DISEASE', 'ONCOLOGY', 'MATERIALS_SCIENCEC0MM4_CHARACTERIZATION_4ND', 'GERIATRICS_4ND_GERONTOLOGY', 'ETHNIC_STUDIES', 'PSYCHOLOGYC0MM4_SOCIAL', 'Quantum_Science_4ND_Technology', 'NUTRITION_4ND_DIETETICS', 'OTORHINOLARYNGOLOGY', 'ACOUSTICS', 'OCEANOGRAPHY', 'SOCIAL_SCIENCESSC0MM4_MATHEMATICAL_METHODS', 'MATERIALS_SCIENCEC0MM4_COMPOSITES', 'LIMNOLOGY', 'CULTURAL_STUDIES', 'BIOCHEMISTRY_4ND_MOLECULAR_BIOLOGY', 'SPECTROSCOPY', 'PSYCHOLOGYC0MM4_PSYCHOANALYSIS', 'MATHEMATICS', 'INFORMATION_SCIENCE_4ND_LIBRARY_SCIENCE', 'AGRICULTURAL_ENGINEERING', 'AUDIOLOGY_4ND_SPEECH-LANGUAGE_PATHOLOGY', 'SOIL_SCIENCE', 'TELECOMMUNICATIONS', 'SURGERY', 'BIOTECHNOLOGY_4ND_APPLIED_MICROBIOLOGY', 'MATHEMATICAL_4ND_COMPUTATIONAL_BIOLOGY', 'NANOSCIENCE_4ND_NANOTECHNOLOGY', 'PSYCHIATRY', 'PHYSICSC0MM4_APPLIED', 'PHYSICSC0MM4_MULTIDISCIPLINARY', 'MATHEMATICSC0MM4_INTERDISCIPLINARY_APPLICATIONS']