$(function() {
    if (addressValue.includes('localhost/ui')) {
        $('#login_form').css("display","");
    }
    if (cookieEngine.hasItem("urap_login")) {
        console.log('[login] token exists. changing to index.');
        $(location).attr('href','index.html');
    }

    var durl = getJsonFromUrl(addressValue);
    if (durl.query) {
        var _urap_id = undefined;
        var _token_id = undefined;
        var _action = undefined;
        var ks = Object.keys(durl.query);
        if (ks.includes('action')) {
            _action = durl.query['action'];
        }
        if (ks.includes('urap_id')) {
            _urap_id = durl.query['urap_id'];
        }
        if (ks.includes('urap_token')) {
            _token_id = durl.query['urap_token'];
        }
        if (_action) {
            if (_action === 'notfound') {
                bootbox.alert({title: 'Eroare autentificare', message: 'Contul dumneavoastra nu exista in baza de date. Contactati adiministratorul aplicatiei URAP 2.0'});
            }
            if (_action === 'notenabled') {
                bootbox.alert({title: 'Eroare autentificare', message: 'Contul dumneavoastra nu este activat. Contactati adiministratorul aplicatiei URAP 2.0'});
            }
        }
        if (_urap_id && _token_id) {
            if (_action === 'ok') {
                $.ajax({
                url: '/api/v2/people/'+_urap_id,
                type: 'GET',
                headers: {
                    'Authorization': _token_id,
                },
                error: function(xhr, ajaxOptions, thrownError){
                    console.log('[login] ERROR: '+thrownError);
                    validationModal('Eroare autentificare', 'Eroare backend: '+thrownError+'<hr><pre>'+JSON.stringify(xhr.responseJSON, null, '\t')+'</pre>', true);
                },
                success: function(data) {
                    console.log('[login] authentication OK');
                    cookieEngine.setItem('urap_login', 
                        JSON.stringify({ loggedIn: true, 
                                        urap_id: data['urap_id'],
                                        identifiers: data['identifiers'],
                                        name: capitalize(data['firstname'])+' '+capitalize(data['lastname']), 
                                        token: _token_id
                        })
                    );
                    $(location).attr('href','index.html');
                }   
                });
            }
        }
    }

    $('#login_email').keyup(function(){
        if (emailValidator($(this).val())) {
            $('#login_button').attr("disabled", false);
        } else {
            $('#login_button').attr("disabled", true);
        }
    });

    $('#login_button').click(function(){
        userLogin();
        $(location).attr('href','index.html');
    });

});