const udata = JSON.parse(cookieEngine.getItem('urap_login'));

$(function() {
    $.fn.selectpicker.Constructor.BootstrapVersion = '4';    
    $('#user_information_span').text(udata['name']); 
    
    if (addressValue.includes('publications.html')) {
        console.log('-- publications-list page loaded.');
        $('#nav_item_assets_list').addClass('active');
        listtable = $('#recordsListTable').DataTable({
                ajax: {
                    url: '/api/v2/dt/user/assets?type=publication&urap_id='+udata['urap_id'],
                    type: 'GET',
                    headers: {'Authorization': udata['token']}
                },
                columns: [
                { title: "ID", sortable: false, searchable: false,  render: function(data, type, full) { 
                    //return '<button id="assetEdit" type="button" class="btn btn-primary btn-sm" asset_id='+full[0]+'>Edit</button>'
                    return '<div><a id="assetEdit" href="#" class="btn btn-success btn-circle btn-sm text-center" asset_id="'+full[0]+'"><i class="far fa-edit"></i></a>'+
                    ' <span style="font-weight:bold;" class="btn btn-'+publication_type_reverse_mapping[full[5]].color+' btn-circle btn-sm text-center">'+publication_type_reverse_mapping[full[5]].abbrev+'</span></div>'
                }}, 
                { title: "Titlu", sortable: false },
                { title: "Autori", sortable: false },
                { title: "An" },
                { title: "Citări", sortable: false, searchable: false,  render: function(data, type, full) {
                    return '<button id="assetCitationsManager" type="button" class="btn btn-primary btn-sm" asset_id="'+full[0]+'">'+(full[4] || 0)+'</button>'
                }},
                ],
                searching: false,
                processing: true,
                serverSide: true,
                order: [[3, "desc"]],
                lengthMenu: [10, 25],
                language: {
                    "decimal": ",",
                    "thousands": ".",
                    "zeroRecords": "Nu exista nicio intrare.",
                    "info": "Afisez _START_ pana la _END_ (_TOTAL_)",
                    "infoFiltered":  "",
                    "lengthMenu": "Afisez _MENU_ intrari",
                    "processing": "Procesez ...",
                    "paginate": {
                        "first":      "Prima",
                        "last":       "Ultima",
                        "next":       "Urm.",
                        "previous":   "Prec."
                    },
                    "search": "Cauta"
                }
        });
        draw_tags_on_modal('editRecordModal', 'records_pubauthors', 'authors');
        draw_tags_on_modal('editRecordModal', 'records_woscat', 'wostags');
        register_asset_edit_modal(publication_template, 'recordsListTable');
        register_asset_edit_modal(publication_template, 'citationsListTable');
        register_asset_add_modal(publication_template, 'citations_list_card_header', 'citations_list_add');
        
        $('#citations_list_close').click(function(){
            $('#citations_list_div').hide();
            $('#records_list_div').fadeToggle( "slow", "linear" );
            listtable.ajax.reload();
            $('#citationsListTable').DataTable().clear();
            $('#citationsListTable').DataTable().destroy();
            $('#citations_list_add').attr('parent_asset_id', '');
            $('#records_checkdocid_row').hide();
            $('#records_checkdoctitle_row').hide();
            $('#records_add').attr('is_edit', '0');
        });
        $('#recordsListTable').on('click', '#assetCitationsManager', function(e){
            var _asset_id = $(this).attr('asset_id');
            var _citations_no = $(this).text();
            console.log('citations-manager: ', _asset_id, _citations_no);
            $('#citations_list_header').text(_asset_id);
            $('#citations_list_add').attr('parent_asset_id', _asset_id);
            $('#records_list_div').hide();
            $('#citations_list_div').fadeToggle( "slow", "linear" );
            cittable = $('#citationsListTable').DataTable({
                ajax: {
                    url: '/api/v2/dt/asset/citations?asset_id='+_asset_id,
                    type: 'GET',
                    headers: {'Authorization': udata['token']}
                },
                columns: [
                { title: "ID", sortable: false, searchable: false,  render: function(data, type, full) { 
                    return '<a id="assetEdit" href="#" class="btn btn-success btn-circle btn-sm text-center" asset_id="'+full[0]+'"><i class="far fa-edit"></i></a>'
                }}, 
                { title: "Titlu", sortable: false },
                { title: "Autori", sortable: false },
                { title: "An" },
                { title: "OP", sortable: false, searchable: false,  render: function(data, type, full) {
                    return '<a id="citations_list_delete_entry" href="#" class="btn btn-danger btn-circle btn-sm" asset_id='+full[0]+'><i class="fas fa-times"></i></a>'
                }},
                ],
                searching: false,
                processing: true,
                serverSide: true,
                order: [[3, "desc"]],
                lengthMenu: [10, 25],
                language: {
                    "decimal": ",",
                    "thousands": ".",
                    "zeroRecords": "Nu exista nicio intrare.",
                    "info": "Afisez _START_ pana la _END_ (_TOTAL_)",
                    "infoFiltered":  "",
                    "lengthMenu": "Afisez _MENU_ intrari",
                    "processing": "Procesez ...",
                    "paginate": {
                        "first":      "Prima",
                        "last":       "Ultima",
                        "next":       "Urm.",
                        "previous":   "Prec."
                    },
                    "search": "Cauta"
                }
            });
        });
    }
    if (addressValue.includes('projects.html')) {
        console.log('-- projects-list page loaded.');
        $('#nav_item_projects_list').addClass('active');
        listtable = $('#recordsListTable').DataTable({
            ajax: {
                url: '/api/v2/dt/user/assets?type=project&urap_id='+udata['urap_id'],
                type: 'GET',
                headers: {'Authorization': udata['token']}
            },
            columns: [
            { title: "ID", sortable: false, searchable: false,  render: function(data, type, full) { 
                return '<button id="assetEdit" type="button" class="btn btn-primary btn-sm" asset_id='+full[0]+'>Edit</button>'
            }}, 
            { title: "Titlu", sortable: false },
            { title: "Membrii", sortable: false },
            { title: "An" },
            { title: "Buget", sortable: false, searchable: false },
            ],
            searching: false,
            processing: true,
            serverSide: true,
            order: [[3, "desc"]],
            lengthMenu: [10, 25],
            language: {
                "decimal": ",",
                "thousands": ".",
                "zeroRecords": "Nu exista nicio intrare.",
                "info": "Afisez _START_ pana la _END_ (_TOTAL_)",
                "infoFiltered":  "",
                "lengthMenu": "Afisez _MENU_ intrari",
                "processing": "Procesez ...",
                "paginate": {
                    "first":      "Prima",
                    "last":       "Ultima",
                    "next":       "Urm.",
                    "previous":   "Prec."
                },
                "search": "Cauta"
            }
        });
        draw_tags_on_modal('editRecordModal', 'records_members_list', 'members');
        register_asset_edit_modal(project_template, 'recordsListTable');
    }

    if (addressValue.includes('add-records-')) {
        console.log('-- records page loaded.');
        _loc = $(location).attr('href').split('/');
        $('#nav_item_records').addClass('active');
        //$('#nav_item_records > a').removeClass('collapsed');
        //$('#nav_item_records > div').addClass('show');
        /*
        $('#nav_item_records > div > div > a').each(function(){
            _href = $(this).attr('href');
            if (_href === _loc[_loc.length-1]) {
                $(this).addClass('active');
                return;
            }
        });
        */
    }

    if (addressValue.includes('user-profile.html')) {
        var _id = '';
        var _etag = '';
        $('input[id="user_profile_wosid"]').amsifySuggestags({});
        $('input[id="user_profile_scopusid"]').amsifySuggestags({});
        $('input[id="user_profile_researcherid"]').amsifySuggestags({});
        $('input[id="user_profile_orcidid"]').amsifySuggestags({});
        $('select').selectpicker({
            width: '100%',
            noneSelectedText: 'Nicio selectie'
        });

        /*
        $('#user_information_profile').click(function(event){
            event.preventDefault();
            return;
        });
        */
        
        $('#user_profile_header_name').text(udata['name'].split(" ")[0]+' '+udata['name'].split(" ")[1]);

        // retrieve departments
        $.ajax({
            type: 'GET',
            url: '/api/v2/departments?max_results=40',
            headers: {
                'Authorization': udata['token']
            },
            error: function(xhr, ajaxOptions, thrownError){
                console.log('[user-profile][get-departments] ERROR: '+thrownError);
                $('#user_profile_overlay > div').html('<div class="alert alert-danger" role="alert">Eroare backend: '+thrownError+'</div>');
            },
            success: function(data){
                console.log('[user-profile][get-departments] retrieved departments: '+data._items.length);
                for (var i=0;i<data._items.length;i++) {
                    $('#user_profile_department').append(new Option(data._items[i].name, data._items[i].id));
                }
                $('select[id=user_profile_department]').selectpicker('refresh');
                //$('#user_profile_overlay').hide();
                user_profile_get_user_data();
            }
        });

        function user_profile_get_user_data() {
            // retrive user profile
            $.ajax({
                type: 'GET',
                url: '/api/v2/people/'+udata['urap_id'],
                headers: {
                    'Authorization': udata['token']
                },
                error: function(xhr, ajaxOptions, thrownError){
                    console.log('[user-profile][user-data] ERROR: '+thrownError);
                    $('#user_profile_overlay > div').html('<div class="alert alert-danger" role="alert">Eroare backend: '+ajaxOptions);
                },
                success: function(data){
                    //console.log('[user-profile][user-data] user data: '+JSON.stringify(data));
                    $('#user_profile_lastname').attr('value', capitalize(data['lastname']));
                    $('#user_profile_firstname').attr('value', capitalize(data['firstname']));
                    $('#user_profile_email').attr('value', data['email']);
                    $('#user_profile_faculty').attr('value', data['faculty']);
                    $('select[id=user_profile_department]').val(data['department']);$('select[id=user_profile_department]').selectpicker('refresh');
                    $('#user_profile_urapid').attr('value', data['urap_id']);
                    if (data['identifiers']['wos']['ids'].length > 0) {
                        input_tag_fill('user_profile_wosid', data['identifiers']['wos']['ids']);
                    }
                    if (data['identifiers']['scopus']['ids'].length > 0) {
                        input_tag_fill('user_profile_scopusid', data['identifiers']['scopus']['ids']);
                    }
                    if (data['identifiers']['researcher_ids'].length > 0) {
                        input_tag_fill('user_profile_researcherid', data['identifiers']['researcher_ids']);
                    }
                    if (data['identifiers']['orcid_ids'].length > 0) {
                        input_tag_fill('user_profile_orcidid', data['identifiers']['orcid_ids']);
                    }
                    $('#user_profile_cnatdcuds').attr('value', data['identifiers']['cnatdcu_ds']);
                    _id = data['_id'];
                    _etag = data['_etag'];
                    $('#user_profile_overlay').hide();  
                }
            });
        }

        $('#user_profile_form').validate({
            submitHandler: function(form) {
                $.ajax({
                    type: 'PATCH',
                    url: '/api/v2/people/'+_id,
                    headers: {
                        'Authorization': udata['token'],
                        'If-Match': _etag,
                        'Content-type': 'application/json'
                    },
                    data:JSON.stringify(
                        {
                            'lastname': $('#user_profile_lastname').val(),
                            'firstname': $('#user_profile_firstname').val(),
                            'email': $('#user_profile_email').val(),
                            'faculty': $('#user_profile_faculty').val(),
                            'department': $('#user_profile_department').val(),
                            'identifiers':{
                                'wos': {
                                    'ids': $('#user_profile_wosid').val().split(',')
                                },
                                'scopus':{
                                    'ids': $('#user_profile_scopusid').val().split(',')
                                },
                                'researcher_ids': $('#user_profile_researcherid').val().split(','),
                                'orcid_ids': $('#user_profile_orcidid').val().split(','),
                                'cnatdcu_ds': $('#user_profile_cnatdcuds').val()
                            }
                        }
                    ),
                    error: function(xhr, ajaxOptions, thrownError){
                        console.log('[user-profile][user-data] ERROR: '+thrownError);
                        $('#user_profile_overlay').show();
                        $('#user_profile_overlay > div').html('<div class="alert alert-danger" role="alert">Eroare actualizare: '+thrownError);
                        $('#user_profile_overlay > div').html(`
                            <div id="user_profile_error_alert" class="alert alert-danger alert-dismissible fade show" role="alert">
                                Eroare actualizare: `+thrownError+`
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        `);
                        $('#user_profile_error_alert').on('closed.bs.alert', function () {
                            $('#user_profile_overlay').hide();
                            $('#user_profile_overlay > div').html('')
                        });
                    },
                    success: function(data){
                        _etag = data['_etag'];
                        $('#user_profile_overlay').show();
                        $('#user_profile_overlay > div').html(`
                            <div id="user_profile_error_alert" class="alert alert-success alert-dismissible fade show" role="alert">
                            Actualizare efectuata cu succes!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                        `);
                        $('#user_profile_error_alert').on('closed.bs.alert', function () {
                            $('#user_profile_overlay').hide();
                            $('#user_profile_overlay > div').html('')
                        });
                    }
                });
              }
        });

    }

    if (addressValue.includes('livesearch.html')) {
        var docs = ['Alabala', 'Cucubau'];
        
        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // `states` is an array of state names defined in "The Basics"
            //local: docs
            remote: {
                prepare: function (query, settings) {
                    console.log(settings)
                    settings.type = "POST";
                    settings.contentType = "application/json; charset=UTF-8";
                    settings.data = JSON.stringify({'title': query});
                    settings.headers = {
                        'Authorization': udata['token']
                    };
                    return settings;
                },
                url: '/api/v2/services/liveTitleSearch'
              }
          });
        
        var promise = engine.initialize();
        promise.done(function() { console.log('ready to go!'); })
               .fail(function() { console.log('err, something went wrong :('); });

        $('#livesearch').typeahead({
            minLength: 1,
            highlight: true,
            hint: true,
          },
          {
            name: 'assets',
            source: engine,
            templates: {
                empty: [
                  '<div class="empty-message">',
                    'unable to find any Best Picture winners that match the current query',
                  '</div>'
                ].join('\n'),
                suggestion: function(value){ return '<div><strong>'+value["title"]+'</strong>-'+value["year"]+'</div>'}
              }
        });
        
        $('#livesearch').bind('typeahead:select', function(ev, suggestion) {
        console.log('Selection: ' + suggestion);
        });
        
    }

    if (addressValue.includes('reports.html')) {
        console.log('-- reports page loaded.');
        _loc = $(location).attr('href').split('/');
        $('#nav_item_reports_list').addClass('active');
        get_reports_view();
    }

    $('#user_information_logout').click(function(){
        userLogout();
    });
});