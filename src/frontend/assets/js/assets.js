var asset_types = [
    {
        "id": 'publication', 
        "name": 'Publicatie', 
        "create": () => {
            asset = new Asset();
            delete asset.project_metadata;
            delete asset.patent_metadata;
            delete asset.other_metadata;
        }
    },
    {
        "id": 'patent', 
        "name": 'Brevet',
        "create": () => {
            asset = new Asset();
            delete asset.publication_metadata;
            delete asset.project_metadata;
            delete asset.other_metadata;
        }
    },
    {
        "id": 'project', 
        "name": 'Proiect',
        "create": () => {
            asset = new Asset();
            delete asset.publication_metadata;
            delete asset.patent_metadata;
            delete asset.other_metadata;
        }
    },
    {
        "id": 'other', 
        "name": 'Altul',
        "create": () => {
            asset = new Asset();
            delete asset.publication_metadata;
            delete asset.patent_metadata;
            delete asset.project_metadata;
        }
    }
];

var asset_type_reverse_mapping = {
    'publication': asset_types[0],
    'patent': asset_types[1],
    'project': asset_types[2],
    'other': asset_types[3]
};

var publication_types = [
    {"id": 'journal', "name": 'Jurnal', 'abbrev': 'J', 'color': 'primary'},
    {"id": 'conference', "name": 'Conferinta', 'abbrev': 'C', 'color': 'warning'},
    {"id": 'workshop', "name": 'Workshop', 'abbrev': 'W', 'color': 'warning'},
    {"id": 'chapter', "name": 'Capitol', 'abbrev': 'BC', 'color': 'secondary'},
    {"id": 'book', "name": 'Carte', 'abbrev':'B', 'color': 'secondary'},
    {"id": 'poster', "name": 'Poster', 'abbrev': 'P', 'color': 'secondary'}
];

var publication_type_reverse_mapping = {
    'journal': publication_types[0],
    'conference': publication_types[1],
    'workshop': publication_types[2],
    'chapter': publication_types[3],
    'book': publication_types[4],
    'poster': publication_types[5]
};

var project_types = [
    {"id": 'education', "name": 'Educațional'},
    {"id": 'research', "name": "Cercetare"}
];

var project_membership = [
    {"id": 'manager', "name": 'Director'},
    {"id": 'member', "name": 'Membru'}
]

var crossref_publication_types = {
    'journal-article': publication_types[0],
    'proceedings-article': publication_types[1],
    'book': publication_types[4],
    'book-chapter': publication_types[3],
};

class Asset {
    constructor(props) {
        if (props == undefined){
            //this._etag = undefined;
            //this._id = undefined;
            this.asset_id = undefined;
            this.asset_type = asset_types[0];

            this.title = undefined;
            this.abstract = undefined;
            this.keywords = undefined;
            this.year = undefined;
            this.is_uvt = 1;
            this.people = [];

            this.index_source = {
                source_name: undefined,
                source_id: undefined
            }
            
            this.publication_metadata = {
                name: undefined,
                issue: undefined,
                volume: undefined,
                pages: undefined,
                type: undefined,
                doi: undefined,
                issn: undefined,
                eissn: undefined,
                isbn: undefined,
                eisbn: undefined,
                cites: [],
                others: {}
            };

            this.project_metadata = {
                type: undefined,
                authority: undefined,
                acronym: undefined,
                budget: undefined,
                vi: undefined,
                date_start: undefined,
                date_end: undefined,
                contractno: undefined,
                other: {}
            };

            this.patent_metadata = {
                authority: undefined,
                other: {}
            };

            this.other_metadata = {};
        }
        else {
            this.addProps(props);
        }
        this.asset_id = this.asset_id ? this.asset_id : uuidv4();
    }
    addProps(properties){
        $.extend(true, this, properties);
    }
    // Do some cleanup before insert
    prepareForInsert(is_edit=false){
        if(this.index_source.source_id == undefined && this.index_source.source_name == undefined){
            this.index_source.source_name = 'URAP';
            this.index_source.source_id = this.asset_id;
        }

        if (!is_edit) {
            // if asset is new -> check if it has an UVT member among the authors and mark it as UVT;
            var _found = false;
            for (var i in this.people) {
                if ('person_id' in Object.keys(this.people[i])) {
                    _found = true;
                    break;
                }
            }
            if (_found) {
                this.is_uvt = 1;
            } else {
                this.is_uvt = 0;
            }
        }

        if(is_edit) {
            console.log('[prepareForInsert] cleanup for update ...')
            delete this.asset_id;
            delete this._id;
            delete this._etag;
            delete this._created;
            delete this._updated;
            delete this._links;
            if (this.asset_type === 'publication') 
                if (typeof this.publication_metadata.cites != 'object') {
                    this.publication_metadata.cites = [];
                }
        } 
    }

    add_citation(parent_asset_id){
        $.ajax({
            url: '/api/v2/assets/'+parent_asset_id,
            type: 'GET',
            headers: {
                'Authorization': udata['token'],
                'Content-type': 'application/json'
            },
            error: function(xhr, ajaxOptions, thrownError){
                console.log('[asset] ERROR: '+thrownError);
                validationModal('Eroare interogare', 'Eroare interogare backend: '+thrownError+'<hr><pre>'+JSON.stringify(xhr.responseJSON, null, '\t')+'</pre>', true);
            },
            success: function(data) {
                console.log('[asset] Interogate OK');
                var clst = undefined;
                if (typeof data.publication_metadata.cites === 'object') {
                    clst = data.publication_metadata.cites;
                } else {
                    clst = []
                }
                var _pid = data.index_source.source_name + ':' + data.index_source.source_id;
                var _cid = asset.index_source.source_name + ':' + asset.index_source.source_id;
                if (clst.includes(_cid) || _pid === _cid) {
                    validationModal('Eroare adaugare citari', 'Documentul de adaugat ca citare este identic cu documentul la care vreti sa-l adaugati.', true);
                    $('#records_cleanup').attr('confirm', '0').click().attr('confirm', '1');
                    return;
                } 
                console.log('[add_citation_to_asset] cites list before update: '+clst)
                clst.push(asset.index_source.source_name + ':' + asset.index_source.source_id);
                console.log('[add_citation_to_asset] cites list after update: '+clst)
                $.ajax({
                    url: '/api/v2/assets/'+data['_id'],
                    type: 'PATCH',
                    headers: {
                        'Authorization': udata['token'],
                        'Content-type': 'application/json',
                        'If-Match': data['_etag']
                    },
                    data: JSON.stringify({'publication_metadata':{'cites': clst}}),
                    error: function(xhr, ajaxOptions, thrownError){
                        console.log('[asset] ERROR: '+thrownError);
                        validationModal('Eroare actualizare', 'Eroare validare backend: '+thrownError+'<hr><pre>'+JSON.stringify(xhr.responseJSON, null, '\t')+'</pre>', true);
                        $('#records_cleanup').attr('confirm', '0').click().attr('confirm', '1');
                    },
                    success: function(data1) {
                        console.log('[asset] Update OK');
                        listtable.ajax.reload();
                        bootbox.alert({title: 'Actualizare', 
                                        message: '<div class="alert alert-success" role="alert">Citarea a fost adaugata cu succes</div>', 
                                        callback: function(){
                                            if ($('#editRecordModal').is(':visible')) {
                                                $('#editRecordModal').modal('hide');
                                            }
                                        }});
                    }
                });
            }
        });
    }

    insert(_etag=undefined, _asset_id=undefined, alert=true) {
        var source_id = this.index_source.source_name + ':' + this.index_source.source_id;
        if (_etag && _asset_id) {
            // update document
            $.ajax({
                url: '/api/v2/assets/'+_asset_id,
                type: 'PATCH',
                headers: {
                    'Authorization': udata['token'],
                    'Content-type': 'application/json',
                    'If-Match': _etag
                },
                data: JSON.stringify(asset),
                error: function(xhr, ajaxOptions, thrownError){
                    console.log('[asset] ERROR: '+thrownError);
                    if (alert)
                        validationModal('Eroare actualizare', 'Eroare validare backend: '+thrownError+'<hr><pre>'+JSON.stringify(xhr.responseJSON, null, '\t')+'</pre>', true);
                },
                success: function(data) {
                    console.log('[asset] Update OK');
                    listtable.ajax.reload();
                    if(alert)
                        bootbox.alert({title: 'Actualizare', 
                                        message: '<div class="alert alert-success" role="alert">Documentul a fost actualizat cu succes</div>', 
                                        callback: function(){
                                            if ($('#editRecordModal').is(':visible')) {
                                                $('#editRecordModal').modal('hide');
                                            }
                                        }});
                }
            });
        } else {
            $.ajax({
                url: '/api/v2/assets',
                type: 'POST',
                headers: {
                    'Authorization': udata['token'],
                    'Content-type': 'application/json'
                },
                data: JSON.stringify(asset),
                error: function(xhr, ajaxOptions, thrownError){
                    console.log('[asset] ERROR: '+thrownError);
                    if (alert)
                        validationModal('Eroare inserare', 'Eroare validare backend: '+thrownError+'<hr><pre>'+JSON.stringify(xhr.responseJSON, null, '\t')+'</pre>', true);
                },
                success: function(data) {
                    console.log('[asset] INSERT OK: ', data);
                    if (alert)
                        validationModal('Inserare', 'Documentul a fost inserat cu succes', true, 'success', function(){
                            $('#records_cleanup').attr('confirm', '0');
                            $('#records_cleanup').click();
                            $('#records_cleanup').attr('confirm', '1');
                        });
                }
            });
        }
    }

    delete() {
        $.ajax({
            url: '/api/v2/assets/'+asset._id,
            type: 'DELETE',
            headers: {
                'Authorization': udata['token'],
                'Content-type': 'application/json',
                'If-Match': asset._etag
            },
            error: function(xhr, ajaxOptions, thrownError){
                console.log('[asset] ERROR: '+thrownError);
                validationModal('Eroare stergere', 'Eroare backend: '+thrownError+'<hr><pre>'+JSON.stringify(xhr.responseJSON, null, '\t')+'</pre>', true);
            },
            success: function(data) {
                console.log('[asset] DELETE OK');
                listtable.ajax.reload();
                bootbox.alert({title: 'Stergere', 
                                message: '<div class="alert alert-success" role="alert">Documentul a fost sters cu succes</div>', 
                                callback: function(){
                                    if ($('#editRecordModal').is(':visible')) {
                                        $('#editRecordModal').modal('hide');
                                    }
                                }});
            }
        });
    }

    set setwoscategory(value) {
        this.publication_metadata.others.wos_category = value.split(',');
    }

    get getwoscategory() {
        if (typeof(this.publication_metadata.others.wos_category) === 'object') {
            return this.publication_metadata.others.wos_category.join(',');
        } else {
            var of = this.publication_metadata.others.wos_category.trim().replace(/&/g, '4ND').replace(/,/g, 'C0MM4').replace(/;\s/g,';').replace(/\s\s+/g, ' ').replace(/\s/g, '_').toUpperCase().split(';');
            console.log('[getwoscategory] ', of)
            return of.join(',')
        }
    }

    set setkeywords(value) {
        this.keywords = value.split(',');
    }
    get getkeywords() {
        return this.keywords.join(',');
    }

    set setyear(v) {
        this.year = v.split('-')[1];
        if (this.asset_type === 'project') {
            this.project_metadata.date_start = v;
        }
    }
    get getyear() {
        if (this.asset_type === 'project') {
            return this.project_metadata.date_start;
        } else return '01-'+this.year;
    }
    // publication authors adders/setters
    set setauthors(v) {
        this.people = [];
        for (var i in v) {
            var _author = {};
            _author['name'] = v[i].name;
            if (uuidValidator(v[i].id)) {
                _author['person_id'] = v[i].id;
            } 
            _author['index'] = i;
            this.people.push(_author);
        }
    }
    get getauthors() {
        var authors = [];
        var _sindex = this.index_source.source_name.toLowerCase();
        console.log('[asset.getauthors] _sindex: ', _sindex);
            
        for (var i in this.people) {
            var _author = {};
            _author['id'] = '';
            if (Object.keys(this.people[i]).includes('person_id')) {
                _author['id'] = this.people[i].person_id;
            } 
            console.log('[asset.getauthors]: computed _author[id] '+_author['id'])
            _author['name'] = this.people[i].name;
            _author['type'] = '';
            authors.push(_author);
        }
        return authors;
    }

    // project adders/getters
    set setmembers(v) {
        this.people = [];
        for (var i=0;i<v.length;i++) {
            var _person = {};
            _person['name'] = v[i].name;
            if (uuidValidator(v[i].id)) {
                _person['person_id'] = v[i].id;
            }
            _person['index'] = i;
            _person['flags'] = {};
            _person['flags']['membership'] = v[i].type;
            this.people.push(_person);
        }
    }
    get getmembers() {
        var members = [];
        for (var i in this.people) {
            var _member = {};
            _member['id'] = '';
            if (Object.keys(this.people[i]).includes('person_id')) {
                _member['id'] = this.people[i].person_id;
            } else {
                if (Object.keys(this.people[i]).includes('source_author_id')) {
                    var _sid = this.people[i].source_author_id;
                    for (var j in udata.identifiers) {
                        if (udata.identifiers[j].ids.includes(_sid)) {
                            _member['id'] = udata['urap_id'];
                        }
                    }
                }
            }
            _member['type'] = this.people[i].flags.membership;
            members.push(_member);
        }
        return members;
    }

    set setissn(v) {
        this.publication_metadata.issn = v.split(',');
    }
    get getissn() {
        return this.publication_metadata.issn.join(',');
    }
    set seteissn(v) {
        this.publication_metadata.eissn = v.split(',');
    }
    get geteissn() {
        return this.publication_metadata.eissn.join(',');
    }
    set setisbn(v) {
        this.publication_metadata.isbn = v.split(',');
    }
    get getisbn() {
        return this.publication_metadata.isbn.join(',');
    }
    set seteisbn(v) {
        this.publication_metadata.eisbn = v.split(',');
    }
    get geteisbn() {
        return this.publication_metadata.eisbn.join(',');
    }
}


