member_index = 1;

function member_list_init(select_db) {
    $('#records_membership_1').selectpicker({
        width: '100%',
        noneSelectedText: 'Nicio selectie'
    }).append($('<option>').text('Valoare selectata').attr('value', '').attr('hidden', '').attr('selected', ''));
    for (var i=0;i<select_db.length;i++) {
        $('#records_membership_1').append($('<option>').text(select_db[i].name).attr('value', select_db[i].id));
    }
    $('#records_membership_1').selectpicker('refresh');

    getPaginationData(progressCallback, '/api/v2/people/?max_results=50', {'Authorization': udata['token']}).then(people_list => {
        people_uvt = people_list;
        people_suggestions = people_list.map((x) => ({tag: '<img src="assets/img/uvt.svg"> '+capitalize(x.firstname)+' '+capitalize(x.lastname), value: x.urap_id}));
        $('#records_member_name_1').attr('value', udata['urap_id']);
        $('#records_member_name_1').amsifySuggestags({
            "suggestions": people_suggestions,
            tagLimit: 1,
            selectOnHover: false,
            whiteList: true
        });
        member_index += 1;
        $('#records_member_add').click(function(){
            $('#records_members_list').append(`
            <div id="records_member_row_`+member_index+`" class="form-row">
                <div class="form-group col-md-6">
                    <label for="records_member_name_`+member_index+`">Nume membru (unul singur)</label>
                    <input type="text" id="records_member_name_`+member_index+`" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="records_membership_`+member_index+`">Calitate</label>
                    <select id="records_membership_`+member_index+`"></select>
                </div>
                <div class="form-group col-md-2 justify-content-center align-self-center">
                        <button id="records_membership_del" index="`+member_index+`" type="button" class="btn btn-danger btn-xl">X</button>
                </div>
            </div>
            `);
            $('#records_member_name_'+member_index).amsifySuggestags({
                "suggestions": people_suggestions,
                tagLimit: 1,
                selectOnHover: false,
                whiteList: true
            });
            $('#records_membership_'+member_index).selectpicker({
                width: '100%',
                noneSelectedText: 'Nicio selectie'
            }).append($('<option>').text('Valoare selectata').attr('value', '').attr('hidden', '').attr('selected', ''));
            for (var i=0;i<select_db.length;i++) {
                $('#records_membership_'+member_index).append(new Option(select_db[i].name, select_db[i].id));
            }
            $('#records_membership_'+member_index).selectpicker('refresh');
            member_index += 1;
        });
    }).catch(console.error);
    $('#records_members_list').on('click', '#records_membership_del', function(){
        _id = $(this).attr('index');
        $('#records_member_row_'+_id).remove();
    });
}