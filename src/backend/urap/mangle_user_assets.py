import json, os
from backend.urap.mangle_helpers import dt_asset_format
from backend.urap.mangle_helpers import dt_assets_table_columns
from backend.urap.mangle_helpers import dt_format_pagination
from backend.urap.mangle_helpers import get_mongo_filter_user_assets
from flask import current_app as app


def dt_user_assets_response_mangle(request, payload):
    '''
		Format the response payload to match DataTables expected value
	'''
    _r = request.args
    #app.logger.debug('----------- {}'.format(payload.data))
    payload.data = json.dumps(dt_asset_format(payload.data, _r['type']))

def dt_user_assets_request_mangle(request, lookup):
    _r = request.args
    _args = request.args.copy()
    if 'urap_id' in _r.keys():
        _atype = None
        if 'type' in _r.keys():
            _atype = _r['type']
        _filter = get_mongo_filter_user_assets(_r['urap_id'], _atype)
        if _filter is not None:
            _args['where'] = json.dumps(_filter)
            app.logger.debug('where filter: {}'.format(_args['where']))
        else:
            app.logger.debug('Query filter is none!')  
    _args.update(dt_format_pagination(_r))
    request.args = _args