import json, os
from flask import current_app as app
from flask import abort
from backend.urap.mangle_helpers import doc_people_user_exists
from backend.urap.mangle_helpers import doc_people_user_duplicates
from bson.objectid import ObjectId
    
def assets_pre_patch_request_mangle(request, lookup):
    '''
        asset update validation rules:
        - current user must be member of the target asset to be allowed to patch it
        - current user must be member in the new people list proposed for patching
        - people list cannot be empty
        - clean up publication_metadata.cites for duplicates
    '''
    _id = request.view_args['_id']
    _token = request.headers.environ['HTTP_AUTHORIZATION']
    _tq = app.data.driver.db['tokens'].find_one({'token': _token})
    _user = app.data.driver.db['people'].find_one({'urap_id': _tq['urap_id']})
    app.logger.debug(_user)
    _doc = app.data.driver.db['assets'].find_one({'_id': ObjectId(_id)})
    _tuf = doc_people_user_exists(_doc, _user, _doc['index_source']['source_name'].lower())
    if not _tuf and 'admin' not in _user['roles']:
        abort(428, "Not allowed to modify the asset.")
    try:
        try:
            _data = json.loads(request.data)
        except json.decoder.JSONDecodeError:
            _data = request.form.to_dict(flat=False)

        _ak = _data.keys()
        if 'people' in _ak or 'publication_metadata' in _ak:
            if 'people' in _data.keys():
                if len(_data['people']) == 0:
                    abort(428, "People list cannot be empty.")
                _user_found = doc_people_user_exists(_data, _user, _doc['index_source']['source_name'].lower())
                if _user_found == False:
                    abort(428, "Current user not found in people list. Cannot add a document you don't own.")
                _user_duplicates = doc_people_user_duplicates(_data)
                if _user_duplicates == True:
                    abort(428, "People list contains duplicates. Cannot add or patch a document with duplicates.")

            if 'publication_metadata' in _data.keys():
                if 'cites' in _data['publication_metadata'].keys():
                    if len(_data['publication_metadata']['cites']) > 1:
                        _data['publication_metadata']['cites'] = list(set(_data['publication_metadata']['cites']))
            
            request.data = json.dumps(_data)
    except Exception as e:
        app.logger.debug('[assets_pre_patch_request] rquest.data: {}'.format(vars(request)))
        app.logger.exception('[assets_pre_patch_request] {}'.format(str(e)))
        pass

def assets_post_get_response_mangle(request, payload):
    '''
        add person_id for assets where people.person_id is missing;
    '''
    data = json.loads(payload.data)
    if 'people' in data.keys():
        people = app.data.driver.db['people']
        for e in data['people']:
            if 'person_id' not in e.keys() and 'source_author_id' in e.keys():
                p = people.find_one({'identifiers.'+data['index_source']['source_name'].lower()+'.ids': {'$in': [e['source_author_id']]}})
                if p is not None:
                    e['person_id'] = p['urap_id']
        payload.data = json.dumps(data)