import json, os
from flask import request, Blueprint, abort, make_response, Response
from flask import current_app as app
from datetime import datetime

from backend.urap import api
from backend.urap.security import blueprint_auth
from backend.urap.helpers import get_user_from_token

api_path = os.path.join(api.URL_PREFIX, api.API_VERSION)

reports_service = Blueprint('reports_service', __name__)
reports_service.before_request(blueprint_auth)

@reports_service.route(api_path + '/reports', defaults={'rtype': None})
@reports_service.route(api_path + '/reports/<rtype>', methods=['GET'])
def get_user_report(rtype):    
    '''
        headers:
            report_year: int
    '''
    
    response = None
    _user = get_user_from_token(request.headers['Authorization'])
    if not _user:
        return abort(404, 'User not found or not enabled')
    _dp = app.data.driver.db['departments'].find_one({'id': _user['department']})
    if not _dp:
        return abort(404, 'User\'s department not found.')
    
    if rtype == None:
        return Response(response=json.dumps(_dp.get('reports', {})), status=200)
    
    if rtype not in _dp.get('reports',{}).keys():
        return abort(404, 'Report type unknown')

    if 'report_year' in request.headers:
        report_year = int(request.headers['report_year'])
    else:
        report_year = datetime.now().year
    
    _init = __import__('backend.urap.reports_modules.'+_dp['reports'][rtype]['module'], globals(), locals(), ['init']).init
    return _init(_user, report_year, rtype)
