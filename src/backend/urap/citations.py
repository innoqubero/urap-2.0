#!/usr/bin/env python3

from eve.auth import requires_auth
from bson import json_util, ObjectId
from flask import request, Blueprint

from eve.auth import TokenAuth
from flask import current_app as app


citations_service = Blueprint('citations_service', __name__)


@citations_service.route('/api/v2/citations/<asset_id>', methods=['GET'])
def get_citations(asset_id):
    assets = app.data.driver.db['assets']
    asset = assets.find_one({'asset_id': asset_id})
    citations = asset['publication_metadata']['cites']
    
    assets_to_return = []
    for citation in citations:
        try:
            a = assets.find_one({'asset_id': citation})
            authors_str = ''
            for author in a['people']:
                authors_str += author['name'] + '; '

            citation_metadata = [a['asset_id'], a['title'], authors_str, a['year']]
            assets_to_return.append(citation_metadata)
        except TypeError:
            pass

    return json_util.dumps(assets_to_return), 200, {'ContentType':'application/json'}


@citations_service.route('/api/v2/editcites/<asset_id>', methods=['PATCH'])
def update_citations_list(asset_id):
    pass