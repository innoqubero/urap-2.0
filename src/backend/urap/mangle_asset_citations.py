import json, os
from backend.urap.mangle_helpers import dt_asset_format
from backend.urap.mangle_helpers import dt_assets_table_columns
from backend.urap.mangle_helpers import dt_default_response
from backend.urap.mangle_helpers import dt_format_pagination
from flask import current_app as app, abort, Response, jsonify
from bson.json_util import dumps

def dt_asset_citations_response_mangle(request, payload):
    '''
		Format the response payload to match DataTables expected value
	'''
    payload.data = json.dumps(dt_asset_format(payload.data, 'publication'))

def dt_asset_citations_request_mangle(request, lookup):
    _r = request.args
    _args = request.args.copy()
    _index_citations_sources = []
    _index_citations_ids = []
    if 'asset_id' in _r.keys():
        assets = app.data.driver.db['assets']
        aid = assets.find_one({'asset_id': _r['asset_id']})
        if aid is not None:
            _citations_list = aid['publication_metadata']['cites']
            app.logger.debug('_citations_list value: {}'.format(_citations_list))
            if type(_citations_list) is not list:
                app.logger.error('No citations found for asset {}'.format(_r['asset_id']))
                abort(Response(response=json.dumps(dt_default_response), status=200))
            if len(_citations_list) == 0:
                app.logger.error('No citations found for asset {}'.format(_r['asset_id']))
                abort(Response(response=json.dumps(dt_default_response), status=200))
            for c in _citations_list:
                cit = c.split(":")
                _index_citations_sources.append(cit[0])
                _index_citations_ids.append(cit[1])
            _args['where'] = json.dumps({'index_source.source_name':{'$in':  list(set(_index_citations_sources))}, 'index_source.source_id':{'$in':  list(set(_index_citations_ids))}})
            app.logger.debug('where filter: {}'.format(_args['where']))
        else:
            abort(Response(response=json.dumps(dt_default_response), status=200))
    else:
        abort(Response(response=json.dumps(dt_default_response), status=200))
    
    _args.update(dt_format_pagination(_r))
    
    app.logger.info('mangled request.args: {}'.format(json.dumps(_args)))
    request.args = _args