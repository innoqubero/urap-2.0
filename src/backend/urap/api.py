import os
from backend.urap.views import views_db

MONGO_HOST = os.getenv('URAP_MONGO_HOST', 'localhost')
MONGO_PORT = int(os.getenv('URAP_MONGO_PORT', 27017))
#MONGO_USERNAME = os.getenv('URAP_MONGO_USERNAME', 'urap')
#MONGO_PASSWORD = os.getenv('URAP_MONGO_PASSWORD', 'urap.application')
MONGO_AUTH_SOURCE = os.getenv('URAP_MONGO_AUTH_SOURCE', 'admin')
MONGO_DBNAME = os.getenv('URAP_MONGO_DBNAME', 'urap')
MONGO_CONNECT_TIMEOUT_MS = 4000

API_VERSION = 'v2'
URL_PREFIX = '/api' 
DEBUG = True

OPTIMIZE_PAGINATION_FOR_SPEED = False

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT']

URAP_UI_URL = 'https://urap.sage.ieat.ro/ui'

# DOMAIN = {}
# for k in views_db.keys():
#    DOMAIN[k] = views_db[k]

DOMAIN = {
    'assets': views_db['asset'],
    'people': views_db['person'],
    'departments': views_db['department'],
    'userassets': views_db['user_assets'],
    'assetbyindexsource': views_db['asset_by_index_source'],
    'assetcitations': views_db['asset_citations'],
    'assetsalltitleid': views_db['all_assets_title_id'],
    'rankings': views_db['ranking']
}

#MONGO_QUERY_BLACKLIST = ['$where']
AUTO_COLLAPSE_MULTI_KEYS = True
AUTO_CREATE_LISTS = True

X_DOMAINS = '*'
X_HEADERS = '*'
#X_HEADERS = ['Content-Type', 'If-Match', 'Authorization', 'cache-control']
