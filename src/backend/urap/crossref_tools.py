#!/usr/bin/env python3

import os
import requests
from eve.auth import requires_auth
import json
from flask import Flask, abort, Blueprint

from eve.auth import TokenAuth
from flask import current_app as app, abort

from backend.urap import api
from backend.urap.helpers import get_name_combinations
from backend.urap.helpers import get_asset_from_crossref

api_path = os.path.join(api.URL_PREFIX, api.API_VERSION)

crossref_tools = Blueprint('crossref_tools', __name__)

@crossref_tools.route(api_path + '/getdatafromdoi/<path:doi>', methods=['GET'])
def get_from_doi(doi):
    res = requests.get(f'https://api.crossref.org/v1/works/{doi}')
    if res.status_code == 200:
        data = None
        try:
            data = json.loads(res.text)
        except:
            abort(404, 'crossref response not in JSON format.')
        asset = get_asset_from_crossref(data)
        return asset
    else:
        abort(404, 'specified DOI not found.')


def get_datacite_citations_from_doi(doi):
    headers = {
        'Accept-Encoding': 'gzip, deflate, br',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Keep-Alive': 'timeout=30, max=100',
        'Origin': 'https://api.datacite.org',
    }

    data = '{"query":"{ work(id: \\"%s\\") { id citationCount citations { nodes { id } } }}"}' % doi
    response = requests.post('https://api.datacite.org/graphql', headers=headers, data=data)

    if response.status_code == 200:
        resp = response.json()

        try:
            if resp['errors']:
                return []
            else:
                citations = resp['data']['work']['citations']['nodes']
                citations = map(lambda x: x['id'], citations)

                return citations
        except KeyError:
            return []

    elif response.status_code == 404:
        return []


def get_crossref_citations_from_doi(doi):
    citations_doi = []
    res = requests.get(f'https://opencitations.net/index/api/v1/citations/{doi}')
    if res.status_code == 200:
        citations = json.loads(res.text)
        for cite in citations:
            citations_doi.append(cite['citing'].split('>')[1].strip())
        return citations_doi


@crossref_tools.route(api_path + '/getcitations/<path:doi>', methods=['GET'])
def get_cites(doi):
    cites = get_crossref_citations_from_doi(doi) + get_datacite_citations_from_doi(doi)

    return json.dumps(list(set(cites)))