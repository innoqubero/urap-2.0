import yaml, os

_cwd = os.path.dirname(os.path.abspath(__file__))

_schema_list = ['person', 'department', 'asset', 'ranking']

schema_db = {}

for s in _schema_list:
    _schema_file = os.path.join(_cwd, '../schemas/{}.yaml'.format(s))
    if os.path.isfile(_schema_file):
        with open(_schema_file) as fp:
            schema_db[s] = yaml.load(fp, Loader=yaml.FullLoader)
    else:
        raise FileNotFoundError('Internal schema file ({}) not found'.format(_schema_file))
