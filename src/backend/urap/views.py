from backend.urap.schema import schema_db

views_db = {}

# if you want to override the default setup just define definition_db['schema_entry'] below this iteration


for k in schema_db.keys():
    views_db[k] = {
        'item_title': '{}'.format(k),
        'cache_control': 'max-age=10,must-revalidate',
        'cache_expires': 10,
        'resource_methods': ['GET', 'POST'],
        'schema': schema_db[k]
    }

views_db['person'] = {
    'item_title': 'person',
    'additional_lookup': {
        'url': 'string',
        'field': 'urap_id'
    },
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,
    'resource_methods': ['GET'],
    'item_methods': ['GET', 'PATCH'],
    'schema': schema_db['person'],
    'id_field': 'urap_id'
}

views_db['asset'] = {
    'item_title': 'asset',
    'additional_lookup': {
        'url': 'string',
        'field': 'asset_id'
    },
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,
    'resource_methods': ['GET', 'POST'],
    'item_methods': ['GET', 'PUT', 'PATCH', 'DELETE'],
    'schema': schema_db['asset'],
    'id_field': 'asset_id'
}

views_db['asset_by_index_source'] = {
    'item_title': 'assets readonly',
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,
    'resource_methods': ['GET'],
    'item_methods': ['GET'],
    'url': 'asset/by-index-source',
    'datasource' : {
        'source': 'assets',
        'projection': {
            'index_source': 1
        }
    }
}

views_db['all_assets_title_id'] = {
    'item_title': 'all assets title and id',
    'resource_methods': ['GET'],
    'item_methods': ['GET'],
    'url': 'assets/all-title-id',
    'cache_expires': 10,
    'cache_control': 'max-age=10,must-revalidate',
    'datasource' : {
        'source' : 'assets',
        'sort' : { 'year' : -1},
        'projection' : {
            'asset_id' : 1,
            'title': 1
        }
    }
}

views_db['ranking'] = {
    'item_title': 'ranking',
        'additional_lookup': {
            'url': 'string',
            'field': 'issn'
        },
        'cache_control': 'max-age=10,must-revalidate',
        'cache_expires': 10,
        'resource_methods': ['GET', 'POST'],
        'item_methods': ['GET', 'PUT', 'PATCH'],
        'schema': schema_db['ranking'],
        'id_field': 'issn'
}

views_db['user_assets'] = {
    'item_title': 'user assets list',
    'resource_methods': ['GET'],
    'item_methods': ['GET'],
    'url': 'dt/user/assets',
    'cache_expires': 10,
    'cache_control': 'max-age=10,must-revalidate',
    'datasource' : {
        'source' : 'assets',
        'sort' : { 'year' : -1},
        'projection' : {
            'asset_id' : 1,
            'title': 1,
            'people': 1,
            'year': 1,
            'publication_metadata': 1,
            'project_metadata': 1
        }
    }
}

views_db['asset_citations'] = {
    'item_title': 'asset citations list',
    'resource_methods': ['GET'],
    'item_methods': ['GET'],
    'url': 'dt/asset/citations',
    'cache_expires': 10,
    'cache_control': 'max-age=10,must-revalidate',
    'datasource' : {
        'source' : 'assets',
        'sort' : { 'year' : -1},
        'projection' : {
            'asset_id' : 1,
            'title': 1,
            'people': 1,
            'year': 1,
            'publication_metadata': 1
        }
    }
}