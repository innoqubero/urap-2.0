from itertools import combinations
from flask import current_app as app
from unidecode import unidecode
from fuzzywuzzy import fuzz

from backend.urap.types import crossref_types

def match_uvt_names(full_name):
    n = full_name.lower().split(',')
    #n = full_name.replace(',', ' ').split(' ')
    names = []
    if len(n) != 2:
        return []
    names.extend(n[0].strip().split(' '))
    names.append(n[1].strip())
    names.extend(n[1].strip().replace('-', ' ').split(' '))
    firsts = [{'firstname': {'$regex': x.replace('.', '\.'), '$options': 'i'}} for x in names]
    lasts = [{'lastname': {'$regex': x.replace('.', '\.'), '$options': 'i'}} for x in names]
    regexs = [*firsts, *lasts]
    #print('[--] authors regex: {}'.format(regexs))
    result = list(app.data.driver.db['people'].find({'$or': regexs }, {'_id': 0, '_create': 0, '_updated': 0}))
    #print('-------------------------------', len(result))
    _person_ids = []
    for r in result:
        anames = []
        anames.extend(get_name_combinations(r['lastname'].lower().strip(), with_name_split=False))
        anames.extend(get_name_combinations(r['firstname'].lower().strip(), with_initial=True))
        
        #print(f'Values: {names} / {anames}')
        #print(f'Sizes: {len(names)} / {len(anames)}')
        
        if match_name(names, anames):
            _person_ids.append(r['urap_id'])
    return _person_ids

def match_name(fnames, unames, l=99):
    fl = len(fnames)
    ul = len(unames)
    if l < 2:
        return False

    if l == 99:
        if fl > ul:
            l = ul
        elif fl < ul:
            l = fl
        else:
            l = fl - 1

    s1 = [set(x) for x in combinations(fnames[1:], l-1)]
    s2 = [set(x) for x in combinations(unames[1:], l-1)]
    
    #print(f'+++++\n{fnames}\n{unames}\n+++++')
    #print(f'-----\n{s1}\n{s2}\n-----')
    
    for c in s1:
        c.add(fnames[0])
        for d in s2:
            d.add(unames[0])
            if c == d:
                #print(f'--matched: {c} // {d}')
                return True

    return match_name(fnames, unames, l=l-1)
        
def match_asset(asset):
    ids = []
    _filter = {}
    if 'doi' in asset['publication_metadata'].keys():
        _filter['publication_metadata.doi'] = asset['publication_metadata']['doi']
    _filter['year'] = asset['year']
    result = list(app.data.driver.db['assets'].find(_filter))
    if len(result) > 0:
        for r in result:
            ids.append(r['asset_id'])
    else:
        a = match_asset_title(asset['title'])
        if a:
            for b in a:
                ids.append(b['asset_id'])
    return ids

def get_user_from_token(token_id):
    _tk = app.data.driver.db['tokens'].find_one({'token': token_id})
    if _tk:
        _ud = app.data.driver.db['people'].find_one({'urap_id': _tk['urap_id']})
        if _ud:
            if _ud.get('enabled', -99) != 1:
                return None
            return _ud
        else:
            return None 
    else:
        return None

def get_name_combinations(name_string, with_initial=False, with_name_split=True):
    names = []
    if name_string == '':
        return names
    if with_name_split:
        name_string = name_string.replace('-', ' ')
    if len(name_string.split(' ')) > 1:
        _fs = name_string.split(' ')
        #names.append(name_string.lower())
        _initial = []
        for e in _fs:
            names.append(e.lower())
            if with_initial:
                _initial.append(f'{e[0].lower()}.')
                _initial.append(f'{e[0].lower()}')
        if with_initial:
            names.append(' '.join(_initial))
            names.append(f'{_fs[0]} {" ".join(_initial[1:])}')
            names.append(_fs[0][0].lower() + '.')
            names.append(_fs[0][0].lower())

    else:
        names.append(name_string.lower())
        if with_initial:
            names.append(name_string[0].lower() + '.')
    return names

def get_asset_from_crossref(data):
    a = {}
    a['asset_type'] = 'publication'
    a['title'] = unidecode(data['message']['title'][0])
    try:
        a['abstract'] = unidecode(data['message']['abstract'].replace('<jats:p>', '').replace('</jats:p>', ''))
    except KeyError:
        a['abstract'] = ''
    a['keywords'] = []
    a['year'] = data['message']['issued']['date-parts'][0][0]
    a['people'] = []
    atidx = 1
    for at in data['message']['author']:
        ato = {}
        ato['name'] = '{}, {}'.format(unidecode(at['family']), unidecode(at['given'])).replace('-', ' ')
        ato['index'] = atidx
        pid = match_uvt_names(ato['name'])
        if len(pid) == 1:
            ato['person_id'] = pid[0]
            a['is_uvt'] = 1
        atidx += 1
        a['people'].append(ato)
    a['publication_metadata'] = {}
    a['publication_metadata']['name'] = data['message']['publisher']
    try:
        a['publication_metadata']['issue'] = data['message']['journal-issue']['issue']
    except:
        a['publication_metadata']['issue'] = ''
    a['publication_metadata']['volume'] = data['message'].get('volume', '')
    a['publication_metadata']['pages'] = data['message'].get('page', '')
    a['publication_metadata']['type'] = crossref_types[data['message']['type']]['id']
    a['publication_metadata']['doi'] = data['message']['DOI']
    a['publication_metadata']['issn'] = ''
    a['publication_metadata']['eissn'] = ''
    try:
        for e in data['message']['issn-type']:
            if e['type'] == 'print':
                a['publication_metadata']['issn'] = e['value']
            if e['type'] == 'electronic':
                a['publication_metadata']['eissn'] = e['value']
    except:
        pass
    a['publication_metadata']['isbn'] = ''
    a['publication_metadata']['eisbn'] = ''
    try:
        for e in data['message']['isbn-type']:
            if e['type'] == 'print':
                a['publication_metadata']['isbn'] = e['value']
            if e['type'] == 'electronic':
                a['publication_metadata']['eisbn'] = e['value']
    except:
        pass
    return a

def match_asset_title(asset_title, range_step=0):
    results = []
    _match_level = 97
    _split_ratio = 0.2
    _title_tokens = asset_title.split(' ')
    _split_factor = round(len(_title_tokens)*_split_ratio)
    if _split_factor == 0:
        return []
    _stop = False
    if range_step == 0:
        _filter = {'$text': {'$search': '"{}"'.format(asset_title)}}
    else:
        _test_title = ' '.join(_title_tokens[0:range_step])
        _filter = {'$text': {'$search': '{}'.format(_test_title)}}
    if range_step + _split_factor >= len(_title_tokens):
        _stop = True
     
    
    result = list(app.data.driver.db['assets'].find(_filter))
    print('_filter', _filter)
    print('_filter', len(result))
    if len(result) > 1:
        if len(result) == 1:
            _match = fuzz.ratio(result[0]['title'].lower(), asset_title.lower())
            app.logger.debug('fwuzzy ratio 1: {}'.format(_match))
            if _match >= _match_level:
                results.append(result)
                _stop = True
        else:
            for r in result:
                _match = fuzz.ratio(r['title'].lower(), asset_title.lower())
                if _match >= _match_level:
                    app.logger.debug('fwuzzy ratio 2: {}'.format(_match))
                    results.append(r)
                    _stop = True
    if _stop:
        app.logger.debug('stopping recursion ...')
        return results
    return match_asset_title(asset_title, range_step=range_step+_split_factor)

def live_title_search(asset_title):
    _filter = {'$text': {'$search': '"{}"'.format(asset_title)}}
    app.logger.debug(_filter)
    result = list(app.data.driver.db['assets'].find(_filter))
    ret = []
    for r in result:
        ret.append({
            "asset_id": r['asset_id'],
            "title": r['title'],
            'year': r['year'],
            'pubtype': r['publication_metadata']['type']
        })
    return ret