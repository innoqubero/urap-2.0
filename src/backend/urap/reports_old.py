#!/usr/bin/env python3

from abc import ABC, abstractmethod

import requests
from eve.auth import requires_auth
import json
from flask import request, Blueprint
from datetime import datetime

# from backend.urap import api
# from backend.urap import bindings

from eve.auth import TokenAuth
from flask import current_app as app

import yaml

reports = Blueprint('reports', __name__)


class ReportFactory:
    def __init__(self):
        self._domains = {"administrarea afacerilor": EconomicsReport(),
                         "asistenta sociala": PsychologyEducationSportReport(),
                         "chimie": ChemistryReport(),
                         "psihologie": PsychologyEducationSportReport(),
                         "sociologie": SociologyReport(),
                         "stiinte ale comunicarii": SociologyReport(),
                         "stiinte ale educatiei": PsychologyEducationSportReport(),
                         "stiinte politice": SociologyReport(),
                         "teatru si artele spectacolului": ShowArtsReport(),
                         "teologie": TheologyReport(),
                         "arte vizuale": VisualArtsReport(),
                         "biologie": BiologyReport(),
                         "cibernetica statistica si informatica economica": EconomicsReport(),
                         "contabilitate": EconomicsReport(),
                         "drept": JuridicalSciencesReport(),
                         "economie": EconomicsReport(),
                         "economie si afaceri internationale": EconomicsReport(),
                         "filosofie": PhilosophyReport(),
                         "finante": EconomicsReport(),
                         "fizica": PhysicsReport(),
                         "geografie": EarthSciencesReport(),
                         "informatica": CSReport(),
                         "istorie": HistoryReport(),
                         "kinetoterapie": PsychologyEducationSportReport(),
                         "limba si literatura": PhilologyReport(),
                         "limbi moderne aplicate": PhilologyReport,
                         "management": EconomicsReport(),
                         "marketing": EconomicsReport(),
                         "matematica": MathReport(),
                         "muzica": ShowArtsReport(),
                         "muzica (interpretare muzicala)": ShowArtsReport()}

    def get_report(self, person_id, token):
        people = app.data.driver.db['people']
        person = people.find_one({'urap_id': person_id})
        department = person['department']['name']
        return self._domains[department].generate_report(person_id, token)


class Report(ABC):
    def __init__(self):
        pass

    def get_criteria(self, domain):
        if domain is not None:
            self._spec = reports.open_resource(f'../report_schemas/{domain}.yml')
            self._criteria = yaml.load(self._spec.read())

    @abstractmethod
    def generate_report(self, person_id, tok):
        raise NotImplementedError

    def get_assets(self, p):
        try:
            headers = {'Authorization': tok}
            assets = requests.get(f'http://localhost/api/v2/dt/user/assets?urap_id={person_id}',
                                  headers=headers)
            if assets.status_code == 200:
                return assets.json()
        except Exception:
            return None


class VisualArtsReport(Report):
    def generate_report(self, person_id, tok):
        pass


class ShowArtsReport(Report):
    def generate_report(self, person_id, tok):
        pass


class BiologyReport(Report):
    def generate_report(self, person_id, tok):
        pass


class ChemistryReport(Report):
    def generate_report(self, person_id, tok):
        pass


class PhilologyReport(Report):
    def generate_report(self, person_id, tok):
        pass


class PhilosophyReport(Report):
    def generate_report(self, person_id, tok):
        pass


class PsychologyEducationSportReport(Report):
    def generate_report(self, person_id, tok):
        pass


class PhysicsReport(Report):
    def generate_report(self, person_id, tok):
        pass


class HistoryReport(Report):
    def generate_report(self, person_id, tok):
        pass


class SociologyReport(Report):
    def generate_report(self, person_id, tok):
        pass


class JuridicalSciencesReport(Report):
    def generate_report(self, person_id, tok):
        pass


class TheologyReport(Report):
    def generate_report(self, person_id, tok):
        pass


class EarthSciencesReport(Report):
    def generate_report(self, person_id, tok):
        pass


class MathReport(Report):
    current_year = datetime.now().year
    criteria_A_isi_level = 0.5
    critatia_A_window = 5
    criteria_A_uefiscdi_ranking = 'RIS' 
    
    def _compute_criteria_a(self):
        #_years = [current_year-x for x in range(1,6)]
        #_asset_list = [a for a in self._assets if a.year in _years]
        _ranked_list = []
        for a in self._assets:
            pass



    def generate_report(self, person_id, tok):        
        self._assets = self.get_assets(person_id, tok)

        data = []

        for asset in self._assets:
            tmp = self._criteria.copy()
            tmp['perspective']['columns']['']
            data.append(self._criteria['perspective'])

        return json.dumps(data)

class CSReport(Report):
    def __init__(self):
        pass

    def generate_report(self, person_id, tok):
        self._assets = self.get_assets(person_id, tok)
        self.get_criteria('informatica')

        return json.dumps({'dept': 'CS', 'data': self._assets})


class EconomicsReport(Report):
    def generate_report(self, person_id, tok):
        pass


rf = ReportFactory()


@reports.route('/api/v2/reports/<user_id>', methods=['GET'])
def generate_report(user_id):
    tok = request.headers.get('Authorization').split(' ')[1]
    return rf.get_report(user_id, tok)
