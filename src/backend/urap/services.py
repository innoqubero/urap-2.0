import json, os
from flask import request, Blueprint, abort, Response
from flask import current_app as app
from datetime import datetime

from backend.urap import api
from backend.urap.security import blueprint_auth
from backend.urap.helpers import match_uvt_names
from backend.urap.helpers import match_asset
from backend.urap.helpers import live_title_search

api_path = os.path.join(api.URL_PREFIX, api.API_VERSION)

urap_services = Blueprint('urap_services', __name__)
urap_services.before_request(blueprint_auth)

@urap_services.route(api_path + '/services/getUrapIdByName', methods=['GET'])
def srv_get_urap_id_by_name():
    '''
        header:
            fullname: "lastname, firstname"
        response:
            200: {'urap_id': [uuid()]}
            404: if not found
    '''
    try:
        _fullname = request.headers['fullname']
    except:
        abort(404, 'No URAP user matched')
    _person_id = match_uvt_names(_fullname)
    if len(_person_id) > 0:
        return Response(response=json.dumps({'urap_id': _person_id}), status=200)
    else:
        abort(404, 'No URAP user matched')

@urap_services.route(api_path + '/services/matchAsset', methods=['POST'])
def srv_match_asset():
    '''
        POST: asset_object
        RESPONSE:
            200: {'asset_id': [uuid()]}
            404: if not matched
    '''
    try:
        _data = json.loads(request.data)
    except:
        abort(404, 'Asset document not properly formatted!')
    _ids = match_asset(_data)
    if len(_ids) == 0:
        abort(404, 'No asset matched!')
    return Response(response=json.dumps({'asset_id': _ids}), status=200)

@urap_services.route(api_path + '/services/liveTitleSearch', methods=['POST'])
def srv_live_title_search():
    '''
        POST: {'title': '...'}
        RESPONSE:
            200: 
            404: if not matched
    '''
    try:
        _data = json.loads(request.data)
        _title = _data['title']
    except:
        abort(404, 'Asset document not properly formatted!')
    return Response(response=json.dumps(live_title_search(_title)), status=200)