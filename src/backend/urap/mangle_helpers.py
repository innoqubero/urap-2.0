import json
from flask import current_app as app

dt_default_response = {
  "recordsTotal": 25,
  "recordsFiltered": 0,
  "data": []
}

dt_assets_table_columns = {
    "publication": {
        "0": {"mongo_key": "asset_id", "column_title" : "ID", "type": "str"}, 
        "1": {"mongo_key": "title", "column_title" : "Titlu", "type": "str"}, 
        "2": {"mongo_key": "people", "column_title" : "Autori", "type": "str"}, 
        "3": {"mongo_key": "year", "column_title" : "An", "type": "int"},
        "4": {"mongo_key": "publication_metadata", "column_title" : "metadata", "type": "str"} 
    },
    "project": {
        "0": {"mongo_key": "asset_id", "column_title" : "ID", "type": "str"}, 
        "1": {"mongo_key": "title", "column_title" : "Titlu", "type": "str"}, 
        "2": {"mongo_key": "people", "column_title" : "Membrii", "type": "str"}, 
        "3": {"mongo_key": "year", "column_title" : "An", "type": "int"},
        "4": {"mongo_key": "project_metadata", "column_title" : "Buget", "type": "int"} 
    }
}

def dt_format_pagination(args):
    _args = {}
    # translate dataTables pagination into pyEve max_results=&page= format
    # if max_results in URL then do nothing as pyEve searches
    if 'length' in args.keys() and 'max_results' not in args.keys():
        _args['max_results'] = args['length']
        if 'start' in args.keys():
            if int(args['length']) > 0: 
                _args['page'] = (int(args['start'])//int(args['length'])) + 1
    # translate dataTables sorting into pyEve sort=[-]collection_key
    if 'order[0][column]' in args.keys():
        if 'order[0][dir]' in args.keys():
            _sort = ''
            if args['order[0][dir]'] == 'desc':
                _sort += '-'
            _sort += dt_assets_table_columns['publication'][args['order[0][column]']]['mongo_key']
            _args['sort'] = _sort
    return _args

def dt_asset_format(data, asset_type):
    rdata = json.loads(data)
    data = {}
    data['recordsTotal'] = rdata['_meta']['max_results']
    data['recordsFiltered'] = rdata['_meta']['total']
    if 'next' in rdata['_links'].keys():
        data['next'] = rdata['_links']['parent']['href'] + rdata['_links']['next']['href']
    items = []
    for i in rdata['_items']:
        _last = None
        _pubtype = None
        if asset_type == 'publication':
            if 'cites' in i['publication_metadata'].keys():
                _last = 0 if (type(i['publication_metadata']['cites']) is int or type(i['publication_metadata']['cites']) is str) else len(i['publication_metadata']['cites'])
            else:
                _last = 0
            _pubtype = i['publication_metadata']['type']
        if asset_type == 'project':
            if 'budget' in i['project_metadata'].keys():
                _last = i['project_metadata']['budget']
            else:
                _last = 0
            _pubtype = ''
        people_str = ''
        for j in i['people']:
            people_str += j['name'] + '; '
        if len(people_str) > 255:
            people_str = people_str[0:252] + '...'
        items.append(
            [
                i['asset_id'],
                i['title'],
                people_str,
                i['year'],
                _last,
                _pubtype

            ]
        )
    data['data'] = items
    del(rdata)
    return data

def doc_people_user_exists(_data, _user, doc_source_name):
    if 'people' in _data.keys():
        for p in _data['people']:
            if 'person_id' in p.keys():
                if p['person_id'] == _user['urap_id']:
                    return True
            if 'source_author_id' in p.keys():
                if p['source_author_id'] in _user['identifiers'][doc_source_name]['ids']:
                    return True
    return False

def doc_people_user_duplicates(_data):
    _dname = []
    _pid = []
    _sid = []
    if 'people' in _data.keys():
        for p in _data['people']:
            try:
                if p['source_author_id'] in _sid:
                    return True
                else:
                    _sid.append(p['source_author_id'])
            except:
                pass
            try:
                if p['person_id'] in _pid:
                    return True
                else:
                    _pid.append(p['person_id'])
            except:
                pass
            if p['name'] in _dname:
                return True
            else:
                _dname.append(p['name'])
    return False

def get_mongo_filter_user_assets(urap_id, asset_type=None, is_uvt=1):
    uid = app.data.driver.db['people'].find_one({'urap_id': urap_id})
    if not uid:
        return uid
    _filter = {}
    _ids = []
    if asset_type not in ['project']:
        for i in uid['identifiers']['wos']['ids']:
            if i != '':
                _ids.append(i)
        for i in uid['identifiers']['scopus']['ids']:
            if i != '':
                _ids.append(i)
    _filter['$or'] = [
        {
            'people.person_id': urap_id
        }
    ]
    if len(_ids) > 0:
        _filter['$or'].append(
            {
                'people.source_author_id':{
                    '$in': _ids
                }
            }
        )
    _filter['is_uvt'] = is_uvt
    if asset_type is not None:
        _filter['asset_type'] = asset_type
    return _filter
    
