import json, os
from flask import current_app as app
from flask import abort
from bson.objectid import ObjectId


def person_pre_patch_request_mangle(request, lookup):
    _token = request.headers.environ['HTTP_AUTHORIZATION']
    _db = app.data.driver.db['tokens']
    _tq = _db.find_one({'token': _token})
    if not _tq:
        app.logger.error('Token {} not registered.'.format(_token))
        abort(403)
    _id = request.view_args['_id']
    _usr = app.data.driver.db['people'].find_one({'_id': ObjectId(_id)})
    if not _usr:
        app.logger.error('Token {} tried to update data for user _id {}'.format(_token, _id))
        abort(403)
    if _usr['urap_id'] != _tq['urap_id']:
        app.logger.error('Token {} tried to patch user profile for {}. Not authorized'.format(_token, _usr['_urap_id']))
        abort(403)