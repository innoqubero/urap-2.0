
import json
from datetime import datetime
from flask import current_app as app, make_response
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from tempfile import NamedTemporaryFile
from openpyxl.styles import Font, Color, Alignment, Border, Side, colors

from backend.urap.reports_modules.helpers import uvt_academic_titles 

def init(_user, report_year, rtype):
    app.logger.debug('{}/{}'.format(report_year, rtype))
    _ufn = _user['firstname'].capitalize() + ' ' + _user['lastname'].capitalize()
    math = MathReport(_user['urap_id'], 
                    source_ids=_user['identifiers']['wos']['ids'] + _user['identifiers']['scopus']['ids'], 
                    year=report_year, 
                    full_name=_ufn, 
                    academic_title=_user['position'])
    response = make_response(math.generate())
    response.headers.set('Content-Type', 'application/x-xlsx')
    response.headers.set(
        'Content-Disposition', 'attachment', filename='{}_matematics_{}_report_{}_{}.xlsx'.format(report_year, rtype, _user['firstname'].lower(), _user['lastname'].lower()))
    app.logger.debug(response)
    return response

class MathReport(object):

    def __init__(self, person_id, source_ids=[], year=None, delta=5, if_min=0.5, ranking_collection='rankings_journals', full_name='', academic_title=''):
        self.person_id = person_id
        self.source_ids = source_ids
        if year:
            self.year = year
        else:
            self.year = datetime.now().year
        self.delta = delta
        self.max_year = year - 1
        self.min_year = self.max_year - self.delta + 1
        self.if_min = if_min
        self.ranking_collection = ranking_collection
        self.full_name = full_name
        try:
            self.academic_title = uvt_academic_titles[academic_title]['title']
        except:
            self.academic_title = ''
    
    def generate(self):
        return self._assets_classifier()

    def _assets_classifier(self):
        '''
            this method will use a mongodb aggregation filter, specific to the current domain
            and it will classify user's assets based on person_id and source_ids;
            the resulting dataset will also contain a list of citations, embedded into the document itself
            using root level key `cites` (this can change on mongodb filter)
        '''

        _acr = list(app.data.driver.db['assets'].aggregate(self._get_user_assets_classifier()))
        # get a unique list of asset_id from the mongo aggregration performed above
        _all_citation_ids = list(set([ c['asset_id'] for r in _acr for c in r['cites']]))
        # get a classified list with the above citations
        _ccr = list(app.data.driver.db['assets'].aggregate(self._get_citations_classifier(_all_citation_ids)))
        
        return self._generate_report_doc(user_fullname=self.full_name, 
                                         user_title=self.academic_title, 
                                         papers_list=_acr,
                                         citations_list=_ccr)

    def _get_user_assets_classifier(self):

        _user_asset_classifier = [
            {
                '$match': {
                    '$and': [
                        {
                            '$or': [
                                {
                                    'people.person_id': ''
                                }, {
                                    'people.source_author_id': {
                                        '$in': []
                                    }
                                }
                            ]
                        }, {
                            'is_uvt': 1,
                            'asset_type': 'publication',
                            'year': { '$lte': 1800}
                        }
                    ]
                }
            }, {
                '$lookup': {
                    'from': 'assets', 
                    'let': {
                        'cites_list': '$publication_metadata.cites'
                    }, 
                    'pipeline': [
                        {
                            '$match': {
                                '$expr': {
                                    '$and': [
                                        {
                                            '$in': [
                                                {
                                                    '$concat': [
                                                        '$index_source.source_name', ':', '$index_source.source_id'
                                                    ]
                                                }, '$$cites_list'
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    ], 
                    'as': 'cites'
                }
            }, {
                '$lookup': {
                    'from': 'rankings', 
                    'localField': 'publication_metadata.issn', 
                    'foreignField': 'issn', 
                    'as': 'ranking'
                }
            }, {
                '$replaceRoot': {
                    'newRoot': {
                        '$mergeObjects': [
                            {
                                '$arrayElemAt': [
                                    '$ranking', 0
                                ]
                            }, '$$ROOT'
                        ]
                    }
                }
            }, {
                '$project': {
                    'ranking': 0, 
                    'rankings.uefiscdi.AIS': 0, 
                    'rankings.uefiscdi.RIF': 0, 
                    'rankings.uefiscdi.zone': 0
                }
            }, {
                '$project': {
                    'asset_id': 1, 
                    'year': 1, 
                    'title': 1, 
                    'people': 1, 
                    'publication_metadata': 1, 
                    'cites': 1, 
                    'math_a': {
                        '$filter': {
                            'input': '$rankings.uefiscdi.RIS', 
                            'cond': {
                                '$and': [
                                    {
                                        '$lte': [
                                            '$$this.year', 1800
                                        ]
                                    }, {
                                        '$gte': [
                                            '$$this.year', 2100
                                        ]
                                    }
                                ]
                            }
                        }
                    }, 
                    'n_i': {
                        '$cond': {
                            'if': {
                                '$isArray': '$people'
                            }, 
                            'then': {
                                '$size': '$people'
                            }, 
                            'else': 0
                        }
                    }
                }
            }, {
                '$addFields': {
                    'cites.cited_asset_id': '$asset_id'
                }
            }, {
                '$project': {
                    'asset_id': 1, 
                    'title': 1, 
                    'year': 1, 
                    'people': 1, 
                    'publication_metadata': 1, 
                    'cites': 1, 
                    'n_i': 1, 
                    's_i': {
                        '$max': '$math_a.if'
                    }
                }
            }, {
                '$project': {
                    'asset_id': 1, 
                    'title': 1, 
                    'year': 1, 
                    'people': 1, 
                    'publication_metadata': 1, 
                    'cites': 1, 
                    'n_i': 1, 
                    's_i': 1, 
                    's_i_n_i': {
                        '$divide': [
                            '$s_i', '$n_i'
                        ]
                    }
                }
            }, {
                '$match': {
                    's_i': {
                        '$gte': 999
                    }
                }
            }
        ]

        _user_asset_classifier[0]['$match']['$and'][0]['$or'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del  _user_asset_classifier[0]['$match']['$and'][0]['$or'][1]
        else:
            _user_asset_classifier[0]['$match']['$and'][0]['$or'][1]['people.source_author_id']['$in'] = self.source_ids
        _user_asset_classifier[0]['$match']['$and'][1]['year']['$lte'] = self.max_year
        _user_asset_classifier[2]['$lookup']['from'] = self.ranking_collection
        _user_asset_classifier[5]['$project']['math_a']['$filter']['cond']['$and'][0]['$lte'][1] = self.max_year
        _user_asset_classifier[5]['$project']['math_a']['$filter']['cond']['$and'][1]['$gte'][1] = self.min_year
        _user_asset_classifier[9]['$match']['s_i']['$gte'] = self.if_min
        app.logger.debug(json.dumps(_user_asset_classifier))
        return _user_asset_classifier

    def _get_citations_classifier(self, citation_ids):

        _citations_classifier = [
            {
                '$match': {
                    '$and': [
                        {
                            'asset_id': {
                                '$in': []
                            }
                        },
                        {
                            '$nor': [
                                {
                                    'people.person_id': ''
                                }, {
                                    'people.source_author_id': {
                                        '$in': []
                                    }
                                }
                            ]
                        }
                    ]
                }
            }, {
                '$lookup': {
                    'from': '', 
                    'localField': 'publication_metadata.issn', 
                    'foreignField': 'issn', 
                    'as': 'ranking'
                }
            }, {
                '$replaceRoot': {
                    'newRoot': {
                        '$mergeObjects': [
                            {
                                '$arrayElemAt': [
                                    '$ranking', 0
                                ]
                            }, '$$ROOT'
                        ]
                    }
                }
            }, {
                '$project': {
                    'ranking': 0, 
                    'rankings.uefiscdi.AIS': 0, 
                    'rankings.uefiscdi.RIF': 0, 
                    'rankings.uefiscdi.zone': 0
                }
            }, {
                '$project': {
                    'asset_id': 1, 
                    'year': 1, 
                    'title': 1, 
                    'people': 1, 
                    'publication_metadata': 1, 
                    'cites': 1, 
                    'math_a': {
                        '$filter': {
                            'input': '$rankings.uefiscdi.RIS', 
                            'cond': {
                                '$and': [
                                    {
                                        '$lte': [
                                            '$$this.year', 2100
                                        ]
                                    }, {
                                        '$gte': [
                                            '$$this.year', 1800
                                        ]
                                    }
                                ]
                            }
                        }
                    }, 
                    'n_i': {
                        '$cond': {
                            'if': {
                                '$isArray': '$people'
                            }, 
                            'then': {
                                '$size': '$people'
                            }, 
                            'else': 0
                        }
                    }
                }
            }, {
                '$project': {
                    'asset_id': 1, 
                    'title': 1, 
                    'year': 1, 
                    'people': 1, 
                    'publication_metadata': 1, 
                    'cites': 1, 
                    'n_i': 1, 
                    's_i': {
                        '$max': '$math_a.if'
                    }
                }
            }, {
                '$project': {
                    '_id': 0,
                    'asset_id': 1, 
                    'title': 1, 
                    'year': 1, 
                    'people': 1, 
                    'publication_metadata': 1, 
                    'cites': 1, 
                    'n_i': 1, 
                    's_i': 1, 
                    's_i_n_i': {
                        '$divide': [
                            '$s_i', '$n_i'
                        ]
                    }
                }
            }, {
                '$match': {
                    's_i': {
                        '$gte': 999
                    }
                }
            }
        ]

        _citations_classifier[0]['$match']['$and'][0]['asset_id']['$in'] = citation_ids
        _citations_classifier[0]['$match']['$and'][1]['$nor'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del _citations_classifier[0]['$match']['$and'][1]['$nor'][1]
        else:
            _citations_classifier[0]['$match']['$and'][1]['$nor'][1]['people.source_author_id']['$in'] = self.source_ids
        _citations_classifier[1]['$lookup']['from'] = self.ranking_collection
        _citations_classifier[4]['$project']['math_a']['$filter']['cond']['$and'][0]['$lte'][1] = self.max_year
        _citations_classifier[4]['$project']['math_a']['$filter']['cond']['$and'][1]['$gte'][1] = self.min_year
        _citations_classifier[7]['$match']['s_i']['$gte'] = self.if_min

        return _citations_classifier

    def _generate_report_doc(self, **kwargs):
        
        ## variables
        _univ_name = 'Universitatea de Vest din Timisoara'
        _fac_name = 'Facultatea de Matematica si Informatica'
        _report_title = 'Fişa de verificare a îndeplinirii standardelor minimale'
        _report_subtitle = 'Indicatori precizaţi în Anexa 1  la OM  nr. 6129 din 20.12.2016, publicată în Monitorul Oficial,  Partea I, nr. 123bis/15.02.2017'
        _numbers_of_papers = len(kwargs['papers_list'])
        _papers_start_row = 11
        _citations_start_row = 2
        ##

        _citations_dict = {}
        for c in kwargs['citations_list']:
            _citations_dict[c['asset_id']] = c
        
        _citations_ids = list(set([ c['asset_id'] for r in kwargs['papers_list'] for c in r['cites']]))
        _number_of_citations_ids = len(kwargs['citations_list'])

        wb = Workbook()
        ws1 = wb.active
        ws1.title = "Raport_Articole"
        ws2 = wb.create_sheet("Raport_Citari")
        
        # report header
        ws1.merge_cells(start_row=1, start_column=1, end_row=1, end_column=6)
        ws1['A1'] = _univ_name
        ws1.merge_cells(start_row=2, start_column=1, end_row=2, end_column=6)
        ws1['A2'] = _fac_name
        ws1.merge_cells(start_row=3, start_column=1, end_row=3, end_column=6)
        ws1['A3'] = kwargs['user_title'] + ' ' + kwargs['user_fullname']
        ws1['A3'].font = Font(bold=True)
        #############################################################################
        
        # report title
        ws1.merge_cells(start_row=5, start_column=1, end_row=5, end_column=6)
        ws1['A5'] = _report_title
        ws1['A5'].font = Font(bold=True, size=20)
        ws1['A5'].alignment = Alignment(horizontal="center")
        ws1.merge_cells(start_row=7, start_column=1, end_row=7, end_column=6)
        ws1['A7'] = _report_subtitle
        ws1['A7'].font = Font(bold=True)
        ws1['A7'].alignment = Alignment(horizontal="center")
        #############################################################################
        
        # articles table header
        ws1.column_dimensions['A'].width = 4
        ws1.column_dimensions['B'].width = 100
        ws1.column_dimensions['C'].width = 5
        ws1.column_dimensions['D'].width = 6
        ws1.column_dimensions['E'].width = 4
        ws1.column_dimensions['F'].width = 7
        ws1['A'+str(_papers_start_row)] = 'Nr. Crt.'
        ws1['B'+str(_papers_start_row)] = 'Articol, referinţa bibliografică ( Autori, titlul articol, revista, vol. (anul)'
        ws1['C'+str(_papers_start_row)] = 'Ult. 7 ani'
        ws1['D'+str(_papers_start_row)] = 's_i'
        ws1['E'+str(_papers_start_row)] = 'n_i'
        ws1['F'+str(_papers_start_row)] = 's_i/n_i'
        for i in ['A', 'B', 'C', 'D', 'E', 'F']:
            _b_style = 'medium'
            ws1[i+str(_papers_start_row)].font = Font(bold=True)
            ws1[i+str(_papers_start_row)].alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
            ws1[i+str(_papers_start_row)].border = Border(top=Side(border_style=_b_style),right=Side(border_style=_b_style),
                                    bottom=Side(border_style=_b_style),
                                    left=Side(border_style=_b_style))
            _b_style = 'thin'
            for row in range(0, _numbers_of_papers):
                ws1[i+str(row+_papers_start_row+1)].border = Border(top=Side(border_style=_b_style),right=Side(border_style=_b_style),
                                    bottom=Side(border_style=_b_style),
                                    left=Side(border_style=_b_style))
        ws1.merge_cells(start_row=9, start_column=1, end_row=9, end_column=6)
        # add report summary with S and S_recent but after you generate the table and have the sums computed
        ws1['A9'].font = Font(bold=True, size=12)
        ws1['A9'].alignment = Alignment(horizontal="center")
        #############################################################################

        # header for citations list / second sheet
        ws2.merge_cells(start_row=2, start_column=1, end_row=2, end_column=4)
        ws2['A2'].font = Font(bold=True, size=12)
        ws2['A2'].alignment = Alignment(horizontal="center", vertical="center", wrapText=True)

        ws2.column_dimensions['A'].width = 45
        ws2['A'+str(_citations_start_row+2)] = 'Articol citat' 
        col_a = ws2['A'+str(_citations_start_row+2)] 
        col_a.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_a.font = Font(bold=True)

        ws2.column_dimensions['B'].width = 4
        ws2['B'+str(_citations_start_row+2)]  = 'Nr. Crt.'
        col_c = ws2['B'+str(_citations_start_row+2)] 
        col_c.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_c.font = Font(bold=True)

        ws2.column_dimensions['C'].width = 70
        ws2['C'+str(_citations_start_row+2)] = 'Articol, referinţa bibliografică ( Autori, titlul articol, revista, vol. (anul)'
        col_d = ws2['C'+str(_citations_start_row+2)]
        col_d.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_d.font = Font(bold=True)

        ws2.column_dimensions['D'].width = 6
        ws2['D'+str(_citations_start_row+2)] = 's_i'
        col_m = ws2['D'+str(_citations_start_row+2)] 
        col_m.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_m.font = Font(bold=True)

        _b_style = 'medium'
        for c in ['A', 'B', 'C', 'D']:
            ws2[c+str(_citations_start_row+2)].border = Border(top=Side(border_style=_b_style),right=Side(border_style=_b_style),
                                    bottom=Side(border_style=_b_style),
                                    left=Side(border_style=_b_style))
        #############################################################################

        # articles and citations table content
        _papers_if_sum = 0
        _papers_if_sum_recent = 0
        _papers_crt = 1
        _citations_crt = 0
        _citations_total_crt = 0
        _pl = kwargs['papers_list']
        for row in range(0, _numbers_of_papers):
            ws1['A'+str(row+_papers_start_row+1)] = _papers_crt
            ws1['B'+str(row+_papers_start_row+1)] = '{}; "{}", {}, {}({}), ISSN: {}, DOI: {}'.format('; '.join([e['name'] for e in _pl[row]['people']]), 
                                                                        _pl[row]['title'], _pl[row]['publication_metadata']['others']['journal_name'], 
                                                                        _pl[row]['publication_metadata']['volume'], _pl[row]['year'], 
                                                                        _pl[row]['publication_metadata']['issn'], _pl[row]['publication_metadata']['doi'])
            ws1['B'+str(row+_papers_start_row+1)].alignment = Alignment(wrapText=True)
            if  _pl[row]['year'] >= (self.year - 7):
                ws1['C'+str(row+_papers_start_row+1)] = 'X'
                ws1['C'+str(row+_papers_start_row+1)].alignment = Alignment(horizontal="center")
                _papers_if_sum_recent += _pl[row]['s_i_n_i']
            ws1['D'+str(row+_papers_start_row+1)] = float('{:.3f}'.format(_pl[row]['s_i']))
            ws1['E'+str(row+_papers_start_row+1)] = _pl[row]['n_i']
            ws1['F'+str(row+_papers_start_row+1)] = float('{:.3f}'.format(_pl[row]['s_i_n_i']))
            _papers_if_sum += _pl[row]['s_i_n_i'] 
            _papers_crt +=1

            _citations_exists = [ True if c['asset_id'] in _citations_dict.keys() else False for c in _pl[row]['cites']]

            if any(_citations_exists):
                _b_style = 'thin'
                col_a = ws2['A'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)] 
                col_a.alignment = Alignment(wrapText=True, vertical="top")
                col_a.font = Font(size=9)
                col_a.border = Border(top=Side(border_style=_b_style),right=Side(border_style=_b_style),
                                    bottom=Side(border_style=_b_style),
                                    left=Side(border_style=_b_style))
                ws2['A'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)] = '{}; "{}", {}, {}({}), ISSN: {}, DOI: {}'.format('; '.join([e['name'] for e in _pl[row]['people']]), 
                                                                            _pl[row]['title'], _pl[row]['publication_metadata']['others']['journal_name'], 
                                                                            _pl[row]['publication_metadata']['volume'], _pl[row]['year'], 
                                                                            _pl[row]['publication_metadata']['issn'], _pl[row]['publication_metadata']['doi'])

                for c in _pl[row]['cites']:
                    if c['asset_id'] in _citations_dict.keys():
                        ws2['B'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)] = _citations_crt + 1
                        _e = _citations_dict[c['asset_id']]
                        ws2['C'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)] = '{}; "{}", {}, {}({}), ISSN: {}, DOI: {}'.format('; '.join([e['name'] for e in _e['people']]), 
                                                                                _e['title'], _e['publication_metadata']['others']['journal_name'], 
                                                                                _e['publication_metadata']['volume'], _e['year'], 
                                                                                _e['publication_metadata']['issn'], _e['publication_metadata']['doi'])
                        col_c = ws2['C'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)]
                        col_c.alignment = Alignment(wrapText=True, vertical="top")
                        col_c.font = Font(size=8)
                        ws2['D'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)] = float('{:.3f}'.format(_e['s_i']))
                        col_d = ws2['P'+str(_citations_total_crt+_citations_start_row+3+_citations_crt)]
                        for i in ['B', 'C', 'D']:
                            ws2[i+str(_citations_total_crt+_citations_start_row+3+_citations_crt)].border = Border(top=Side(border_style=_b_style),right=Side(border_style=_b_style),
                                            bottom=Side(border_style=_b_style),
                                            left=Side(border_style=_b_style))
                        _citations_crt +=1
                _citations_total_crt += 1
        #############################################################################

        # add papers report summary title with S and S_recent values
        ws1['A9'] = ' Articole:  Punctaj întrunit: S = {:.3f}, S_recent = {:.3f}'.format(_papers_if_sum, _papers_if_sum_recent)
        # add citations report summary title with S and S_recent values
        ws2['A2'] = 'Citari: in reviste cu s_i >= 0.5; Punctaj intrunit C = ' + str(_citations_crt)
        #############################################################################

        content = save_virtual_workbook(wb)
        return content