
from openpyxl.styles import Font, Color, Alignment, Border, Side, colors

uvt_academic_titles = {
    'profesor': {
        'title': 'prof. dr.'
    },
    'conferentiar': {
        'title': 'conf. dr.'
    },
    'lector/sl': {
        'title': 'lector dr.'
    },
    'asistent': {
        'title': 'asistent'
    }
}

# Functions useful for openpyxl

# Create borders within a range like A1:E1

def set_border_range(ws, cell_range, b_style):
    border = Border(left=Side(border_style=b_style),
                    right=Side(border_style=b_style),
                    top=Side(border_style=b_style),
                    bottom=Side(border_style=b_style))

    rows = [rows for rows in ws[cell_range]]
    flattened = [item for sublist in rows for item in sublist]
    [(setattr(cell, 'border', border)) for cell in flattened]

# Set alignment and font size within a range like A1:E1

def set_align_font(ws, cell_range, alignment, font_size, bold_font):
    font = Font(bold=bold_font, size=font_size)
    align = alignment

    rows = [rows for rows in ws[cell_range]]
    flattened = [item for sublist in rows for item in sublist]
    [(setattr(cell, 'font', font), setattr(cell, 'alignment', align)) for cell in flattened]
