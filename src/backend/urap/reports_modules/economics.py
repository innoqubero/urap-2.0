import json
from datetime import datetime
from flask import current_app as app, make_response
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from tempfile import NamedTemporaryFile
from openpyxl.styles import Font, Color, Alignment, Border, Side, colors

from backend.urap.reports_modules.helpers import set_border_range, set_align_font

from backend.urap.reports_modules.helpers import set_border_range, set_align_font, uvt_academic_titles

# bump

def init(_user, report_year, rtype):
    app.logger.debug('{}/{}'.format(report_year, rtype))
    _ufn = _user['firstname'].capitalize() + ' ' + _user['lastname'].capitalize()
    economics = EconomicsReport(_user['urap_id'],
                      source_ids=_user['identifiers']['wos']['ids'] + _user['identifiers']['scopus']['ids'],
                      year=report_year,
                      full_name=_ufn,
                      academic_title=_user['position'])
    response = make_response(economics.generate())
    response.headers.set('Content-Type', 'application/x-xlsx')
    response.headers.set(
        'Content-Disposition', 'attachment',
        filename='{}_economics_{}_report_{}_{}.xlsx'.format(report_year, rtype, _user['firstname'].lower(),
                                                             _user['lastname'].lower()))
    app.logger.debug(response)
    return response


class EconomicsReport(object):

    def __init__(self, person_id, source_ids=[], year=None, delta=5, ranking_collection='rankings_journals',
                 full_name='', academic_title=''):
        self.person_id = person_id
        self.source_ids = source_ids
        if year:
            self.year = year
        else:
            self.year = datetime.now().year
        self.delta = delta
        self.max_year = year - 1
        self.min_year = self.max_year - self.delta + 1
        self.ranking_collection = ranking_collection
        self.full_name = full_name
        try:
            self.academic_title = uvt_academic_titles[academic_title]['title']
        except:
            self.academic_title = ''

        self._multiplication_coefficients = {
            # Core Economics
            'ECONOMICS': 10,
            'BUSINESSC0MM4_FINANCE': 10,
            'BUSINESS': 10,
            'MANAGEMENT': 10,
            # Infoeconomics
            'COMPUTER_SCIENCEC0MM4_ARTIFICIAL_INTELLIGENCE': 8,
            'COMPUTER_SCIENCEC0MM4_INTERDISCIPLINARY_APPLICATIONS': 8,
            'COMPUTER_SCIENCEC0MM4_INFORMATION_SYSTEMS': 8,
            'COMPUTER_SCIENCEC0MM4_THEORY_4ND_METHODS': 8,
            'COMPUTER_SCIENCEC0MM4_SOFTWARE_ENGINEERING': 8,
            'COMPUTER_SCIENCEC0MM4_HARDWARE_4ND_ARCHITECTURE': 8,
            'OPERATIONS_RESEARCH_4ND_MANAGEMENT_SCIENCE': 8,
            'STATISTICS_4ND_PROBABILITY': 8,
            'COMPUTER_SCIENCEC0MM4_CYBERNETICS': 8,
        }

        self._citations_quartiles = {
            'Q1': 1,
            'Q2': 0.75,
            'Q3': 0.5,
            'Q4': 0.25
        }

        self._prestigious_publishers = ['Academic Press', 'Addison Wesley', 'Allen and Unwin', 'Blackwell',
                                        'Brookings Institution', 'Cambridge University Press',
                                        'Columbia University Press', 'Cornell University Press', 'CRC Press',
                                        'Duke University Press', 'Edward Elgar12Elsevier', 'Emerald Group',
                                        'Harper Collins', 'Harvard University Press', 'IEEE Computer Society',
                                        'Indiana University Press', 'IOS Press', 'John Wiley & Sons',
                                        'Johns Hopkins University Press', 'Kluwer Academic Press', 'Macmillan',
                                        'McGraw-Hill', 'MIT Press', 'New York University Press', 'North Holland',
                                        'Oxford University Press', 'Palgrave Macmillan', 'Chapman & Hall',
                                        'Pearson Education', 'Prentice Hall', 'Presses Universitaires de France',
                                        'Princeton University Press', 'Routledge', 'Rutgers University Press',
                                        'Sage', 'Springer', 'Stanford University Press', 'Taylor & Francis Group',
                                        'University of California Press', 'University of Chicago Press',
                                        'Wiley-Blackwell', 'Wiley', 'World Scientific', 'Yale University Press']

    def _get_multiplication_factor(self, wos_categories):
        max = 0

        for wos_cat in wos_categories:
            wos_cat = wos_cat.upper()

            try:
                factor = self._multiplication_coefficients[wos_cat]
            except KeyError:
                # If it's not in the rated categories, any other category has a factor of 6
                return 6
            if factor > max:
                max = factor

        return max

    # This method selects the best quartile from a list of dictionaries of quartiles
    # such as [{'year': 2017, 'rank': 'Q2', 'ais': 0.373}, {'year': 2019, 'rank': 'Q1', 'ais': 0.526}]
    def _get_best_quartile(self, quartiles_list):
        _best_quartile = 4

        app.logger.debug(quartiles_list)
        if quartiles_list is not None:
            for ranking in quartiles_list:
                val = int(ranking['rank'][-1])
                if val < _best_quartile:
                    _best_quartile = val

        return 'Q' + str(_best_quartile)

    def generate(self):
        return self._assets_classifier()

    def _assets_classifier(self):
        '''
            this method will use a mongodb aggregation filter, specific to the current domain
            and it will classify user's assets based on person_id and source_ids;
            the resulting dataset will also contain a list of citations, embedded into the document itself
            using root level key `cites` (this can change on mongodb filter)
        '''

        _acr = list(app.data.driver.db['assets'].aggregate(self._get_user_assets_classifier()))
        # get a unique list of asset_id from the mongo aggregration performed above
        _all_citation_ids = list(set([c['asset_id'] for r in _acr for c in r['cites']]))

        # get a classified list with the above citations
        _ccr = list(app.data.driver.db['assets'].aggregate(self._get_citations_classifier(_all_citation_ids)))

        # get a list containing books published by prestigious publishers
        _pbks = list(app.data.driver.db['assets'].aggregate(self._get_prestigious_books()))

        # get a list containing books the author has published
        _bks = list(app.data.driver.db['assets'].aggregate(self._get_books_classifier()))

        # get a list containing ISI proceedings the author has published
        _ipc = list(app.data.driver.db['assets'].aggregate(self._get_proceedings_classifier()))

        # get a list projects the author has been in
        _prj = list(app.data.driver.db['assets'].aggregate(self._get_projects_classifier()))

        return self._generate_report_doc(user_fullname=self.full_name,
                                         user_title=self.academic_title,
                                         papers_list=_acr,
                                         citations_list=_ccr,
                                         books_list=_bks,
                                         prestigious_books_list=_pbks,
                                         isi_proceedings=_ipc,
                                         projects_list=_prj)

    # This function sorts publications by their score satisfying
    # the requirement of having a maximum of 10 items from each
    # group of publications (articles, books, proceedings, etc)
    # input: dictionary of publications of the form:
    # _publications = {
    #     'articles': [],
    #     'books': [],
    #     'prestige_books': [],
    #     'chapters': [],
    #     'proceedings': []
    # }
    def _sort_publications(self, publications_dict):
        _sorted_dict = {}
        _max_items = 10

        for k,v in publications_dict.items():
            _sorted_dict[k] = sorted(v, key = lambda i: i['p_i'], reverse=True)

        for k,v in _sorted_dict.items():
            if _max_items >= 0:
                _sorted_dict[k] = v[:_max_items]
                _max_items -= len(v)
            else:
                _sorted_dict[k] = []

        return _sorted_dict

    def _get_user_assets_classifier(self):

        _user_asset_classifier = [{
            '$match': {
                '$and': [
                    {
                        '$or': [
                            {
                                'people.person_id': '9d4588fd-aac1-53b5-8a05-a491b9b8fa72'
                            }, {
                                'people.source_author_id': {
                                    '$in': [
                                        '4488726', '55604283100'
                                    ]
                                }
                            }
                        ]
                    }, {
                        'is_uvt': 1,
                        'asset_type': 'publication',
                        'year': {
                            '$lte': 2020
                        }
                    }
                ]
            }
        }, {
            '$lookup': {
                'from': 'assets',
                'let': {
                    'cites_list': {
                        '$cond': {
                            'if': {
                                '$isArray': '$publication_metadata.cites'
                            },
                            'then': '$publication_metadata.cites',
                            'else': []
                        }
                    }
                },
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$and': [
                                    {
                                        '$in': [
                                            {
                                                '$concat': [
                                                    '$index_source.source_name', ':', '$index_source.source_id'
                                                ]
                                            }, '$$cites_list'
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                ],
                'as': 'cites'
            }
        }, {
            '$lookup': {
                'from': 'rankings',
                'let': {
                    'pissn': '$publication_metadata.issn',
                    'eissn': '$publication_metadata.eissn'
                },
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$or': [
                                    {
                                        '$eq': [
                                            '$issn', '$$pissn'
                                        ]
                                    }, {
                                        '$eq': [
                                            '$issn', '$$eissn'
                                        ]
                                    }
                                ]
                            }
                        }
                    }, {
                        '$project': {
                            '_id': 0
                        }
                    }
                ],
                'as': 'ranking'
            }
        }, {
            '$replaceRoot': {
                'newRoot': {
                    '$mergeObjects': [
                        {
                            '$arrayElemAt': [
                                '$ranking', 0
                            ]
                        }, '$$ROOT'
                    ]
                }
            }
        }, {
            '$project': {
                'ranking': 0,
                'rankings.uefiscdi.RIS': 0,
                'rankings.uefiscdi.RIF': 0,
                'rankings.uefiscdi.zone': 0
            }
        }, {
            '$project': {
                'asset_id': 1,
                'year': 1,
                'title': 1,
                'people': 1,
                'publication_metadata': 1,
                'cites': 1,
                'max_ais': {
                    '$map': {
                        'input': {
                            '$filter': {
                                'input': '$rankings.uefiscdi.AIS',
                                'as': 'ais',
                                'cond': {
                                    '$or': [
                                        {
                                            '$eq': [
                                                '$$ais.year', '$year'
                                            ]
                                        }, {
                                            '$eq': [
                                                '$$ais.year', 2019
                                            ]
                                        }
                                    ]
                                }
                            }
                        },
                        'as': 'ais_entry',
                        'in': {
                            'if': '$$ais_entry.if'
                        }
                    }
                },
                'n': {
                    '$cond': {
                        'if': {
                            '$isArray': '$people'
                        },
                        'then': {
                            '$size': '$people'
                        },
                        'else': 0
                    }
                }
            }
        }, {
            '$addFields': {
                'cites.cited_asset_id': '$asset_id'
            }
        }, {
            '$project': {
                'asset_id': 1,
                'title': 1,
                'year': 1,
                'people': 1,
                'publication_metadata': 1,
                'cites': 1,
                'max_ais': {
                    '$max': '$max_ais.if'
                },
                'n': 1
            }
        }, {
            '$project': {
                'asset_id': 1,
                'title': 1,
                'year': 1,
                'people': 1,
                'publication_metadata': 1,
                'cites': 1,
                'n': 1,
                'max_ais': 1,
                'p_i': {
                    '$multiply': [
                        {
                            '$abs': {
                                '$subtract': [
                                    {
                                        '$multiply': [
                                            {
                                                '$subtract': [
                                                    '$n', 1
                                                ]
                                            }, 0.1
                                        ]
                                    }, 1
                                ]
                            }
                        }, '$max_ais'
                    ]
                }
            }
        }, {
            '$match': {
                'p_i': {
                    '$ne': None
                }
            }
        }]

        _user_asset_classifier[0]['$match']['$and'][0]['$or'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del _user_asset_classifier[0]['$match']['$and'][0]['$or'][1]
        else:
            _user_asset_classifier[0]['$match']['$and'][0]['$or'][1]['people.source_author_id']['$in'] = self.source_ids
        _user_asset_classifier[2]['$lookup']['from'] = self.ranking_collection

        return _user_asset_classifier

    def _get_citations_classifier(self, citation_ids):
        _citations_classifier =[{
            '$match': {
                'asset_id': {
                    '$in': [
                        '535665bc-0b5f-41b2-9cc1-096d8f9a23f6', 'e809ab01-61d5-453b-bfa4-dfbcaca68958', 'f5ec6bf9-ec85-46f7-8e17-04d2780d6ecb', '5ebff5d3-08e9-4352-81c6-2010ceff6931', '366f512e-ff87-4eee-9de8-4b811dbbc8e4', '1043e5d9-b376-40ee-af3e-e66e2f06db93', '97ef0a27-609e-497d-855c-4ca5a5aabd62', '00ce2a57-efbd-4839-a7aa-626a08e45c9e', '86ad6ac1-eddb-43b6-8bc0-bf4364314227', '644ef4fe-3798-4572-beaa-6d52624d1277', 'cc8474f8-2574-4d7c-a8ca-d4cfc2954e2f', '58bda818-8e00-43b6-9059-e478cbec7a87', 'e73ad094-8884-4a97-a0e5-ec8f1438787e', 'e205ecfe-2656-441d-b75c-c7227d4dd31c', 'f77186e7-921a-49fe-9a1d-27e10aa27a82', 'd3adac6f-03ee-435e-866a-6c6d8bd89c5b', '748ab7ad-6947-412a-8060-1ff2f0f5406f', '20d33b41-d4ef-42f4-84a7-43360f019c36', '64634350-648f-46a8-af8a-e8aaace51bfe', '031e170c-5293-41c7-8f07-e65ff49bf668', '2275f973-eb4c-4cce-b086-961ce3b73313', 'd720b9cd-e804-4a0b-b5e0-d4e7d1e06c43', '8b699b24-9d8b-411a-bd86-8ba35c9637da', '0159837d-4fd9-45f9-a844-a5128abcdac0', '95604a42-bec8-4866-be02-f87a1eb79d90', '8dd5d8b9-b819-4b06-8e35-2e790365627a', 'ed378137-3d5b-4ce1-ac40-6617b2288632', 'cce53beb-e3f1-4620-851f-bcbbee826992', '4a7ca0bb-2e22-487e-b611-1f260b2c26d2', 'd75fe013-82eb-4bda-b5db-3201abf15bad', '4051defc-2bdb-4e27-be69-bd5e5fc8d5a7', '2511013b-11cc-47a6-8179-dc152368b3b3', 'b7529283-56bd-4f13-bd5b-a74a2817ae8b', '4a2a7ed6-eeee-433c-8250-62a72ecc3839', 'd82a9109-1e42-4556-a8c2-732cf5a3c605', '4b87918f-d5db-4422-bee6-642b9b3fdbd5', '738fc686-bb63-4ceb-9864-f43be1fc983a', '89dc5be9-c098-4a66-9444-62f1c29474f4', '74b9c26d-3c98-4cea-a98e-3221e8c8b8ea', 'd8f137a3-3ce4-47c4-b571-75c3b2dad0cd', '60bf2c5f-6855-4b66-a54d-f55513503e36', '490e8a49-1806-4413-8f55-8aebb3d6b93e', 'a6503f49-f04b-4d9c-b3e0-166bf21270f0', '2d4428ac-47d2-4f8d-b569-be4a9439ad23', 'b8b9a0a3-f1c1-4bf3-b6dc-d4d298d4bfae', '0bbd8d42-a7ab-4d59-9a48-2249bcd3abe2', '8bbb2947-0c71-4ab6-b2a7-1495d6556c5a', 'aa80a487-a68e-4857-9972-51dcd184c5c7', '5535df54-17c2-4e7a-97a0-89e093934b03', '9107e29b-2c48-481b-ae43-32a747c1b7dc', 'ba6a197d-60c5-4142-997c-d9b77ec394ff', 'b944cfc8-a356-49c7-ab8b-3befa1bf49a0', 'b4c01046-c27c-4045-8251-7e1ffa26c894', 'b6c4d73a-0b81-4627-9e59-4d1ad0f29e96', 'a84225ab-5647-474f-b564-a2621b146fae'
                    ]
                }
            }
        }, {
            '$lookup': {
                'from': 'rankings',
                'let': {
                    'pissn': '$publication_metadata.issn',
                    'eissn': '$publication_metadata.eissn'
                },
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$or': [
                                    {
                                        '$eq': [
                                            '$issn', '$$pissn'
                                        ]
                                    }, {
                                        '$eq': [
                                            '$issn', '$$eissn'
                                        ]
                                    }
                                ]
                            }
                        }
                    }, {
                        '$project': {
                            '_id': 0
                        }
                    }
                ],
                'as': 'ranking'
            }
        }, {
            '$replaceRoot': {
                'newRoot': {
                    '$mergeObjects': [
                        {
                            '$arrayElemAt': [
                                '$ranking', 0
                            ]
                        }, '$$ROOT'
                    ]
                }
            }
        }, {
            '$project': {
                'ranking': 0,
                'rankings.uefiscdi.RIS': 0,
                'rankings.uefiscdi.RIF': 0,
                'rankings.uefiscdi.zone': 0
            }
        }, {
            '$project': {
                'asset_id': 1,
                'year': 1,
                'title': 1,
                'people': 1,
                'publication_metadata': 1,
                'cites': 1,
                'quartiles': {
                    '$filter': {
                        'input': '$rankings.jcr',
                        'as': 'quartile',
                        'cond': {
                            '$or': [
                                {
                                    '$eq': [
                                        '$$quartile.year', '$year'
                                    ]
                                }, {
                                    '$eq': [
                                        '$$quartile.year', 2019
                                    ]
                                }
                            ]
                        }
                    }
                },
                'n': {
                    '$cond': {
                        'if': {
                            '$isArray': '$people'
                        },
                        'then': {
                            '$size': '$people'
                        },
                        'else': 0
                    }
                }
            }
        }, {
            '$project': {
                'asset_id': 1,
                'title': 1,
                'year': 1,
                'people': 1,
                'publication_metadata': 1,
                'cites': 1,
                'max_ais': {
                    '$max': '$quartiles.ais'
                },
                'quartiles': 1,
                'n': 1
            }
        }, {
            '$project': {
                'asset_id': 1,
                'title': 1,
                'year': 1,
                'people': 1,
                'publication_metadata': 1,
                'cites': 1,
                'n': 1,
                'max_ais': 1,
                'quartiles': 1
            }
        }, {
            '$match': {
                'max_ais': {
                    '$ne': None
                }
            }
        }]

        _citations_classifier[0]['$match']['asset_id']['$in'] = citation_ids
        app.logger.debug(f"IDS: {citation_ids}")

        _citations_classifier[1]['$lookup']['from'] = self.ranking_collection

        return _citations_classifier

    def _get_books_classifier(self, **kwargs):

        _books_classifier = [{
            '$match': {
                '$or': [
                    {
                        'people.person_id': '9d4588fd-aac1-53b5-8a05-a491b9b8fa72'
                    }, {
                        'people.source_author_id': {
                            '$in': [
                                '4488726', '55604283100'
                            ]
                        }
                    }
                ]
            }
        }, {
            '$match': {
                'publication_metadata.type': {
                    '$eq': 'book'
                }
            }
        }, {
            '$project': {
                'title': 1,
                'publication_metadata': 1,
                'year': 1,
                'people': 1,
                'n': {
                    '$size': '$people'
                },
                'p_i': {
                    '$divide': [
                        0.2, {
                            '$size': '$people'
                        }
                    ]
                }
            }
        }]

        _books_classifier[0]['$match']['$or'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del _books_classifier[0]['$match']['$or'][1]
        else:
            _books_classifier[0]['$match']['$or'][1]['people.source_author_id']['$in'] = self.source_ids

        return _books_classifier
    
    def _get_proceedings_classifier(self, **kwargs):
        _proceedings_classifier = [{
            '$match': {
                '$and': [
                    {
                        '$or': [
                            {
                                'people.person_id': '9d4588fd-aac1-53b5-8a05-a491b9b8fa72'
                            }, {
                                'people.source_author_id': {
                                    '$in': [
                                        '4488726', '55604283100'
                                    ]
                                }
                            }
                        ]
                    }, {
                        'is_uvt': 1, 
                        'asset_type': 'publication', 
                        'publication_metadata.type': 'conference', 
                        'year': {
                            '$lte': 2020
                        }
                    }
                ]
            }
        }, {
            '$project': {
                'asset_id': 1, 
                'year': 1, 
                'title': 1, 
                'people': 1, 
                'publication_metadata': 1, 
                'n': {
                    '$cond': {
                        'if': {
                            '$isArray': '$people'
                        }, 
                        'then': {
                            '$size': '$people'
                        }, 
                        'else': 0
                    }
                }
            }
        }, {
            '$project': {
                'asset_id': 1, 
                'year': 1, 
                'title': 1, 
                'people': 1, 
                'publication_metadata': 1, 
                'n': {
                    '$cond': {
                        'if': {
                            '$isArray': '$people'
                        }, 
                        'then': {
                            '$size': '$people'
                        }, 
                        'else': 0
                    }
                }, 
                'p_i': {
                    '$divide': [
                        0.1, '$n'
                    ]
                }
            }
        }]
        
        _proceedings_classifier[0]['$match']['$and'][0]['$or'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del _proceedings_classifier[0]['$match']['$and'][0]['$or'][1]
        else:
            _proceedings_classifier[0]['$match']['$and'][0]['$or'][1]['people.source_author_id']['$in'] = self.source_ids

        return _proceedings_classifier

    def _get_prestigious_books(self, **kwargs):
        _prestigious_books_classifier = [{
            '$match': {
                '$or': [
                    {
                        'people.person_id': '9d4588fd-aac1-53b5-8a05-a491b9b8fa72'
                    }, {
                        'people.source_author_id': {
                            '$in': [
                                '4488726', '55604283100'
                            ]
                        }
                    }
                ]
            }
        }, {
            '$match': {
                '$and': [
                    {
                        'publication_metadata.type': {
                            '$eq': 'book'
                        }
                    }, {
                        'publication_metadata.others.publisher': {
                            '$in': []
                        }
                    }
                ]
            }
        }, {
            '$project': {
                'title': 1,
                'publication_metadata': 1,
                'year': 1,
                'people': 1,
                'n': {
                    '$size': '$people'
                },
                'p_i': {
                    '$divide': [
                        0.2, {
                            '$size': '$people'
                        }
                    ]
                }
            }
        }]

        _prestigious_books_classifier[0]['$match']['$or'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del _prestigious_books_classifier[0]['$match']['$or'][1]
        else:
            _prestigious_books_classifier[0]['$match']['$or'][1]['people.source_author_id']['$in'] = self.source_ids

        _prestigious_books_classifier[1]['$match']['$and'][1]['publication_metadata.others.publisher']['$in'] = self._prestigious_publishers

        return _prestigious_books_classifier

    def _get_projects_classifier(self, **kwargs):
        _projects_classifier = [{
            '$match': {
                '$or': [
                    {
                        'people.person_id': '9d4588fd-aac1-53b5-8a05-a491b9b8fa72'
                    }, {
                        'people.source_author_id': {
                            '$in': [
                                '4488726', '55604283100'
                            ]
                        }
                    }
                ]
            }
        }, {
            '$match': {
                'asset_type': {
                    '$eq': 'project'
                }
            }
        }, {
            '$project': {
                'title': 1, 
                'project_metadata': 1, 
                'year': 1, 
                'people': 1, 
                'role': {
                    '$map': {
                        'input': {
                            '$filter': {
                                'input': '$people', 
                                'as': 'ppl', 
                                'cond': {
                                    '$eq': [
                                        '$$ppl.person_id', '9d4588fd-aac1-53b5-8a05-a491b9b8fa72'
                                    ]
                                }
                            }
                        }, 
                        'as': 'person', 
                        'in': {
                            'role': '$$person.role'
                        }
                    }
                }
            }
        }, {
            '$project': {
                'title': 1, 
                'project_metadata': 1, 
                'year': 1, 
                'people': 1, 
                'role': {
                    '$arrayElemAt': [
                        '$role.role', 0
                    ]
                }
            }
        }]

        _projects_classifier[0]['$match']['$or'][0]['people.person_id'] = self.person_id
        if len(self.source_ids) == 0:
            del _projects_classifier[0]['$match']['$or'][1]
        else:
            _projects_classifier[0]['$match']['$or'][1]['people.source_author_id']['$in'] = self.source_ids

        _projects_classifier[2]['$project']['role']['$map']['input']['$filter']['cond']['$eq'][1] = self.person_id

        return _projects_classifier

    def _generate_report_doc(self, **kwargs):
        ## variables
        _univ_name = 'Universitatea de Vest din Timisoara'
        _fac_name = 'Facultatea de Economie si Administrarea Afacerilor'
        _report_title = 'Fişa de verificare a îndeplinirii standardelor minimale'
        _report_subtitle = 'Indicatori precizaţi în Anexa 27  la OM  nr. 3482 din 20.12.2016, publicată în Monitorul Oficial,  Partea I, nr. 123bis/15.02.2017'
        _papers_start_row = 11
        _citations_start_row = 2
        # !!! Total publication_crt needs to be <= 10 !!!
        _total_publications_crt = 0
        _publications = {
            'articles': [],
            'books': [],
            'prestige_books': [],
            'chapters': [],
            'proceedings': []
        }

        _pl = kwargs['papers_list']

        # Multiply P_i by the factor based on wos_category
        for idx, paper in enumerate(_pl):
            _pl[idx]['p_i'] = float('{:.3f}'.format(_pl[idx]['p_i'])) * \
                              self._get_multiplication_factor(_pl[idx]['publication_metadata']['others']['wos_category']) \
                if _pl[idx]['p_i'] else None

        _publications['articles'] = _pl
        _publications['books'] = kwargs['books_list']
        _publications['proceedings'] = kwargs['isi_proceedings']
        _publications['prestige_books'] = kwargs['prestigious_books_list']

        _publications = self._sort_publications(_publications)

        _numbers_of_papers = len(_publications['articles'])
        _number_of_books = len(_publications['books'])
        _number_of_proceedings = len(_publications['proceedings'])
        _number_of_prestigious_books = len(_publications['prestige_books'])

        _citations_dict = {}
        _papers_citations_mapping = {}

        # Sort and extract best 10 citations
        _cites = sorted(kwargs['citations_list'], key = lambda i: i['max_ais'], reverse=True)[:10]

        for c in _cites:
            _citations_dict[c['asset_id']] = c

        wb = Workbook()
        ws1 = wb.active
        ws1.title = "Raport_Articole"
        ws2 = wb.create_sheet("Raport_Citari")
        ws3 = wb.create_sheet("Carti")
        ws4 = wb.create_sheet("Publicatii_Edituri_Prestigiu")
        ws5 = wb.create_sheet("Articole_ISI_Proceedings")
        ws6 = wb.create_sheet("Proiecte")
        ws7 = wb.create_sheet("Conditii_Minimale")

        # report header
        ws1.merge_cells(start_row=1, start_column=1, end_row=1, end_column=6)
        ws1['A1'] = _univ_name
        ws1.merge_cells(start_row=2, start_column=1, end_row=2, end_column=6)
        ws1['A2'] = _fac_name
        ws1.merge_cells(start_row=3, start_column=1, end_row=3, end_column=6)
        ws1['A3'] = kwargs['user_title'] + ' ' + kwargs['user_fullname']
        ws1['A3'].font = Font(bold=True)
        #############################################################################

        # report title
        ws1.merge_cells(start_row=5, start_column=1, end_row=5, end_column=6)
        ws1['A5'] = _report_title
        ws1['A5'].font = Font(bold=True, size=20)
        ws1['A5'].alignment = Alignment(horizontal="center")
        ws1.merge_cells(start_row=7, start_column=1, end_row=7, end_column=6)
        ws1['A7'] = _report_subtitle
        ws1['A7'].font = Font(bold=True)
        ws1['A7'].alignment = Alignment(horizontal="center")
        #############################################################################

        # articles table header
        ws1.column_dimensions['A'].width = 4
        ws1.column_dimensions['B'].width = 100
        ws1.column_dimensions['C'].width = 5
        ws1.column_dimensions['D'].width = 6
        ws1.column_dimensions['E'].width = 4
        ws1.column_dimensions['F'].width = 7
        ws1['A' + str(_papers_start_row)] = 'Nr. Crt.'
        ws1['B' + str(
            _papers_start_row)] = 'Articol, referinţa bibliografică ( Autori, titlul articol, revista, vol. (anul)'
        ws1['C' + str(_papers_start_row)] = 'M (coeficient de multiplicare)'
        ws1['D' + str(_papers_start_row)] = 'N (număr de autori)'
        ws1['E' + str(_papers_start_row)] = 'AIS'
        ws1['F' + str(_papers_start_row)] = 'Punctaj Final'

        set_border_range(ws1, f'A{str(_papers_start_row)}:F{str(_papers_start_row)}', 'medium')
        set_align_font(ws1, f'A{str(_papers_start_row)}:F{str(_papers_start_row)}',
                            Alignment(horizontal="center", vertical="center", wrapText=True), 11, True)

        for i in range(_numbers_of_papers):
            set_border_range(ws1, f'A{str(i + _papers_start_row + 1)}:F{str(i + _papers_start_row + 1)}', 'thin')

        ws1.merge_cells(start_row=9, start_column=1, end_row=9, end_column=6)
        ws1['A9'].font = Font(bold=True, size=12)
        ws1['A9'].alignment = Alignment(horizontal="center")
        #############################################################################

        # header for citations list / second sheet
        ws2.merge_cells(start_row=2, start_column=1, end_row=2, end_column=6)
        ws2['A2'].font = Font(bold=True, size=12)
        ws2['A2'].alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        ws2.row_dimensions[4].height = 25

        ws2.column_dimensions['A'].width = 45
        ws2['A' + str(_citations_start_row + 2)] = 'Articol citat'
        col_a = ws2['A' + str(_citations_start_row + 2)]
        col_a.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_a.font = Font(bold=True)

        ws2.column_dimensions['B'].width = 4
        ws2['B' + str(_citations_start_row + 2)] = 'Nr. Crt.'
        col_c = ws2['B' + str(_citations_start_row + 2)]
        col_c.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_c.font = Font(bold=True)

        ws2.column_dimensions['C'].width = 70
        ws2['C' + str(
            _citations_start_row + 2)] = 'Articol, referinţa bibliografică ( Autori, titlul articol, revista, vol. (anul)'
        col_d = ws2['C' + str(_citations_start_row + 2)]
        col_d.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_d.font = Font(bold=True)

        ws2.column_dimensions['D'].width = 6
        ws2['D' + str(_citations_start_row + 2)] = 'AIS'
        col_m = ws2['D' + str(_citations_start_row + 2)]
        col_m.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_m.font = Font(bold=True)

        ws2.column_dimensions['E'].width = 9
        ws2['E' + str(_citations_start_row + 2)] = 'Cuartila'
        col_m = ws2['E' + str(_citations_start_row + 2)]
        col_m.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_m.font = Font(bold=True)

        ws2.column_dimensions['F'].wiFth = 8
        ws2['F' + str(_citations_start_row + 2)] = 'Punctaj final'
        col_m = ws2['F' + str(_citations_start_row + 2)]
        col_m.alignment = Alignment(horizontal="center", vertical="center", wrapText=True)
        col_m.font = Font(bold=True)

        _b_style = 'medium'
        for c in ['A', 'B', 'C', 'D', 'E', 'F']:
            ws2[c + str(_citations_start_row + 2)].border = Border(top=Side(border_style=_b_style),
                                                                   right=Side(border_style=_b_style),
                                                                   bottom=Side(border_style=_b_style),
                                                                   left=Side(border_style=_b_style))
        #############################################################################

        # articles and citations table content
        _papers_if_sum = 0
        _papers_if_sum_recent = 0
        _papers_crt = 1
        _citations_crt = 0
        _citations_total_crt = 0
        _citations_if_sum = 0

        for row in range(0, _numbers_of_papers):
            _total_publications_crt += 1
            ws1['A' + str(row + _papers_start_row + 1)] = _total_publications_crt
            ws1['B' + str(row + _papers_start_row + 1)] = '{}; "{}", {}, {}({}), ISSN: {}, DOI: {}'.format(
                '; '.join([e['name'] for e in _publications['articles'][row]['people']]),
                _publications['articles'][row]['title'],
                _publications['articles'][row]['publication_metadata']['others']['journal_name'],
                _publications['articles'][row]['publication_metadata']['volume'],
                _publications['articles'][row]['year'],
                _publications['articles'][row]['publication_metadata']['issn'],
                _publications['articles'][row]['publication_metadata']['doi'])

            ws1['B' + str(row + _papers_start_row + 1)].alignment = Alignment(wrapText=True)
            ws1['C' + str(row + _papers_start_row + 1)] = self._get_multiplication_factor(_publications['articles'][row]['publication_metadata']['others']['wos_category'])
            ws1['C' + str(row + _papers_start_row + 1)].alignment = Alignment(horizontal="center")
            ws1['D' + str(row + _papers_start_row + 1)] = _publications['articles'][row]['n']
            ws1['E' + str(row + _papers_start_row + 1)] = '{:.3f}'.format(_publications['articles'][row]['max_ais']) if _publications['articles'][row]['max_ais'] else None
            ws1['F' + str(row + _papers_start_row + 1)] = _publications['articles'][row]['p_i']
            _papers_if_sum += _publications['articles'][row]['p_i'] if _publications['articles'][row]['p_i'] else 0
            _papers_crt += 1


        # Citations

        _prev_asset_id = None

        for paper in _pl:
            for cites in paper['cites']:
                if cites['asset_id'] in _citations_dict.keys():
                    _b_style = 'thin'
                    ws2.row_dimensions[_citations_start_row + _citations_crt].height = 25
                    col_a.alignment = Alignment(wrapText=True, vertical="top")
                    col_a.font = Font(size=9)
                    col_a.border = Border(top=Side(border_style=_b_style),
                                          right=Side(border_style=_b_style),
                                          bottom=Side(border_style=_b_style),
                                          left=Side(border_style=_b_style))

                    if not paper['asset_id'] == _prev_asset_id:
                        app.logger.debug(f"{paper['asset_id']} != {_pl[idx - 1]['asset_id']}")
                        ws2['A' + str(
                            _citations_start_row + 3 + _citations_crt)] = '{}; "{}", {}, {}({}), ISSN: {}, DOI: {}'.format(
                            '; '.join([e['name'] for e in paper['people']]),
                            paper['title'], paper['publication_metadata']['others']['journal_name'],
                            paper['publication_metadata']['volume'], paper['year'],
                            paper['publication_metadata']['issn'], paper['publication_metadata']['doi'])

                    _prev_asset_id = paper['asset_id']

                    ws2['B' + str(_citations_start_row + 3 + _citations_crt)] = _citations_crt + 1

                    _e = _citations_dict[cites['asset_id']]
                    _best_quartile = self._get_best_quartile(_e['quartiles'])

                    ws2['C' + str(
                    _citations_start_row + 3 + _citations_crt)] = '{}; "{}", {}, {}({}), ISSN: {}, DOI: {}'.format(
                    '; '.join([e['name'] for e in _e['people']]),
                    _e['title'], _e['publication_metadata']['others']['journal_name'],
                    _e['publication_metadata']['volume'], _e['year'],
                    _e['publication_metadata']['issn'], _e['publication_metadata']['doi'])
                    col_c = ws2['C' + str(_citations_start_row + 3 + _citations_crt)]
                    col_c.alignment = Alignment(wrapText=True, vertical="top")
                    col_c.font = Font(size=8)
                    ws2['D' + str(_citations_start_row + 3 + _citations_crt)] = float(
                    '{:.3f}'.format(_e['max_ais']))
                    ws2['D' + str(_citations_start_row + 3 + _citations_crt)].alignment = Alignment(horizontal='center')
                    ws2['E' + str(_citations_start_row + 3 + _citations_crt)] = _best_quartile
                    ws2['E' + str(_citations_start_row + 3 + _citations_crt)].alignment = Alignment(horizontal='center')
                    ws2['F' + str(_citations_start_row + 3 + _citations_crt)] = self._citations_quartiles[_best_quartile]
                    ws2['F' + str(_citations_start_row + 3 + _citations_crt)].alignment = Alignment(horizontal='center')
                    col_d = ws2['P' + str(_citations_start_row + 3 + _citations_crt)]
                    for i in ['B', 'C', 'D', 'E', 'F']:
                        ws2[i + str(_citations_start_row + 3 + _citations_crt)].border = Border(
                        top=Side(border_style=_b_style),
                        right=Side(border_style=_b_style),
                        bottom=Side(border_style=_b_style),
                        left=Side(border_style=_b_style))

                    _citations_if_sum += self._citations_quartiles[_best_quartile]
                    _citations_crt += 1
        #############################################################################

        # add papers report summary title with S and S_recent values
        ws1['A9'] = ' Total punctaj articole publicate in reviste indexate ISI  cu AIS nenul = {:.3f}'.format(_papers_if_sum)

        # add citations report summary title with S and S_recent values
        ws2['A2'] = 'Citări în reviste cotate ISI cu scor de influenţă absolut (Article Influence Score - AIS) nenul (maxim 10 citări) Punctaj intrunit C = ' + \
                    str('{:.3f}'.format(_citations_if_sum))
        #############################################################################

        # books sheet

        ws3['A2'] = 'Carte publicată la o editură națională sau internațională, altele decât în Anexa 1'
        ws3.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
        ws3['A2'].font = Font(bold=True, size=12)
        ws3['A2'].alignment = Alignment(horizontal='center')

        ws3.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)
        ws3['A4'].alignment = Alignment(horizontal='center')

        ws3.column_dimensions['A'].width = 10
        ws3.column_dimensions['B'].width = 100
        ws3.column_dimensions['C'].width = 8
        ws3.row_dimensions[6].height = 50

        ws3['A6'] = 'Nr. Crt'
        ws3['B6'] = 'Publicaţie'
        ws3['C6'] = 'N\n(numar\nde\nautori)'
        ws3['D6'] = 'Pi'
        ws3['E6'] = 'Punctaj\nfinal'

        set_border_range(ws3, 'A6:E6', 'medium')
        set_align_font(ws3, 'A6:E6', Alignment(horizontal='center'), 11, True)
        # books entries
        
        ws3.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
        ws3['A2'].font = Font(bold=True, size=12)
        ws3['A2'].alignment = Alignment(horizontal='center')

        ws3.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)
        ws3['A4'].alignment = Alignment(horizontal='center')
        ws3['A4'].font = Font(bold=True)

        ws3.column_dimensions['A'].width = 10
        ws3.column_dimensions['B'].width = 100
        ws3.column_dimensions['C'].width = 8
        ws3.row_dimensions[6].height = 50

        ws3['A6'] = 'Nr. Crt'
        ws3['B6'] = 'Publicaţie'
        ws3['C6'] = 'N\n(numar\nde\nautori)'
        ws3['D6'] = 'Pi'
        ws3['E6'] = 'Punctaj\nfinal'

        _books_if_sum = 0
        _books_crt = 1
        _books_start_row = 6

        for row in range(0, _number_of_books):
            _total_publications_crt += 1
            ws3['A' + str(row + _books_start_row + 1)] = _total_publications_crt
            ws3['B' + str(row + _books_start_row + 1)] = '{}; "{}", ({}), ISBN: {}, DOI: {}'.format(
                '; '.join([e['name'] for e in _publications['books'][row]['people']]),
                _publications['books'][row]['title'], _publications['books'][row]['year'], _publications['books'][row]['publication_metadata']['isbn'],
                _publications['books'][row]['publication_metadata']['doi'])

            ws3['B' + str(row + _books_start_row + 1)].alignment = Alignment(wrapText=True)
            ws3['C' + str(row + _books_start_row + 1)] = _publications['books'][row]['n']
            ws3['C' + str(row + _books_start_row + 1)].alignment = Alignment(horizontal='center')
            ws3['D' + str(row + _books_start_row + 1)] = '0,200'
            ws3['D' + str(row + _books_start_row + 1)].alignment = Alignment(horizontal='center')
            ws3['E' + str(row + _books_start_row + 1)] = '{:.3f}'.format(_publications['books'][row]['p_i']) if _publications['books'][row]['p_i'] else None
            ws3['E' + str(row + _books_start_row + 1)].alignment = Alignment(horizontal='center')

            for col in ['A', 'B', 'C', 'D', 'E']:
                ws3[col + str(row + _books_start_row + 1)].border = Border(top=Side(border_style='thin'),
                                                                           right=Side(border_style='thin'),
                                                                           bottom=Side(border_style='thin'),
                                                                           left=Side(border_style='thin'))

                ws3[col + '6'].border = Border(top=Side(border_style='medium'),
                                               right=Side(border_style='medium'),
                                               bottom=Side(border_style='medium'),
                                               left=Side(border_style='medium'))

            _books_if_sum += _publications['books'][row]['p_i'] if _publications['books'][row]['p_i'] else 0
            _books_crt += 1

        ws3['A4'] = 'Punctaj cărţi publicate la o editură năţională sau internaţională = {:.3f}'.format(_books_if_sum)

        #############################################################################

        # Edituri prestigiu

        _prestigious_books_if_sum = 0
        _prestigious_books_crt = 1
        _books_start_row = 6

        ws4.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
        ws4['A2'].font = Font(bold=True, size=12)
        ws4['A2'].alignment = Alignment(horizontal='center')

        ws4.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)
        ws4['A4'].alignment = Alignment(horizontal='center')
        ws4['A4'].font = Font(bold=True)

        ws4.column_dimensions['A'].width = 10
        ws4.column_dimensions['B'].width = 100
        ws4.column_dimensions['C'].width = 8
        ws4.row_dimensions[6].height = 50

        ws4['A6'] = 'Nr. Crt'
        ws4['B6'] = 'Publicaţie'
        ws4['C6'] = 'N\n(numar\nde\nautori)'
        ws4['D6'] = 'Pi'
        ws4['E6'] = 'Punctaj\nfinal'
        ws4['A2'] = 'Capitol în carte publicată la o editură internațională din Anexa 1 (punctaj individual 0,25/N)'

        set_border_range(ws4, 'A6:E6', 'medium')
        set_align_font(ws4, 'A6:E6', Alignment(horizontal='center'), 11, True)

        for row in range(0, _number_of_prestigious_books):
            ws4['A' + str(row + _books_start_row + 1)] = _books_crt
            ws4['B' + str(row + _books_start_row + 1)] = '{}; "{}", ({}), ISBN: {}, DOI: {}'.format(
                '; '.join([e['name'] for e in _publications['prestige_books'][row]['people']]),
                _publications['prestige_books'][row]['title'],
                _publications['prestige_books'][row]['year'],
                _publications['prestige_books'][row]['publication_metadata']['isbn'],
                _publications['prestige_books'][row]['publication_metadata']['doi'])

            ws4['B' + str(row + _books_start_row + 1)].alignment = Alignment(wrapText=True)
            ws4['C' + str(row + _books_start_row + 1)] = _publications['prestige_books'][row]['n']
            ws4['C' + str(row + _books_start_row + 1)].alignment = Alignment(horizontal='center')
            ws4['D' + str(row + _books_start_row + 1)] = '0,200'
            ws4['D' + str(row + _books_start_row + 1)].alignment = Alignment(horizontal='center')
            ws4['E' + str(row + _books_start_row + 1)] = '{:.3f}'.format(_publications['prestige_books'][row]['p_i']) if _publications['prestige_books'][row]['p_i'] else None
            ws4['E' + str(row + _books_start_row + 1)].alignment = Alignment(horizontal='center')

            set_border_range(ws4, f'A{str(row + _books_start_row + 1)}:E{str(row + _books_start_row + 1)}', 'thin')

            _prestigious_books_if_sum += _publications['prestige_books'][row]['p_i'] if _publications['prestige_books'][row]['p_i'] else 0
            _prestigious_books_crt += 1

        ws4['A4'] = 'Punctaj cărţi publicate la o editură internaţională din Anexa 1 = {:.3f}'.format(_prestigious_books_if_sum)

        #############################################################################

        # ISI proceedings sheet

        ws5['A2'] = 'Articol in volume ISI Proceedings (punctaj individual 0,1/N)'

        ws5.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
        ws5['A2'].font = Font(bold=True, size=12)
        ws5['A2'].alignment = Alignment(horizontal='center')

        ws5.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)
        ws5['A4'].alignment = Alignment(horizontal='center')
        ws5['A4'].font = Font(bold=True)

        ws5.column_dimensions['A'].width = 10
        ws5.column_dimensions['B'].width = 100
        ws5.column_dimensions['C'].width = 8
        ws5.row_dimensions[6].height = 50

        ws5['A6'] = 'Nr. Crt'
        ws5['B6'] = 'Publicaţie'
        ws5['C6'] = 'N\n(numar\nde\nautori)'
        ws5['D6'] = 'Pi'
        ws5['E6'] = 'Punctaj\nfinal'

        set_border_range(ws5, f'A6:E6', 'medium')
        set_align_font(ws5, 'A6:E6', Alignment(horizontal='center'), 11, True)

        ws5.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
        ws5['A2'].font = Font(bold=True, size=12)
        ws5['A2'].alignment = Alignment(horizontal='center')

        ws5.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)
        ws5['A4'].alignment = Alignment(horizontal='center')

        ws5.column_dimensions['A'].width = 10
        ws5.column_dimensions['B'].width = 100
        ws5.column_dimensions['C'].width = 8
        ws5.row_dimensions[6].height = 50

        ws5['A6'] = 'Nr. Crt'
        ws5['B6'] = 'Publicaţie'
        ws5['C6'] = 'N\n(numar\nde\nautori)'
        ws5['D6'] = 'Pi'
        ws5['E6'] = 'Punctaj\nfinal'
        
        _proceedings_if_sum = 0
        _proceedings_crt = 1
        _proceedings_start_row = 6

        for row in range(0, _number_of_proceedings):
            _total_publications_crt += 1
            ws5['A' + str(row + _proceedings_start_row + 1)] = _total_publications_crt
            ws5['B' + str(row + _proceedings_start_row + 1)] = '{}; "{}", {}, {}, {}, ISBN: {}, ISSN: {}, DOI: {}'.format(
                '; '.join([e['name'] for e in _publications['proceedings'][row]['people']]),
                _publications['proceedings'][row]['title'],
                _publications['proceedings'][row]['publication_metadata']['others']['journal_name'],
                _publications['proceedings'][row]['publication_metadata']['volume'],
                _publications['proceedings'][row]['year'],
                _publications['proceedings'][row]['publication_metadata']['isbn'],
                _publications['proceedings'][row]['publication_metadata']['issn'],
                _publications['proceedings'][row]['publication_metadata']['doi'])

            ws5['B' + str(row + _proceedings_start_row + 1)].alignment = Alignment(wrapText=True)
            ws5['C' + str(row + _proceedings_start_row + 1)] = _publications['proceedings'][row]['n']
            ws5['C' + str(row + _proceedings_start_row + 1)].alignment = Alignment(horizontal='center')
            ws5['D' + str(row + _proceedings_start_row + 1)] = '0,100'
            ws5['D' + str(row + _proceedings_start_row + 1)].alignment = Alignment(horizontal='center')
            ws5['E' + str(row + _proceedings_start_row + 1)] = '{:.3f}'.format(_publications['proceedings'][row]['p_i'])
            ws5['E' + str(row + _proceedings_start_row + 1)].alignment = Alignment(horizontal='center')

            _proceedings_if_sum += _publications['proceedings'][row]['p_i'] if _publications['proceedings'][row]['p_i'] else 0
            _proceedings_crt += 1

            set_border_range(ws5, f'A{str(row + _proceedings_start_row + 1)}:E{str(row + _proceedings_start_row + 1)}', 'thin')

        ws5['A4'] = 'Punctaj articol publicate in volume ISI Proceedings = {:.3f}'.format(_proceedings_if_sum)

        #############################################################################

        # projects sheet

        ws6['A2'] = 'Proiecte de cercetare câștigate în competiții naționale sau internaționale.'
        ws6.merge_cells(start_row=2, start_column=1, end_row=2, end_column=5)
        ws6['A2'].font = Font(bold=True, size=12)
        ws6['A2'].alignment = Alignment(horizontal='center')

        ws6.merge_cells(start_row=4, start_column=1, end_row=4, end_column=5)
        ws6['A4'].alignment = Alignment(horizontal='center')
        ws6['A4'].font = Font(bold=True)

        ws6.column_dimensions['A'].width = 10
        ws6.column_dimensions['B'].width = 100
        ws6.column_dimensions['C'].width = 8
        ws6.row_dimensions[6].height = 50

        ws6['A6'] = 'Nr. Crt'
        ws6['B6'] = 'Proiect'
        ws6['D6'] = 'Perioada proiectului'
        ws6['C6'] = 'Funcţia'

        set_border_range(ws6, 'A6:D6', 'medium')
        set_align_font(ws6, 'A6:D6', Alignment(horizontal='center'), 11, True)

        # projects entries

        _projects_crt = 1
        _pjl = kwargs['projects_list']
        _number_of_projects = len(_pjl)
        _projects_start_row = 6


        for row in range(0, _number_of_projects):
            ws6['A' + str(row + _projects_start_row + 1)] = _projects_crt
            ws6['B' + str(row + _projects_start_row + 1)] = '"{}", ("{}"), {}, {}'.format(
                _pjl[row]['title'], _pjl[row]['project_metadata']['acronym'], _pjl[row]['year'],
                _pjl[row]['project_metadata']['contractno'])

            ws6['B' + str(row + _projects_start_row + 1)].alignment = Alignment(wrapText=True)
            ws6['C' + str(row + _projects_start_row + 1)] = _pjl[row]['role']
            ws6['C' + str(row + _projects_start_row + 1)].alignment = Alignment(horizontal='center')
            ws6['D' + str(row + _projects_start_row + 1)] = '{} - {}'.format(_pjl[row]['project_metadata']['date_start'],
                                                                             _pjl[row]['project_metadata']['date_end'])

            _projects_crt += 1

        ws6['A4'] = 'Total participari in proiecte = {}'.format(_projects_crt - 1)

        for row in range(_projects_start_row + 1, _projects_start_row + _projects_crt):
            set_border_range(ws6, f'A{row}:D{row}', 'thin')

        #############################################################################

        # minimal habilitation criteria sheet

        ws7.column_dimensions['A'].width = 40
        ws7.column_dimensions['B'].width = 40
        ws7.column_dimensions['C'].width = 40
        ws7.row_dimensions[4].height = 30
        ws7.row_dimensions[9].height = 35
        ws7.row_dimensions[10].height = 110

        ws7.merge_cells(start_row=2, start_column=1, end_row=2, end_column=3)
        ws7['A2'] = 'Îndeplinirea condiţiilor minimale pentru abilitare:'
        ws7['A2'].font = Font(bold=True, size=12)
        ws7['A2'].alignment = Alignment(horizontal='center')

        for i in range(5, 11):
            ws7['A' + str(i)].font = Font(bold=True)

        ws7['B4'] = "Punctaj minim pentru \n abilitare"
        ws7['B4'].font = Font(bold=True)
        ws7['C4'] = "Punctaj obtinut de \n candidat"
        ws7['C4'].font = Font(bold=True)

        ws7['A5'] = 'Punctaj publicaţii (P)'
        ws7['A6'] = 'Punctaj citări (C)'
        ws7['A7'] = 'Punctaj final (S)'
        ws7['A8'] = 'Număr articole ISI cu AIS nenul'
        ws7['A9'] = 'Număr articole ISI publicate în Core Economic & Infoeconomics'
        ws7['A9'].alignment = Alignment(wrap_text=True)
        ws7['A10'] = 'Pentru profesor universitar, cercetător ştiinţific I şi abilitare, trebuie indeplinită una din următoarele condiții:'
        ws7['A10'].alignment = Alignment(wrap_text=True)

        ws7['B5'] = '2'
        ws7['B6'] = '1,2'
        ws7['B7'] = '4'
        ws7['B8'] = '4'
        ws7['B9'] = '2'
        ws7['B10'] = 'a) din cele maxim 10 articole, candidatul trebuie să fie autor sau coautor a cel puţin două articole publicate în reviste cotate ISI cu scor absolut de influenţă (AIS) mai mare decât 0,15;\nb) să fi câștigat în competiții naționale sau internaționale cel putin 2 proiecte/granturi de cercetare, cu excepția proiectelor finanțate prin programe operaționale de tip POSDRU, POS-CEE sau similare, dintre care unul în calitate de director de proiect sau responsabil partener'
        ws7['B10'].alignment = Alignment(wrap_text=True)

        ws7['C5'] = _papers_if_sum
        ws7['C6'] = _citations_if_sum
        ws7['C7'] = _papers_if_sum + _citations_if_sum
        ws7['C8'] = _papers_crt - 1

        _infoeconomics_and_core_economics_papers_crt = 0

        for pub in _publications['articles']:
            if self._get_multiplication_factor(pub['publication_metadata']['others']['wos_category']) >= 8:
                _infoeconomics_and_core_economics_papers_crt += 1

        ws7['C9'] = _infoeconomics_and_core_economics_papers_crt

        for i in range(4, 11):
            ws7['B' + str(i)].alignment = Alignment(horizontal='center')
            ws7['C' + str(i)].alignment = Alignment(horizontal='center')

        ws7.merge_cells(start_row=10, start_column=2, end_row=10, end_column=3)

        set_border_range(ws7, f'B4:C4', 'medium')

        for row in range(5, 11):
            set_border_range(ws7, f'B{row}:C{row}', 'thin')

        content = save_virtual_workbook(wb)
        return content