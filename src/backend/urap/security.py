from flask import current_app as app, abort, request
from werkzeug.http import parse_authorization_header
from eve.auth import TokenAuth

def blueprint_auth():
    if 'Authorization' not in request.headers:
        return abort(401, 'Please provide proper credentials')
    if not app.data.driver.db['tokens'].find_one({'token': request.headers['Authorization']}):
        return abort(401, 'Please provide proper credentials')

class URAPTokenAuth(TokenAuth):
    def check_auth(self, token, allowed_roles, resource, method):
        tokens = app.data.driver.db['tokens']
        return tokens.find_one({'token': token})
