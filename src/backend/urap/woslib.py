import logging
import time
import re
import json
import requests
from timeit import default_timer
from bs4 import BeautifulSoup
from urllib import parse
import sys, os

class Helpers(object):

    def get_logger(self, name='__app__', file=None, debug=False):
        log_level = logging.INFO
        if debug == True:
            log_level = logging.DEBUG            
        logger = logging.getLogger(name)
        logger.setLevel(log_level)
        if file == None:
            ch = logging.StreamHandler()
        else:
            ch = logging.FileHandler(file)
        ch.setLevel(log_level)
        formatter = logging.Formatter('%(asctime)s - %(filename)s:%(funcName)20s() - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        return logger

    def get_http_prefix(self, type):
        '''
            WoS URLs prefixes to be used when querying via HTTP
        '''
        _prefixes = {
            "index": "https://www.webofknowledge.com",
            "post_url": "https://apps.webofknowledge.com/WOS_GeneralSearch.do",
            "record_url": "https://apps.webofknowledge.com/full_record.do?product=WOS&search_mode=GeneralSearch&qid=",
            "citation_url": "https://apps.webofknowledge.com/full_record.do?product=WOS&search_mode=CitingArticles&qid=",
            "citations_url": "http://apps.webofknowledge.com",
            "summary_url": "https://apps.webofknowledge.com/summary.do?product=WOS&parentProduct=WOS&search_mode=GeneralSearch&page=1&action=changePageSize&&pageSize=50",
            "apps_index": "https://apps.webofknowledge.com"
        }

        if type not in _prefixes.keys():
            return None
        else:
            return _prefixes[type]

    def get_http_header(self):
        '''
            HTTP header to use when making WoS calls
        '''
        return {
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
        }

    def dump_html(self, dom):
        '''
            dump DOM html reprezentation in case of an unrecoverable error to examin what
            went wrong
        '''
        with open('/tmp/dump-{}.html'.format(time.strftime("%Y_%m_%d-%H_%M_%S")), 'w') as f:
            f.write(dom.prettify())

    def dump_csv(self, opath, header, list):
        import csv
        with open(opath, 'w', newline='') as csvfile:
            sw = csv.writer(csvfile, delimiter=',',
                       quotechar='"', quoting=csv.QUOTE_MINIMAL)
            sw.writerow(header)
            for l in list:
                sw.writerow(l)

    def get_uvt_names(self):
        return [
                    'Fac Math, Timisoara 1900, Romania',
                    'WEST UNIVERSITY OF TIMISOARA',
                    'AGR UNIV TIMISOARA',
                    'IND CHEM ENVIRONM ENGN FAC',
                    'TIMISOARA UNIV',
                    'UNIT WEST',
                    'UNIV DIN TIMISOARA',
                    'UNIV OUEST TIMISOARA',
                    'UNIV TIMIS OARA',
                    'UNIV TIMISOARA',
                    'UNIV TIMISOARA W',
                    'UNIV TIMISOARA WEST',
                    'UNIV VEST',
                    'UNIV VEST DIN TIMISOARA',
                    'Univ Vest Timisoara',
                    'UNIV W',
                    'UNIV W TIMISOARA',
                    'UNIV W TOMISOARA',
                    'UNIV WEST',
                    'UVT',
                    'VEST UNIV TIMISOARA',
                    'W TIMISOARA UNIV',
                    'W UNIV',
                    'W UNIV RIMISOARA',
                    'W UNIV TIMIOARA',
                    'W UNIV TIMIS OARA',
                    'W UNIV TIMISAORA',
                    'W UNIV TIMISOARA',
                    'W UNIV TIMISOARA V PARVAN',
                    'W UNIV TIMISOARRA',
                    'W UNIV TIMISOUARA',
                    'W UNIV TIMISOURA',
                    'W UNIV TIMSOARA',
                    'W UNIV TINDSOARA',
                    'W UNIV TUNISOARA',
                    'W UNV TIMISOARA',
                    'W VIRGINIA UNIV TIMISOARA',
                    'WEAT UNIV TIMISOARA',
                    'WEST UNIV',
                    'WEST UNIV TIMISOARA',
                    'WESTERN UNIV',
                    'WESTERN UNIV TIMISOARA'
                ]

    def get_wos_session(self):
        _logger = self.get_logger(name='wos-session', debug=True)
        s = requests.Session()
        _logger.info('Creating a WoS session ...')
        ret = s.get(Helpers().get_http_prefix('index'), headers=Helpers().get_http_header())
        if ret.status_code == 200:
            _logger.debug('WoS Session URL: {}'.format(ret.url))
            sid = re.match(r'.*&SID=([a-zA-Z0-9]+)&.*', str(ret.url)).group(1)
        else:
            _logger.error('WoS Session failed')
            _logger.error('Wos Session http code: {}'.format(ret.status_code))
            _logger.debug('WoS Session returned: \n{}'.format(ret.text))
            return None, None
        return s, sid

class WosLib(object):

    def __init__(self, work_dir, logger=None, local_file=None):
        if logger == None:
            self.logger = Helpers().get_logger(name=__name__)
        else:
            self.logger = logger
        self.work_dir = work_dir
        self.local_file = local_file

    def wos_parse_paper_page(self, dom):
        if not dom('value'):
            self.logger.error("Received HTML is not ok!")
            Helpers().dump_html(dom)
            return None

        def _get_pages(tag):
            if tag.name == 'span':
                c = tag.get('class', None)
                if c != None and 'FR_label' in c:
                    if 'Pages:' in tag.text:
                        return tag

        def _get_issn(tag):
            if tag.name == 'span':
                c = tag.get('class', None)
                if c != None and 'FR_label' in c:
                    if 'ISSN:' in tag.text:
                        return tag

        def _get_eissn(tag):
            if tag.name == 'span':
                c = tag.get('class', None)
                if c != None and 'FR_label' in c:
                    if 'eISSN:' in tag.text:
                        return tag

        def _get_isbn(tag):
            if tag.name == 'span':
                c = tag.get('class', None)
                if c != None and 'FR_label' in c:
                    if 'ISBN:' in tag.text:
                        return tag

        def _get_doi(tag):
            if tag.name == 'span':
                c = tag.get('class', None)
                if c != None and 'FR_label' in c:
                    if 'DOI:' in tag.text:
                        return tag

        def _get_abstract(tag):
            if tag.name == 'div':
                c = tag.get('class', None)
                if c != None and 'title3' in c:
                    if 'Abstract' in tag.text:
                        return tag

        parse_dict = {}
        journal = dom("p", class_='sourceTitle')
        if journal:
            parse_dict['journal'] = journal[0].text.strip("\n")
        else:
            parse_dict['journal'] = ""
        title = dom("div", class_='title')
        if title:
            parse_dict['title'] = title[0].text.strip("\n")
        else:
            parse_dict['title'] = ""
        try:
            wos_id_tag = dom.findAll(attrs={'name': 'recordID', 'type': 'hidden'})
            parse_dict['wos_id'] = wos_id_tag[0]['value']
        except:
            parse_dict['wos_id'] = '-'
        '''
        pages = dom.find(_get_pages).next_sibling
        print(pages)
        if not pages:
            pages = dom.find(lambda tag: tag.name == "span" and "Article Number" in tag.text)

        #print('-- parse_dict value: ', parse_dict)
        #print('\n-- Pages type: ', type(pages))
        try:
            #print('-- Pages value: \n', pages.next_sibling)
            pages = pages.next_sibling
            #print('-- Pages next value: \n', pages)
            #print(pages.strip())
            if pages != '\n':
                parse_dict['number'] = pages.strip()
            else:
                parse_dict['number'] = pages.next_sibling.string.strip()
        except AttributeError:
            if len(dom('value')) > 5:
                parse_dict['number'] = dom('value')[4].string
            else:
                parse_dict['number'] = ""
        #except TypeError as e:
        #    print('\nPaper exception:{}\n\t{}'.format(os.path.basename(savepath), e))
        #    with open('./exceptions/{}.html'.format(os.path.basename(savepath)), 'w') as fp:
        #        fp.write(str(dom))
        #    raise
        '''
        try:
            volume = dom.find(lambda tag: tag.name == "span" and "Volume" in tag.text).next_sibling
            if volume != '\n':
                parse_dict['volume'] = volume.strip()
            else:
                parse_dict['volume'] = volume.next_sibling.string.strip()
        except AttributeError:
            if len(dom('value')) > 3:
                parse_dict['volume'] = dom('value')[2].string
            else:
                parse_dict['volume'] = ""

        try:
            issue = dom.find(lambda tag: tag.name == "span" and "Issue" in tag.text)
            if issue:
                issue = issue.next_sibling
                if issue != '\n':
                    parse_dict['issue'] = issue.strip()
                else:
                    parse_dict['issue'] = issue.next_sibling.string.strip()
            else:
                parse_dict['issue'] = ""
        except AttributeError:
            if len(dom('value')) > 4:
                parse_dict['issue'] = dom('value')[3].string
            else:
                parse_dict['issue'] = ""

        try:
            date = dom.find(lambda tag: tag.name == "span" and "Published" in tag.text)  # .next_sibling
            if not date:
                date = dom.find(lambda tag: tag.name == "span" and "Date" in tag.text)
            date = date.next_sibling
            if date != '\n':
                parse_dict['date'] = date.strip()
            else:
                parse_dict['date'] = date.next_sibling.string.strip()
        except AttributeError:
            if len(dom('value')) > 7:
                parse_dict['date'] = dom('value')[6].string
            else:
                parse_dict['date'] = ""
        try:
            ctype = dom.find(lambda tag: tag.name == "span" and "Web of Science Categories:" in tag.text).next_sibling
            if ctype != '\n':
                parse_dict['wos_category'] = ctype.strip()
            else:
                parse_dict['wos_category'] = ctype.next_sibling.string.strip()
        except AttributeError:
            parse_dict['wos_category'] = ''

        try:
            dtype = dom.find(lambda tag: tag.name == "span" and "Document Type:" in tag.text).next_sibling
            if dtype != '\n':
                parse_dict['document_type'] = dtype.strip()
            else:
                parse_dict['document_type'] = dtype.next_sibling.string.strip()
        except AttributeError:
            parse_dict['document_type'] = ''

        try:
            issn = dom.find(_get_issn).next_sibling
            if issn != '\n':
                parse_dict['issn'] = issn.strip()
            else:
                parse_dict['issn'] = issn.next_sibling.string.strip()
        except AttributeError:
            parse_dict['issn'] = ""
        except Exception as e:
            parse_dict['issn'] = ""
            self.logger.debug('issn find error: {}'.format(e))

        try:
            eissn = dom.find(_get_eissn).next_sibling
            if eissn != '\n':
                parse_dict['eissn'] = eissn.strip()
            else:
                parse_dict['eissn'] = eissn.next_sibling.string.strip()
        except AttributeError:
            parse_dict['eissn'] = ""
        except Exception as e:
            parse_dict['eissn'] = ""
            self.logger.debug('eissn find error: {}'.format(e))

        try:
            isbn = dom.find(_get_isbn).next_sibling
            if isbn != '\n':
                parse_dict['isbn'] = isbn.strip()
            else:
                parse_dict['isbn'] = isbn.next_sibling.string.strip()
        except AttributeError:
            parse_dict['isbn'] = ''
        except Exception as e:
            parse_dict['isbn'] = ''
            self.logger.debug('isbn find error: {}'.format(e))

        try:
            doi = dom.find(_get_doi).next_sibling
            if doi != '\n':
                parse_dict['doi'] = doi.strip()
            else:
                parse_dict['doi'] = doi.next_sibling.string.strip()
        except AttributeError:
            parse_dict['doi'] = ""

        parse_dict['email'] = [a.text for a in dom('a', class_='snowplow-author-email-addresses')]
        parse_dict['fund'] = [(f('td')[0].string.strip(), [fd.string.strip() for fd in f('div')]) for f in
                              dom('tr', class_='fr_data_row')]
        parse_dict['keyword'] = [a.text for a in dom("a", class_="snowplow-kewords-plus-link")]
        authorinfo = dom('a', title="Find more records by this author")
        parse_dict['author'] = []
        for ai in authorinfo:
            auname = ai.next_sibling.strip().strip(",").strip(";").strip('(').strip(')')
            auid = 'dais_id:'
            auinst = []
            if ai.next_sibling.next_sibling:
                for inst in ai.next_sibling.next_sibling('b')[1:]:
                    if inst:
                        try:
                            auinst.append(int(inst.string))
                        except TypeError:
                            pass
            try:
                _q = dict(parse.parse_qsl(parse.urlparse(ai.get("href")).query))
                auid += _q['dais_id']
            except:
                pass
            parse_dict['author'].append((auname, auid, auinst))

        try:
            parse_dict['abstract'] = dom.find(_get_abstract).next_sibling.next_sibling.text
        except:
            parse_dict['abstract'] = ''

        parse_dict['cited_num'], parse_dict['referenced_num'] = [int(re.subn(",", "", n.text.strip())[0]) for n in
                                                                 dom('span', class_='large-number')[0:2]]
        referenced = dom('a', class_='snowplow-citation-network-cited-reference-count-link')
        if referenced:
            parse_dict['referenced_link'] = "https://apps.webofknowledge.com/" + referenced[0].get("href")
        cited = dom('a', class_='snowplow-citation-network-times-cited-count-link')
        if cited:
            parse_dict['cited_link'] = "https://apps.webofknowledge.com/" + cited[0].get("href")
        inst = []
        instshort = []
        addlist = dom('td', class_='fr_address_row2')
        for address in addlist:
            if address('a'):
                l = address.text.split('\n')
                ldeno = re.sub(r"^\[ [0-9]+ \] ", "", l[0].strip())
                inst.append(ldeno)
                if len(l) == 3:
                    instshort.append(l[-1].strip())
                elif len(l) == 4:
                    instshort.append(l[-2].strip())
                else:
                    instshort.append("")
        if len(inst) == 0:
            addlist = dom('td', class_='fr_address_row2')
            for address in addlist:
                if not address('a'):
                    inst.append(address.text.split('\n')[0])
        parse_dict['inst'] = inst
        parse_dict['instshort'] = instshort
        js = dom("div", class_='flex-row-partition2')
        parse_dict['hotpapers'] = False
        parse_dict['highlycited'] = False
        if js:
            js = js[0].contents[1]
            hc = re.search(r"'highlyCited': true", js.string[-100:])
            hp = re.search(r"'hotPaper': true", js.string[-100:])
            if hc:
                parse_dict['highlycited'] = True
            if hp:
                parse_dict['hotpapers'] = True

        parse_dict['cited_papers'] = []
        return parse_dict

    def wos_parse_papers_list(self, dom, start):
        _papers = []
        self.logger.debug('start number {}'.format(start))
        for i in range(start, start + 10):
            paper = {}
            div = dom.find('div', id='RECORD_{}'.format(i))
            if div is not None:
                wos_url = div.find('span', id='fetch_wos_subject_Span_{}'.format(i)).get('url')
                wos_id = re.match(r'.*&isickref=(WOS:[a-zA-Z0-9]+)&.*', str(wos_url)).group(1)

                published = div.find(
                    lambda tag: tag.name == "span" and "Published:" in tag.text and 'label' in tag.attrs[
                        'class']).next_sibling.text.strip()
                cited_field = div.find(
                    lambda tag: tag.name == "div" and "Times Cited:" in tag.text and 'search-results-data-cite' in
                                tag.attrs['class']).text
                cited = re.match(r'.*Times Cited: ([0-9]+).*', str(cited_field)).group(1)
                paper['wos_id'] = wos_id
                paper['published'] = published
                paper['citations_number'] = cited
                _papers.append(paper)
            else:
                Helpers().dump_html(dom)
                self.logger.debug('RECORD_ID div find is None')
        return _papers

    def wos_get_papers_list_with_pagination(self, dom, path_prefix, s, perpage=None, nextpageno=None):
        papers = []
        _id = 1
        _count = 0
        if perpage is None:
            pp = 10
        else:
            pp = perpage
        self.logger.debug('perpage: {}'.format(pp))
        self.logger.debug('path_prefix: {}'.format(path_prefix))
        ni = dom.find("span", {"id": "footer_formatted_count"}).string
        _total = int(re.subn(",", "", ni)[0])
        nr_pages = dom.find("span", id='pageCount.top').string
        self.logger.info('Number of papers: {}'.format(_total))
        self.logger.info('Number of pages: {}'.format(nr_pages))
        self.logger.info('Crawled page: {}'.format(_id))
        ex = self.wos_parse_paper_list_with_pagination(dom, (_id*pp+1)-pp, path_prefix, s, perpage=pp)
        #json.dump(ex, open('{}{}.json'.format(path_prefix, _id), 'w'), indent=4)
        papers.extend(ex)
        next_page_field = dom.find("a", class_='paginationNext snowplow-navigation-nextpage-top')
        next_page = None
        if next_page_field is not None:
            next_page = next_page_field.get('href')
            if nextpageno is not None:
                next_page = re.sub(r'(page=.*)', "page={}".format(int(nextpageno)), next_page)
                _id = int(nextpageno) - 1
            try:
                pageno = re.match(r'.*&page=([0-9]+).*', str(next_page)).group(1)
                self.logger.debug('next_page_no: {}'.format(pageno))
            except:
                pass
        while next_page:
            tstart = default_timer()
            self.logger.debug('next_page: {}'.format(next_page))
            ret2 = s.get(next_page, headers=Helpers().get_http_header())
            if ret2.status_code == 200:
                _id += 1
                self.logger.info('crawled page: {}'.format(_id))
                x2 = BeautifulSoup(ret2.text, "lxml")
                if not x2('value'):
                    self.logger.error('Returned value is not the expected one')
                    return None
                next_page_field = x2.find("a", class_='paginationNext snowplow-navigation-nextpage-top')
                if next_page_field is not None:
                    next_page = next_page_field.get('href')
                    try:
                        pageno = re.match(r'.*&page=([0-9]+).*', str(next_page)).group(1)
                        self.logger.debug('next_page_no: {}'.format(pageno))
                    except:
                        pass
                else:
                    next_page = None
                ex = self.wos_parse_paper_list_with_pagination(x2, (_id*pp+1)-pp, path_prefix, s, perpage=pp)
                #json.dump(ex, open('{}{}.json'.format(path_prefix, _id), 'w'), indent=4)
                papers.extend(ex)
                x2 = None
                self.logger.debug(
                    'fetched:{:5.2f}s'.format(default_timer() - tstart))
                self.logger.info('collected {} papers until now.'.format(len(papers)))

        return papers

    def wos_parse_paper_list_with_pagination(self, dom, start, path_prefix, s=None, perpage=None):
        _papers = []
        if perpage is None:
            pp = 10
        else:
            pp = perpage
        self.logger.debug('start number {}'.format(start))
        for i in range(start, start + pp):
            paper = {}
            div = dom.find('div', id='RECORD_{}'.format(i))
    
            # check if et all. exists
            if div is not None:
                wos_url = div.find('span', id='fetch_wos_subject_Span_{}'.format(i)).get('url')
                wos_id = re.match(r'.*&isickref=(WOS:[a-zA-Z0-9]+)&.*', str(wos_url)).group(1)
                self.logger.debug('paper:{}'.format(wos_id))
                _fn = path_prefix + wos_id.split(':')[1] + '.json'
                if os.path.isfile(_fn):
                    self.logger.info('\t - paper:{} already crawled. Skipping ...'.format(wos_id))
                    continue
                ## title
                title = ''
                atitle = div.find_all('a', attrs={'class': 'smallV110 snowplow-full-record'})
                try:
                    title = atitle[0].findChild().text
                except Exception as e:
                    self.logger.debug('EXCEPTION title extraction failed: {} ({})'.format(str(e), wos_id))
                self.logger.debug('\t - identified title: {}'.format(title))
                ##

                ## authors
                authors = []
                adiv = None
                adiv_full = False
                _v1_path = '/home/silviu/temp/urap/v1'
                _v1_fn = os.path.join(_v1_path, 'paper-wos_{}.json'.format(wos_id.split(":")[1]))
                if os.path.isfile(_v1_fn):
                    self.logger.debug('\t - found v1 json. Using this one for authors list ({})'.format(_v1_fn))
                    with open(_v1_fn) as fp:
                        aidx = 1
                        pdb = json.load(fp)
                        for a in pdb['author']:
                            authors.append({'name': a['name'], 'dais_id': a['dais_id'], 'index': aidx})
                            aidx += 1
                else:
                    etallcheck = div.find(text=re.compile('; et al.'))
                    if etallcheck:   
                        if not s:
                            self.logger.error('WoS Session not set. Cannot grab the full record. ({})'.format(wos_id))
                            continue
                        self.logger.debug('\t - "et al." present in authors string. crawling the full paper')
                        _href = atitle[0].get('href')
                        self.logger.debug('\t - full record url: {}'.format(_href))
                        ret = s.get(Helpers().get_http_prefix('apps_index') + _href, headers=Helpers().get_http_header())
                        if ret.status_code == 200:
                            fdom = BeautifulSoup(ret.text, "lxml")
                            alst = fdom.find(lambda tag: tag.name == "span" and "By:" in tag.text and 'FR_label' in tag.attrs[
                                'class'])
                            # get parent tag to have all the entries in one mini DOM
                            aal = alst.parent
                            adiv = aal.find_all('a', attrs={'title': 'Find more records by this author'})
                            adiv_full = True
                        else:
                            self.logger.error('\t - full record retrieval failed; http_code:{} ({})'.format(ret.status_code, wos_id))
                            continue

                    if adiv is None:
                        adiv = div.find_all('a', attrs={'title': 'Find more records by this author'})
                    aidx = 1
                    for a in adiv:
                        try:
                            dais_id = re.match(r'.*&daisIds=([a-zA-Z0-9]+).*', str(a['href'])).group(1)
                        except Exception as e:
                            dais_id = ''
                            self.logger.error('\t ERROR getting dais_id: {} ({})'.format(str(e), wos_id))
                        name = a.text
                        if adiv_full:
                            try:
                                _nn = re.match(r'.*\((.*)\).*', str(a.next_sibling)).group(1)
                            except Exception as e:
                                _nn = ''
                                self.logger.error('\t ERROR extracting name from full record: {} ({})'.format(a.next_sibling, wos_id))
                            authors.append({'name': _nn, 'dais_id': dais_id, 'index': aidx})
                        else:
                            authors.append({'name': a.text, 'dais_id': dais_id, 'index': aidx})
                        aidx += 1
                self.logger.debug('\t - identified no of authors: {}'.format(len(authors)))
                ##

                try:
                    doi = div.find('span', id='doi_{}'.format(i)).text
                except:
                    doi = ''
                self.logger.debug('\t - doi:{}'.format(doi))
                try:
                    published = div.find(
                        lambda tag: tag.name == "span" and "Published: " in tag.text and 'label' in tag.attrs[
                            'class']).next_sibling.findChild().text
                except:
                    published = div.find('span', id='early_access_month_{}'.format(i)).text
                self.logger.debug('\t - published:{}'.format(published))
                try:
                    published_year = re.match(r'.*([1|2][0|9][0-9][0-9]).*', str(published)).group(1)
                except:
                    published_year = ''
                self.logger.debug('\t - published_year:{}'.format(published_year))
                cited_field = div.find(
                    lambda tag: tag.name == "div" and "Times Cited:" in tag.text and 'search-results-data-cite' in
                                tag.attrs['class']).text
                cited = re.match(r'.*Times Cited: ([0-9]+).*', str(cited_field)).group(1)
                paper['wos_id'] = wos_id
                paper['title'] = title
                paper['published'] = published
                paper['published_year'] = published_year
                paper['doi'] = doi
                paper['citations'] = cited
                paper['authors'] = authors
                paper['timestamp'] = int(time.time())
                #self.logger.debug('\n{}'.format(json.dumps(paper, indent=4)))
                _papers.append(paper)
                if os.path.isfile(_fn):
                    self.logger.error('{:!>80}'.format(wos_id))
                else:
                    with open(_fn, 'w') as fp:
                        json.dump(paper, fp, indent=4)
            else:
                self.logger.debug('div is None')
                Helpers().dump_html(dom)
        return _papers

    def wos_get_paper_list_by_institution(self, institution_name, session=None, esid=None, perpage=None, nextpageno=None):
        wos_post_url = Helpers().get_http_prefix('post_url')
        wos_index = Helpers().get_http_prefix('index')
        wos_summary = Helpers().get_http_prefix('summary_url')

        dwn_metadata_dir = os.path.join(self.work_dir, 'download/all-papers-metadata')
        if not os.path.isdir(dwn_metadata_dir):
            self.logger.debug('Creating download metadata directory: {}'.format(dwn_metadata_dir))
            os.makedirs(dwn_metadata_dir)

        if self.local_file:
            s = None
            with open(self.local_file) as fp:
                x = BeautifulSoup(fp, 'lxml')
            return self.wos_get_papers_list_with_pagination(x, dwn_metadata_dir + '/paper-', s)

        search_data = {
            "fieldCount": "1",
            "action": "search",
            "product": "WOS",
            "search_mode": "GeneralSearch",
            "formUpdated": "true",
            "value(input1)": institution_name,
            "value(select1)": 'OG',
            "SID": "",
            "rs_rec_per_page": "50",
            "rs_refinePanel": "visibility:show",
        }
        if session is None:
            s = requests.Session()
        else:
            s = session
        self.logger.info('Starting to collect papers metadata for: "{}"'.format(institution_name))
        self.logger.info('Retrieve WoS SID number for this session')
        if esid is None:
            ret = s.get(wos_index, headers=Helpers().get_http_header())
            if ret.status_code == 200:
                sid = re.match(r'.*&SID=([a-zA-Z0-9]+)&.*', str(ret.url)).group(1)
        else:
            sid = esid
        search_data['SID'] = sid
        self.logger.debug('WoS_HTTP search data: \n{}'.format(search_data))
        self.logger.info('Starting to parse the papers list ...')
        self.logger.info('WoS_HTTP SID number: {}'.format(sid))
        ret = s.post(wos_post_url, data=search_data, headers=Helpers().get_http_header())
        if ret.status_code == 200:
            x = BeautifulSoup(ret.text, "lxml")
            if not x('value'):
                self.logger.error('wos_post_url: returned value doesn\'t contain <value>')
                return None
            return self.wos_get_papers_list_with_pagination(x, dwn_metadata_dir + '/paper-', s, perpage=perpage, nextpageno=nextpageno)
        else:
            self.logger.error('initial HTTP request returned status not 200')
            self.logger.error('\t {}'.format(ret.status_code))
            self.logger.error('\t {}'.format(ret.url))
            return None

    def wos_get_paper_citations_by_wosid(self, wos_id, session=None, esid=None, get_citations=False):
        wos_record_url = Helpers().get_http_prefix('record_url')
        wos_post_url = Helpers().get_http_prefix('post_url')
        wos_index = Helpers().get_http_prefix('index')

        search_data = {
            "fieldCount": "1",
            "action": "search",
            "product": "WOS",
            "search_mode": "GeneralSearch",
            # "max_field_count": "50",
            "formUpdated": "true",
            "value(input1)": wos_id,
            "value(select1)": "UT",
            "SID": "",
            "rs_rec_per_page": "50",
            "rs_refinePanel"	: "visibility:show",
        }

        _metadata_dir = os.path.join(self.work_dir, 'download/all-papers-metadata-citations')
        if not os.path.isdir(_metadata_dir):
            os.makedirs(_metadata_dir)
        _citation_file_prefix = os.path.join(_metadata_dir, 'paper-{}-citation-'.format(wos_id.split(":")[1]))

        sid = ''
        if session is None:
            self.logger.info('Starting a WoS SID session ...')
            s = requests.Session()
        else:
            s = session

        if esid is None:
            self.logger.info('Crawling for paper: {}'.format(wos_id))
            self.logger.info('Generating a WoS SID ...')
            ret = s.get(wos_index, headers=Helpers().get_http_header())
            if ret.status_code == 200:
                sid = re.match(r'.*&SID=([a-zA-Z0-9]+)&.*', str(ret.url)).group(1)
                search_data['SID'] = sid
            else:
                self.logger.error('WoS ID session could be retrieved. HTTP GET index return <> 200: {}'.format(ret.status_code))
                return None
        else:
            search_data['SID'] = esid
        ret = s.post(wos_post_url, data=search_data)
        #self.logger.debug('wos_post_url: {}, search_data: {}'.format(wos_post_url, search_data))
        if ret.status_code == 200:
            x = BeautifulSoup(ret.text, "lxml")
            if not x('value'):
                self.logger.info('wos_post_url: returned DOM is not the expected one')
                Helpers().dump_html(x)
                return None
        else:
            self.logger.info('return status not 200')
            self.logger.info('\t {}'.format(ret.status_code))
            return None

        div = x.find('div', id='RECORD_{}'.format(1))

        cited_field = div.find(
            lambda tag: tag.name == "div" and "Times Cited:" in tag.text and 'search-results-data-cite' in tag.attrs[
                'class'])
        cited = int(re.match(r'.*Times Cited: ([0-9]+).*', str(cited_field.text)).group(1))
        if cited == 0:
            return []
        cited_links = Helpers().get_http_prefix("citations_url")+'/'+cited_field.find(
            lambda tag: tag.name == "a" and 'snowplow-times-cited-link' in tag.attrs['class']
        ).get("href")
        self.logger.debug('Citations link: {}'.format(cited_links))
        ret = s.get(cited_links)
        if ret.status_code == 200:
            if ret.text.find('None of the Citing Articles are in your subscription') != -1:
                self.logger.info(
                    "NOT_ALLOWED: subscription doesn't allow the read of citations list")
                return []
            x2 = BeautifulSoup(ret.text, "lxml")
            if not x2('value'):
                self.logger.info('citation_links: returned DOM is not the expected one')
                Helpers().dump_html(x2)
                return None
        else:
            self.logger.info('return status not 200:')
            self.logger.info('\t {}'.format(ret.status_code))
            return None

        return self.wos_get_papers_list_with_pagination(x2, _citation_file_prefix, s)

    def wos_get_paper_content_by_wosid(self, wos_id, session=None, esid=None):
        wos_record_url = Helpers().get_http_prefix('record_url')
        wos_post_url = Helpers().get_http_prefix('post_url')
        wos_index = Helpers().get_http_prefix('index')
        wos_id = wos_id.split(":")[1]
        search_data = {
            "fieldCount": "1",
            "action": "search",
            "product": "WOS",
            "search_mode": "GeneralSearch",
            # "max_field_count": "50",
            "formUpdated": "true",
            "value(input1)": wos_id,
            "value(select1)": "UT",
            "SID": "",
            "rs_rec_per_page": "50",
            "rs_refinePanel"	: "visibility:show",
        }
        sid = ''
        if session is None:
            self.logger.info('Starting a WoS SID session ...')
            s = requests.Session()
        else:
            s = session
        _pfile = 'crawler/papers/paper-wos_{}.json'.format(wos_id)
        if os.path.isfile(_pfile):
            self.logger.info('paper {} has no citations and already downloaded. Stopping'.format(wos_id))
            return None
        if esid is None:
            self.logger.info('crawling for WoS ID: {}'.format(wos_id))
            self.logger.info('get WoS SID ...')
            ret = s.get(wos_index, headers=Helpers().get_http_header())
            if ret.status_code == 200:
                sid = re.match(r'.*&SID=([a-zA-Z0-9]+)&.*', str(ret.url)).group(1)
                search_data['SID'] = sid
            else:
                self.logger.error('WoS ID session could be retrieved. HTTP GET index return <> 200: {}'.format(ret.status_code))
                return None
        else:
            search_data['SID'] = esid
        ret = s.post(wos_post_url, data=search_data)
        if ret.status_code == 200:
            x = BeautifulSoup(ret.text, "lxml")
            if not x('value'):
                self.logger.info('wos_post_url: returned DOM is not the expected one')
                Helpers().dump_html(x)
                return None
        else:
            self.logger.info('return status not 200')
            self.logger.info('\t {}'.format(ret.status_code))
            return None

        cu = x("a", class_="smallV110 snowplow-full-record")[0].get("href")
        qid = re.match(r".*&qid=([0-9]+)&.*", cu).group(1)
        wos_gu = wos_record_url + '{}&SID={}&doc=1'.format(qid, sid)
        self.logger.info('wos_get_url: {}'.format(wos_gu))
        ret = s.get(wos_gu, headers=Helpers().get_http_header())
        if ret.status_code == 200:
            x2 = BeautifulSoup(ret.text, 'lxml')
            if not x2('value'):
                self.logger.info('wos_get_url: returned DOM is not the expected one')
                Helpers().dump_html(x2)
                return None
            return self.wos_parse_paper_page(x2)
        else:
            self.logger.error('return status not 200')
            self.logger.error('\t {}'.format(ret.status_code))
            return None

