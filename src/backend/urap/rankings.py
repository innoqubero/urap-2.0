#!/usr/bin/env python3

from eve.auth import requires_auth
from bson import json_util, ObjectId
from flask import request, Blueprint

from eve.auth import TokenAuth
from flask import current_app as app


rankings_service = Blueprint('rankings_service', __name__)


"""
Request example:
http://localhost:5001/api/v2/rankings/0960-7692/uefiscdi
Response example:
{"AIS": [{"2011": 0.764}, {"2012": 0.949}, {"2013": 0.848}, {"2014": 1.127}, {"2015": 1.149}, {"2016": 1.386}, {"2017":
1.456}, {"2019": 1.561}], "RIF": [{"2012": 2.08}, {"2013": 1.697}, {"2014": 2.213}, {"2015": 2.266}, {"2016": 2.244},
{"2017": 2.44}, {"2018": 2.201}, {"2019": 2.121}], "RIS": [{"2012": 2.177}, {"2013": 2.12}, {"2014": 2.55}, {"2015":
2.623}, {"2016": 2.912}, {"2017": 3.059}, {"2018": 3.471}, {"2019": 3.656}], "zone": [{"ACOUSTICS": {"2015": {"index":
"SCIE", "zona": 1, "top": 1}, "2016": {"index": "SCIE", "zona": 1, "top": 1}, "2017": {"index": "SCIE", "zona": 1,
"top": 1}, "2018": {"index": "SCIE", "zona": 1, "top": 1}, "2019": {"index": "SCIE", "zona": 1, "top": 2}}},
{"OBSTETRICS_GYNECOLOGY": {"2015": {"index": "SCIE", "zona": 1, "top": 10}, "2016": {"index": "SCIE", "zona": 1, "top":
6}, "2017": {"index": "SCIE", "zona": 1, "top": 7}, "2018": {"index": "SCIE", "zona": 1, "top": 6}, "2019": {"index":
"SCIE", "zona": 1, "top": 5}}}, {"RADIOLOGY_NUCLEARMEDICINE_MEDICAL": {"2015": {"index": "SCIE", "zona": 1, "top":
20}}}, {"RADIOLOGY_NUCLEARMEDICINE_MEDICALIMAGING": {"2016": {"index": "SCIE", "zona": 1, "top": 14}, "2017": {"index":
"SCIE", "zona": 1, "top": 14}, "2018": {"index": "SCIE", "zona": 1, "top": 13}, "2019": {"index": "SCIE", "zona": 1,
"top": 12}}}]}
"""
@rankings_service.route('/api/v2/rankings/<issn>/<source>', methods=['GET'])
def get_all_metrics_by_issn_and_source(issn, source):
    rankings = app.data.driver.db['rankings']
    ranking = rankings.find_one({'issn': issn})

    try:
        data = ranking['rankings'][source]
        return json_util.dumps(data), 200, {'ContentType':'application/json'}
    except IndexError:
        return json_util.dumps(data), 200, {'ContentType': 'application/json'}


"""
Request example:
http://localhost:5001/api/v2/rankings/0960-7692/uefiscdi/2018
Response example:
{"zone": {"ACOUSTICS": {"index": "SCIE", "zona": 1, "top": 1}, "OBSTETRICS_GYNECOLOGY": {"index": "SCIE", "zona": 1,
"top": 6}, "RADIOLOGY_NUCLEARMEDICINE_MEDICALIMAGING": {"index": "SCIE", "zona": 1, "top": 13}}, "RIF": 2.201, "RIS":
3.471}
"""
@rankings_service.route('/api/v2/rankings/<issn>/<source>/<int:year>', methods=['GET'])
def get_all_metrics_by_issn_and_year(issn, source, year):
    rankings = app.data.driver.db['rankings']
    ranking = rankings.find_one({'issn': issn})
    year = str(year)
    data = {}
    data['zone'] = {}

    for key, value in ranking['rankings'][source].items():
        for elem in value:
            try:
                if year in elem.keys():
                    data[key] = elem[year]
                else:

                    for k,v in elem.items():
                        if type(v) == dict:
                            data['zone'][k] = v[year]
            except KeyError:
                pass

    return json_util.dumps(data), 200, {'ContentType':'application/json'}


"""
Request example:
http://localhost:5001/api/v2/rankings/0960-7692/uefiscdi/AIS
Response example:
[{"2011": 0.764}, {"2012": 0.949}, {"2013": 0.848}, {"2014": 1.127}, {"2015": 1.149}, {"2016": 1.386}, {"2017": 1.456},
{"2019": 1.561}]
"""
@rankings_service.route('/api/v2/rankings/<issn>/<source>/<string:metric>', methods=['GET'])
def get_all_metrics_by_issn_and_metric(issn, source, metric):
    data = json_util.loads(get_all_metrics_by_issn_and_source(issn, source)[0])

    try:
        return json_util.dumps(data[metric]), 200, {'ContentType':'application/json'}
    except KeyError:
        return json_util.dumps({}), 404, {'ContentType': 'application/json'}


"""
Request example:
http://localhost:5001/api/v2/rankings/0960-7692/uefiscdi/zone/2016
Response example:
{"ACOUSTICS": {"index": "SCIE", "zona": 1, "top": 1}, "OBSTETRICS_GYNECOLOGY": {"index": "SCIE", "zona": 1, "top": 6},
"RADIOLOGY_NUCLEARMEDICINE_MEDICALIMAGING": {"index": "SCIE", "zona": 1, "top": 14}}
"""
@rankings_service.route('/api/v2/rankings/<issn>/<source>/<metric>/<year>', methods=['GET'])
def get_metric_by_year(issn, source, metric, year):
    data = json_util.loads(get_all_metrics_by_issn_and_metric(issn, source, metric)[0])
    _zone_data = {}

    try:
        for score in data:
            if type(score) == str:
                return json_util.dumps(data[score]), 200, {'ContentType': 'application/json'}
            elif type(score) == dict:
                if year in score.keys():
                    return json_util.dumps(score), 200, {'ContentType': 'application/json'}
                if metric == 'zone':
                    _zone_scores = {}

                    for entry in data:
                        for k,v in entry.items():
                            try:
                                _zone_scores[k] = v[year]
                            except KeyError:
                                pass

                    return json_util.dumps(_zone_scores), 200, {'ContentType': 'application/json'}
    except KeyError:
        return json_util.dumps({}), 404, {'ContentType': 'application/json'}


"""
Request example:
http://localhost:5001/api/v2/rankings/0960-7692/uefiscdi/2015/zoneVal
Response example:
{"ACOUSTICS": "Rosie", "OBSTETRICS_GYNECOLOGY": "Rosie", "RADIOLOGY_NUCLEARMEDICINE_MEDICAL": "Rosie"}
"""
@rankings_service.route('/api/v2/rankings/<issn>/<source>/<year>/zoneVal', methods=['GET'])
def get_zone_name_by_year(issn, source, year):
    data = json_util.loads(get_metric_by_year(issn, source, 'zone', year)[0])

    zone_values = {}
    zone = {1: 'Rosie',
            2: 'Galbena',
            3: 'Gri'}


    for k,v in data.items():
        zone_values[k] = zone[v['zona']]

    try:
        return json_util.dumps(zone_values), 200, {'ContentType':'application/json'}
    except KeyError:
        return json_util.dumps({}), 404, {'ContentType': 'application/json'}


"""
Request example:
http://localhost:5001/api/v2/rankings/0960-7692/uefiscdi/2015/acoustics
Response example:
{"index": "SCIE", "zona": 1, "top": 1}
"""
@rankings_service.route('/api/v2/rankings/<issn>/<source>/<int:year>/<string:category>', methods=['GET'])
def get_zone_by_category_and_year(issn, source, year, category):
    data = json_util.loads(get_metric_by_year(issn, source, 'zone', str(year))[0])
    category = category.upper()

    try:
        return json_util.dumps(data[category]), 200, {'ContentType':'application/json'}
    except KeyError:
        return json_util.dumps({}), 404, {'ContentType': 'application/json'}

