#!/usr/bin/env python3

import uuid
import requests
from eve.auth import requires_auth
import json
from flask import request, Blueprint

# from backend.urap import api
# from backend.urap import bindings

from eve.auth import TokenAuth
# from flask import current_app as app

from pybtex.database import parse_bytes
from pybtex.scanner import TokenRequired


bibtex_import = Blueprint('bibtex_import', __name__)


bibtex_to_urap_types_mapping = {'article': 'journal',
                                'conference': 'conference',
                                'book': 'book',
                                'chapter': 'chapter'}


def get_bib_field(bibfile, field):
    try:
        return bibfile[field]
    except KeyError as e:
        return None


def remove_empty_fields(d):
    for entry in list(d.keys()):
        if type(d[entry]) is dict:
            remove_empty_fields(d[entry])
        elif d[entry] is None:
            del d[entry]


@bibtex_import.route('/api/v2/importbibtex', methods=['POST'])
def import_from_bibtex_file():
    if (not request.files['bibsource'].filename.endswith('.bib')):
        return json.dumps({'status': 'Not a valid BibTeX file'}), 422, {'ContentType': 'application/json'}
    try:
        bibfile = parse_bytes(request.files['bibsource'].read(), 'bibtex')
    except TokenRequired:
        return json.dumps({'status': 'Not a valid BibTeX file'}), 422, {'ContentType': 'application/json'}

    token = request.form['token']

    for e in bibfile.entries:
        asset_id = str(uuid.uuid4())
        asset = {'asset_id': asset_id,
                 'asset_type': 'publication',
                 'index_source': {'source_name': 'URAP',
                                  'source_id': asset_id}}

        fields = bibfile.entries[e].fields
        authors = bibfile.entries[e].persons['author'] if len(bibfile.entries[e].persons['author']) > 0 else bibfile.entries[e].persons['editor']

        asset['people'] = []
        counter = 0
        for author in authors:
            counter += 1
            asset['people'].append({'name': str(author),
                                    'source_author_id': '',
                                    'index': counter})

        asset['title'] = get_bib_field(fields, 'title')
        asset['year'] = get_bib_field(fields, 'year')

        pub_metadata = {}
        pub_metadata['type'] = bibtex_to_urap_types_mapping[bibfile.entries[e].type]
        pub_metadata['volume'] = get_bib_field(fields, 'volume')
        pub_metadata['doi'] = get_bib_field(fields, 'doi')
        pub_metadata['issn'] = get_bib_field(fields, 'issn')
        pub_metadata['isbn'] = get_bib_field(fields, 'isbn')

        try:
            pub_metadata['pages'] = get_bib_field(fields, 'pages')
            pub_metadata['pages'] = int(pub_metadata['pages']) if pub_metadata['pages'] else None
        except ValueError:
            pages = get_bib_field(fields, 'pages').split('-')
            pub_metadata['pages'] = (int(pages[-1]) - int(pages[0])) if pub_metadata['pages'] else None

        pub_metadata['others'] = {'publisher': get_bib_field(fields, 'publisher'),
                                  'journal_name': get_bib_field(fields, 'journal'),
                                  'series': get_bib_field(fields, 'series')}

        asset['publication_metadata'] = pub_metadata

        remove_empty_fields(asset)
        asset = json.dumps(asset)

        # ToDo: check URL and port here!! don't leave it like this
        res = requests.post('http://localhost:5001/api/v2/assets', data=asset,
                            headers={'Authorization': token, 'Content-Type': 'application/json'})

        if res.status_code != 201:
            raise Exception(res.text)

    return json.dumps({'publications': len(bibfile.entries)}), 201, {'ContentType':'application/json'}
