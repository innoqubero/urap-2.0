publication_types = [
    {"id": 'journal', "name": 'Jurnal'},
    {"id": 'conference', "name": 'Conferinta'},
    {"id": 'workshop', "name": 'Workshop'},
    {"id": 'chapter', "name": 'Capitol'},
    {"id": 'book', "name": 'Carte'},
    {"id": 'poster', "name": 'Poster'}
]

crossref_types = {
    'journal-article': publication_types[0],
    'proceedings-article': publication_types[1],
    'book': publication_types[4],
    'book-chapter': publication_types[3],
}

