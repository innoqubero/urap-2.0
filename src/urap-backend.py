#!/usr/bin/env python3

import os, sys
sys.setrecursionlimit(10000)
import uuid, datetime
import requests
from eve import Eve
from eve.auth import requires_auth
import json
from flask import Flask, url_for, redirect, request, abort
import logging

from backend.urap import api
from backend.urap import mangle_user_assets
from backend.urap import mangle_asset_citations
from backend.urap import mangle_assets
from backend.urap import mangle_people

from backend.urap.imports import bibtex_import
from backend.urap.crossref_tools import crossref_tools
from backend.urap.citations import citations_service
from backend.urap.rankings import rankings_service
from backend.urap.reports import reports_service
from backend.urap.services import urap_services

from backend.urap.security import URAPTokenAuth
from flask import current_app as app

from flask_saml2.sp import ServiceProvider
from flask_saml2.utils import certificate_from_file, private_key_from_file


urap_ui_url = 'https://urap.sage.ieat.ro/ui'
urap_cwd = os.path.dirname(os.path.realpath(__file__))
urap_settings = os.path.join(urap_cwd, 'backend/urap/api.py')



class URAPServiceProvider(ServiceProvider):
    def get_logout_return_url(self):
        return url_for('index', _external=True)

    def get_default_login_return_url(self):
        return url_for('index', _external=True)

urap_app = Eve(auth=URAPTokenAuth, settings=urap_settings)
urap_app.secret_key = 'mohuDie1Du1na4wie5ieyoo9eeDon5hoht6fee2ooquoNahzah'

urap_app.register_blueprint(bibtex_import)
urap_app.register_blueprint(crossref_tools)
urap_app.register_blueprint(citations_service)
urap_app.register_blueprint(rankings_service)
urap_app.register_blueprint(reports_service)
urap_app.register_blueprint(urap_services)

sp = URAPServiceProvider()

urap_app.config['SAML2_SP'] = {
    'certificate': certificate_from_file(os.path.join(urap_cwd, 'backend/secrets/urap.sp--cert.pem')),
    'private_key': private_key_from_file(os.path.join(urap_cwd, 'backend/secrets/urap.sp--key.pem')),
}
urap_app.config['SAML2_IDENTITY_PROVIDERS'] = [
    {
        'CLASS': 'flask_saml2.sp.idphandler.IdPHandler',
        'OPTIONS': {
            'display_name': 'e-UVT Identity Provider',
            'entity_id': 'https://login.e-uvt.ro/aai/saml2/idp/metadata.php',
            'sso_url': 'https://login.e-uvt.ro/aai/saml2/idp/SSOService.php',
            'slo_url': 'https://login.e-uvt.ro/aai/saml2/idp/SingleLogoutService.php',
            'certificate': certificate_from_file(os.path.join(urap_cwd, 'backend/secrets/login.e-uvt.ro.idp--cert.pem')),
        },
    },
]

@urap_app.route('/', methods=['GET'])
def real_index():
    return redirect('/ui/', 302)

@urap_app.route('/login', methods=['GET'])
def index():
    if sp.is_user_logged_in():
        auth_data = sp.get_auth_data_in_session()

        message = f'''
        <p>You are logged in as <strong>{auth_data.nameid}</strong>.
        The IdP sent back the following attributes:<p>
        '''
        attrs = '<dl>{}</dl>'.format(''.join(
            f'<dt>{attr}</dt><dd>{value}</dd>'
            for attr, value in auth_data.attributes.items()))
        
        logout_url = url_for('flask_saml2_sp.logout')
        logout = f'<form action="{logout_url}" method="POST"><input type="submit" value="Log out"></form>'
        pp = request.args.get('page')
        if pp is not None:
            return message + attrs + logout
        urap_app.logger.debug(auth_data.attributes)
        if 'mail' not in auth_data.attributes.keys():
            return redirect('{}/login.html?action={}'.format(urap_ui_url, 'notfound'), 302)
        email = auth_data.attributes['mail']
        people = app.data.driver.db['people']
        p = people.find_one({'email': email})
        if p == None:
            return redirect('{}/login.html?action=notfound', 302)
        else:
            if p.get('enabled', 0) != 1:
                return redirect('{}/login.html?action=notenabled'.format(urap_ui_url), 302)
            tokens = app.data.driver.db['tokens']
            r = tokens.find_one({'urap_id': p['urap_id']})
            tokenid = None
            if r == None:
                tokenid = str(uuid.uuid4())
                o = tokens.insert_one({"token": tokenid, "urap_id": p["urap_id"], "createdAt": datetime.datetime.utcnow()})
            else:
                tokenid = r['token']
            return redirect('{}/login.html?action=ok&urap_token={}&urap_id={}'.format(urap_ui_url, tokenid, p['urap_id']), 302)
    else:
        login_url = url_for('flask_saml2_sp.login')
        return redirect(login_url, 302)
        

urap_app.register_blueprint(sp.create_blueprint(), url_prefix='/sso/')


if __name__ == '__main__':
    #handler = logging.FileHandler('urap.log')
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s - %(filename)s:%(funcName)20s() - %(levelname)s - %(message)s'))
    urap_app.logger.setLevel(logging.DEBUG)
    urap_app.logger.addHandler(handler)

    urap_app.on_pre_PATCH_people += mangle_people.person_pre_patch_request_mangle
    urap_app.on_pre_PATCH_assets += mangle_assets.assets_pre_patch_request_mangle
    urap_app.on_post_GET_assets += mangle_assets.assets_post_get_response_mangle
    urap_app.on_post_GET_userassets += mangle_user_assets.dt_user_assets_response_mangle
    urap_app.on_pre_GET_userassets += mangle_user_assets.dt_user_assets_request_mangle
    urap_app.on_pre_GET_assetcitations += mangle_asset_citations.dt_asset_citations_request_mangle
    urap_app.on_post_GET_assetcitations += mangle_asset_citations.dt_asset_citations_response_mangle

    urap_app.run(host=os.getenv('URAP_HOSTNAME', '127.0.0.1'), port=int(os.getenv('URAP_PORT', 5001)), debug=True, threaded=True)
    #import bjoern
    #bjoern.run(urap_app, os.getenv('URAP_HOSTNAME', '127.0.0.1'), int(os.getenv('URAP_PORT', 5001)))
