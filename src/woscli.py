#!/usr/bin/env python

import argparse
from backend.urap.woslib import Helpers
from backend.urap.woslib import WosLib
from datetime import datetime
import sys, os, time, json

parser = argparse.ArgumentParser(allow_abbrev=False) 
parser.add_argument("--get-institution-papers-metadata", default=False, action="store_true", help="get institution papers metadata") 
parser.add_argument("--institution-name", type=str, default='west university of timisoara', help="institution name")
parser.add_argument("--get-institution-papers-citations-metadata", default=False, action="store_true", help="get institution papers citations metadata")
parser.add_argument("--papers-metadata-db", default='', type=str, help="papers metadata json file")
parser.add_argument("--wos-sid", default=None, type=str, help="WoS pregerated SID")
parser.add_argument("--wos-perpage", default=10, type=str, help="WoS pregerated SID")
parser.add_argument("--wos-nextpageno", default=None, type=int, help="WoS next page to start with")
parser.add_argument('--work-dir', type=str, default='_work', help='working directory for temporary files')
parser.add_argument('--force', default=False, action="store_true", help='force overwrite operations')
parser.add_argument('--debug', default=False, action="store_true", help='enable debugging')
parser.add_argument('--debug-file', default=None, type=str, help='redirect debug information to a file')

args = parser.parse_args()

if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)

logger = Helpers().get_logger(name="woscli", file=args.debug_file, debug=args.debug)

logger.debug('{:-<80}'.format(""))
logger.debug('WoS CLI arguments:')
dargs = vars(args)
for k, v in dargs.items():
    logger.debug(' -- {}: {}'.format(k, v))
logger.debug('{:-<80}'.format(""))

if not os.path.isdir(args.work_dir):
    logger.debug('Creating working directory: {}'.format(args.work_dir))
    os.mkdir(args.work_dir)
else:
    if not args.force and args.get_institution_papers_metadata:
        logger.error('Working directory already exists. Use --force to enable overwrite operations')
        sys.exit(1)

major_command = False

if args.get_institution_papers_metadata:
    major_command = True
    all_papers = WosLib(args.work_dir, logger=logger).wos_get_paper_list_by_institution(args.institution_name, esid=args.wos_sid, perpage=int(args.wos_perpage), nextpageno=args.wos_nextpageno)
    db = {}
    db['timestamp'] = int(time.time())
    db['papers_no'] = len(all_papers)
    db['data'] = all_papers
    json.dump(db, open(os.path.join(args.work_dir, 'all-metadata.json'), 'w'), indent=4)

if args.get_institution_papers_citations_metadata:
    if major_command:
        logger.error('Another main command incompatible with this one was selected.')
        sys.exit(1)
    major_command = True
    if len(args.papers_metadata_db) == 0:
        papers_meta_db = os.path.join(args.work_dir, 'all-metadata.json')
    else:
        papers_meta_db = args.papers_metadata_db
    if not os.path.isfile(papers_meta_db):
        logger.error('Papers metadata JSON file not found. ({})'.format(papers_meta_db))
        sys.exit(1)
    papers_meta_prefix = os.path.join(args.work_dir, 'download/all-papers-metadata-papers/paper-')
    with open(papers_meta_db, 'r') as fp:
        pdb = json.load(fp)
    logger.info('Database size: {}'.format(pdb['papers_no']))
    logger.info('Database timestamp: {}'.format(datetime.utcfromtimestamp(pdb['timestamp']).strftime('%Y-%m-%d %H:%M:%S')))
    ctd = 0
    ctt = 1
    cttotal = len(pdb['data'])
    s, sid = Helpers().get_wos_session()
    for p in pdb['data']:
        wos_id = p['wos_id'].split(":")[1]
        if len(p['doi']) == 0:
            ctd += 1
        logger.info('processing paper [{}/{}]: {}'.format(ctt, cttotal, wos_id))
        logger.debug('\t - year: {}'.format(p['published_year']))
        logger.debug('\t - citations: {}'.format(p['citations']))
        logger.debug('\t - doi: {}'.format(p['doi']))
        logger.debug('\t - crawled date: {}'.format(datetime.utcfromtimestamp(p['timestamp']).strftime('%Y-%m-%d %H:%M:%S')))
        if int(p['citations']) > 0:
            if int(p['citations']) > 300:
                logger.debug('\t - number of citations > 300, starting session inside the crawler ...')
                p['citations_list'] = WosLib(args.work_dir, logger).wos_get_paper_citations_by_wosid(p['wos_id'])
            else:
                logger.debug('\t - number of citations < 300, using session ...')
                _p = WosLib(args.work_dir, logger).wos_get_paper_citations_by_wosid(p['wos_id'], session=s, esid=sid)
                if _p is None:
                    logger.info('{:!!<80}'.format('reloading WoS session'))
                    s, sid = Helpers().get_wos_session()
                    p['citations_list'] = WosLib(args.work_dir, logger).wos_get_paper_citations_by_wosid(p['wos_id'], session=s, esid=sid)
                else:
                    p['citations_list'] = _p
        else:
            logger.info('\t !! skipping {} with 0 citations'.format(wos_id))
        ctt += 1
    
    json.dump(pdb, open('all_metadata_with_citations.json'), indent=4)
    logger.info('Papers without DOI: {} out of {}'.format(ctd, len(pdb['data'])))
