FROM ubuntu:20.04

LABEL maintainer="Silviu Panica <silviu@solsys.ro>"

ENV DEBIAN_FRONTEND noninteractive
RUN apt -y update && apt -y upgrade && apt -y install git python3-pip
RUN mkdir -p /root/.ssh && chmod 700 /root/.ssh
RUN echo '-----BEGIN OPENSSH PRIVATE KEY-----\n\
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW\n\
QyNTUxOQAAACA+2WT7pjRnXW7xNbJh78/k9aA9h6EMUxpPvCdXB//SwwAAAJgENczxBDXM\n\
8QAAAAtzc2gtZWQyNTUxOQAAACA+2WT7pjRnXW7xNbJh78/k9aA9h6EMUxpPvCdXB//Sww\n\
AAAEC9gNt5vrohvClPa6+ejcBPtMiHID/R0UKA0YxYj3wjxT7ZZPumNGddbvE1smHvz+T1\n\
oD2HoQxTGk+8J1cH/9LDAAAAEnVidW50dUB1cmFwLWRldi0wMQECAw==\n\
-----END OPENSSH PRIVATE KEY-----\n'\
> /root/.ssh/id_ed25519
RUN chmod 600 /root/.ssh/id_ed25519
RUN ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts
RUN mkdir -p /srv/repos && cd /srv/repos && git clone git@bitbucket.org:silviu001/urap-2.0.git && \
    cd urap-2.0 && pip3 install -r requirements.txt

COPY scripts/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
EXPOSE 5001

ENTRYPOINT /docker-entrypoint.sh